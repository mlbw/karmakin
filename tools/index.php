<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$directory = getcwd();
$phpfiles = glob($directory . "/*.php");
$dirs = array_filter(glob('*'), 'is_dir');
$additional_links = array(
	//'MailCatcher' => 'http://192.168.33.10:1080/',
	//'DB Search and Replace' => 'http://dailymauritius.mu/tools/snr'
	'DNS Check' => 'https://intodns.com/dailymauritius.mu',
	'IP Blacklist Check' => 'https://mxtoolbox.com/SuperTool.aspx?action=blacklist%3a89.221.209.34&run=toolpage',
	'Mail Spam Check' => 'https://www.mail-tester.com/'
	)
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Tools</title>

    <!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    	@import url(http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css);
		.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
		color: #fff;
		background-color: #428bca;
		}

		.margintop20 {
		    margin-top:20px;
		}

		.nav-pills>li>a {
		border-radius: 0px;
		}

		a {
		color: #000;
		text-decoration: none;
		text-transform: capitalize;
		}

		a:hover {
		color: #000;
		text-decoration: none;
		}


		.nav-stacked>li+li {
		margin-top: 0px;
		margin-left: 0;
		border-bottom:1px solid #dadada;
		border-left:1px solid #dadada;
		border-right:1px solid #dadada;
		}
		.fa{
			margin-right: 20px;
		}
    </style>
  </head>
  <body>
		<div class="container">
		    <div class="row">
		        <div class="col-md-3">

		        </div>
		        <div class="col-md-6">
		        	<ul class="nav nav-pills nav-stacked margintop20">
		        		<li class="active"><a href="/tools">Menu</a></li>
						<?php foreach($phpfiles as $phpfile):?>
							<?php if (basename($phpfile) !== 'index.php'): ?>
								<li><a href="<?php echo basename($phpfile);?>"><i class="fa fa-cogs fa-fw"></i><?php echo basename($phpfile);?></a></li>
							<?php endif; ?>
		            	<?php endforeach; ?>
						<?php foreach($dirs as $dir):?>
								<li><a href="<?php echo basename($dir);?>"><i class="fa fa-cogs fa-fw"></i><?php echo basename($dir);?></a></li>
		            	<?php endforeach; ?>
						<?php foreach($additional_links as $additional_link_label => $additional_link):?>
								<li><a target="_blank" href="<?php echo $additional_link;?>"><i class="fa fa-cogs fa-fw"></i><?php echo $additional_link_label;?></a></li>
		            	<?php endforeach; ?>		            	
		            </ul>
		        	
		        </div>
		        <div class="col-md-3">

		        </div>
		    </div>
		</div>

		

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>