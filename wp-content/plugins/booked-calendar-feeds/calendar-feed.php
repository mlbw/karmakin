<?php

header('Content-type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename=appointment-feed-'.BOOKEDICAL_VERSION.'.ics');

if (isset($_GET['calendar']) && $_GET['calendar']):
	$calendar_id = $_GET['calendar'];
else:
	$calendar_id = false;
endif;

$today_timestamp = strtotime(date('Y-m-01'));
$one_year_later_timestamp = strtotime(date('Y-m-d', strtotime("+365 days")));

$args = array(
	'post_type' => 'booked_appointments',
	'posts_per_page' => -1,
	'post_status' => array('publish', 'future'),
	'meta_query' => array(
		array(
			'key'     => '_appointment_timestamp',
			'value'   => array( $today_timestamp, $one_year_later_timestamp ),
			'compare' => 'BETWEEN',
		)
	)
);

if ($calendar_id):
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'booked_custom_calendars',
			'field'    => 'id',
			'terms'    => $calendar_id,
		)
	);
endif;

if ($calendar_id):
	$calendar_name = get_term_by('id',$calendar_id,'booked_custom_calendars');
	$calendar_name = $calendar_name->name;
else :
	$calendar_name = 'Appointments';
endif;

$appts_in_this_timeslot = array();

$bookedAppointments = new WP_Query($args);
if($bookedAppointments->have_posts()):
	while ($bookedAppointments->have_posts()):
		$bookedAppointments->the_post();
		global $post;
		$timestamp = get_post_meta($post->ID, '_appointment_timestamp',true);
		$timeslot = get_post_meta($post->ID, '_appointment_timeslot',true);
		$user_id = get_post_meta($post->ID, '_appointment_user',true);
		$day = date('d',$timestamp);
		$appointments_array[$post->ID]['post_id'] = $post->ID;
		$appointments_array[$post->ID]['timestamp'] = $timestamp;
		$appointments_array[$post->ID]['timeslot'] = $timeslot;
		$appointments_array[$post->ID]['status'] = $post->post_status;
		$appointments_array[$post->ID]['user'] = $user_id;
		$appts_in_this_timeslot[] = $post->ID;
	endwhile;
endif;

?>BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//getbooked.io//Booked Calendar
CALSCALE:GREGORIAN
<?php if (!empty($appts_in_this_timeslot)):

	foreach($appts_in_this_timeslot as $appt_id):
	
		$guest_name = get_post_meta($appt_id, '_appointment_guest_name',true);
		$guest_email = get_post_meta($appt_id, '_appointment_guest_email',true);
		
		if (!$guest_name):
			
			// Customer Information
			$user_id = $appointments_array[$appt_id]['user'];			
			$user_info = get_userdata($appointments_array[$appt_id]['user']);
			$user_info = get_userdata($user_id);
			$display_name = booked_get_name($user_id);
			$email = $user_info->user_email;
		
		else:
		
			$display_name = $guest_name;
			$email = $guest_email;
			
		endif;
	
		// Appointment Information
		if (isset($appt_id)):
		
			$time_format = get_option('time_format');
			$date_format = get_option('date_format');
			$appt_id = $appt_id;

			$timestamp = get_post_meta($appt_id, '_appointment_timestamp',true);
			$timeslot = get_post_meta($appt_id, '_appointment_timeslot',true);
			$cf_meta_value = get_post_meta($appt_id, '_cf_meta_value',true);
			
			$date_display = date_i18n($date_format,$timestamp);
			$day_name = date_i18n('l',$timestamp);
			
			$timeslots = explode('-',$timeslot);
			$time_start = date_i18n('H:i:s',strtotime($timeslots[0]));
			$time_start_string = date_i18n($time_format,strtotime($timeslots[0]));
			$time_end = date_i18n('H:i:s',strtotime($timeslots[1]));
			$time_end_string = date_i18n($time_format,strtotime($timeslots[1]));
			
			if ($timeslots[0] == '0000' && $timeslots[1] == '2400'):
				$formatted_start_date = dateToCal(get_post_meta($appt_id, '_appointment_timestamp',true),true);
				$formatted_end_date = false;
			else :
				$end_date = date_i18n('Y-m-d',strtotime(get_gmt_from_date(date_i18n('Y-m-d H:i:s',get_post_meta($appt_id, '_appointment_timestamp',true)))));
				$end_date_time = $end_date . date_i18n('H:i:s',strtotime(get_gmt_from_date(date_i18n('Y-m-d H:i:s',strtotime($end_date.' '.$timeslots[1])))));
				$formatted_start_date = dateToCal(get_post_meta($appt_id, '_appointment_timestamp',true));
				$formatted_end_date = date_i18n('Ymd\THis',strtotime($end_date_time));
			endif;
			
?>BEGIN:VEVENT
DTSTAMP:<?php echo $formatted_start_date; ?>Z
<?php if ($formatted_end_date): ?>
DTSTART:<?php echo $formatted_start_date; ?>Z
DTEND:<?php echo $formatted_end_date; ?>Z
<?php else: ?>
DTSTART;VALUE=DATE:<?php echo $formatted_start_date; ?>

<?php endif; ?>
SUMMARY:<?php echo $display_name; ?>

<?php if ($email || $cf_meta_value): ?>DESCRIPTION:<?php echo $email ? $email.'\r\n' : ''; echo $cf_meta_value ? cleanupCustomFields($cf_meta_value) : ''; ?><?php endif; ?>

UID:booked-appointment-<?php echo $appt_id; ?>

END:VEVENT
<?php
			
		endif;
				
	endforeach;
	
endif;
	
?>END:VCALENDAR<?php

/* Convert Dates */
function dateToCal($timestamp,$all_day = false) {
	if ($all_day):
		return date_i18n('Ymd',strtotime(get_gmt_from_date(date_i18n('Y-m-d H:i:s',$timestamp))));
	else:
		return date_i18n('Ymd\THis',strtotime(get_gmt_from_date(date_i18n('Y-m-d H:i:s',$timestamp))));
	endif;
}

/* Convert Custom Fields */
function cleanupCustomFields($string) {
	$string = '\r\n'.$string;
	return strip_tags( str_replace(array('<br>','<br />','</p>'),array('\r\n
 ','\r\n
 ','\r\n'), preg_replace('/([\,;])/','\\\$1', $string) ) );
}