<?php 
//Template version: 1.7

$display_save_button = false;
$field_id = 0;


/*$exists_at_least_one_attachment_to_show = 0;
 foreach($file_order_metadata as $file_fields)
{
	$current_status = "wc-".$order->get_status( );
	if( !isset($file_fields['order-attachment-hide-by-status']) || !in_array($current_status, $file_fields['order-attachment-hide-by-status']))
		$exists_at_least_one_attachment_to_show++;
} */

if($exists_at_least_one_attachment_to_show > 0):
?>
<div id="wcam-frontend-box">
	<h2 id="wcam-title"><?php echo $title; ?> </h2>
	<form method="post" id="wcam-form-box">
	<input type="hidden" name="wcam_save_customer_feedbacks" value="yes"></input>
	
	<!-- Attachments loop -->
		<?php 
		
		foreach($file_order_metadata as $file_fields):  
		
		?>
		<div class="wcam_responsive_column"> <!-- wcam_responsive_column -->	
		<?php 
		    $current_status = "wc-".$order->get_status( );
			if(isset($file_fields['order-attachment-hide-by-status']) && in_array($current_status, $file_fields['order-attachment-hide-by-status']))
				continue;
			
			$field_id++;
			//$feedback_required = isset($file_fields['customer-feedback-required']) ? $file_fields['customer-feedback-required'] : 'no';
			$feedback_strategy = isset($file_fields['customer-feedback-strategy']) ? $file_fields['customer-feedback-strategy'] : 'never';
			?>
			<input type="hidden" name="wcam_attachment_id[<?php echo $file_fields['id']; ?>]" value="<?php echo $file_fields['id']; ?>"></input>
			<h4 class="wcam-attachment-title"><?php echo $file_fields['title']; ?></h4> 
			
			<?php if(!$wcam_order_model->is_attachment_expired($file_fields, WCAM_Order::get_date_created($order))): ?>
					<!-- Preview Image or Download/View Link -->
					<?php if($preview_image == 'yes' && isset($file_fields['absolute_path']) && wcam_is_image($file_fields['absolute_path'])): ?>
						<a target="<?php echo $link_open_method; ?>" href="<?php echo $file_fields['url']?>" <?php if($use_lightbox_preview) echo 'data-title="'.str_replace('"', "", $file_fields['title']).'"  data-lightbox="wcam-attachments"';?>><img src="<?php echo $file_fields['url']?>" width="<?php echo $preview_image_width; ?>" height="<?php echo $preview_image_height; ?>" class="wcam-image-preview"></img></a>
					<?php else: ?>
						<a target="<?php echo $link_open_method; ?>" class="button button-primary wcam-donwload-button" href="<?php echo $file_fields['url']; ?>"><?php echo $download_view_button_text; ?></a>
					<?php endif; ?>
					<!-- End Preview Image or Download/View Link -->
					<!-- Attach file URL to clipboad button -->
					<?php if(isset($file_fields['display-copy-to-clipboard-button']) && $file_fields['display-copy-to-clipboard-button'] == 'yes'):?>
					<a target="<?php echo $link_open_method; ?>" class="button button-primary wcam-clipboard-button" data-file-url="<?php echo $file_fields['url']; ?>" href="#"><?php echo $copy_to_clipboard_button_text; ?></a>
					<?php endif; ?>
					<!-- Preview file URL to clipboad button-->
					
					<!-- Successful message print -->
					<?php if( isset($feedback_has_been_saved[$file_fields['id']])): ?>
						<div class="wcam_successful_message"><?php _e('Feedback successfully saved', 'woocommerce-attach-me');?></div>
					<?php endif; ?>
					
					<!-- Approval Box -->
					<?php if(isset($file_fields['customer-has-to-be-approved']) && $file_fields['customer-has-to-be-approved'] == 'yes')
							 { 
								if( (!isset($file_fields['customer-has-approved']) || $file_fields['customer-has-approved'] == 'none') ||
									 (isset($file_fields['customer-can-reapprove']) && $file_fields['customer-can-reapprove'] == 'yes'))
								{ ?>
									<label><?php echo $approve_box_title; ?></label>
						  <?php }else{ ?>
									<label><?php 
									
									echo $approve_already_sent_message." "; 
									switch($file_fields['customer-has-approved'])
									{
										case "none": echo $approve_not_yet_text; break;
										case "yes": echo $approve_yes_text; break;
										case "no": echo $approve_no_text; break;
									} ?></label>
								<?php } 
								
								if( (!isset($file_fields['customer-has-approved']) || $file_fields['customer-has-approved'] == 'none') || 
									(isset($file_fields['customer-can-reapprove'])  && $file_fields['customer-can-reapprove'] == 'yes') )
									{
										$display_save_button = true;
										if($select_box_style == 'select')
										{ ?>
											<select name="wcam_customer_approval[<?php echo $file_fields['id']; ?>]" class="wcam_customer_approval" data-id="<?php echo $field_id; ?>">
											  <option value="none" <?php if(!isset($file_fields['customer-has-approved']) || $file_fields['customer-has-approved'] == 'none') echo 'selected';?>><?php echo $approve_not_yet_text; ?></option>
											  <option value="yes" <?php if(isset($file_fields['customer-has-approved']) && $file_fields['customer-has-approved'] == 'yes') echo 'selected';?>><?php  echo $approve_yes_text; ?></option>
											  <option value="no" <?php if(isset($file_fields['customer-has-approved']) && $file_fields['customer-has-approved'] == 'no') echo 'selected';?>><?php  echo $approve_no_text; ?></option>
											</select>
										<?php }
											else{ ?>
											  <input class="wcam-radio-button" type="radio" name="wcam_customer_approval[<?php echo $file_fields['id']; ?>]" value="none" <?php if(!isset($file_fields['customer-has-approved']) || $file_fields['customer-has-approved'] == 'none') echo 'checked="checked"';?>> <?php echo $approve_not_yet_text; ?></input><br/>
											  <input class="wcam-radio-button" type="radio" name="wcam_customer_approval[<?php echo $file_fields['id']; ?>]" value="yes" <?php if(isset($file_fields['customer-has-approved']) && $file_fields['customer-has-approved'] == 'yes') echo 'checked="checked"';?>> <?php echo $approve_yes_text; ?></input><br/>
											  <input class="wcam-radio-button" type="radio" name="wcam_customer_approval[<?php echo $file_fields['id']; ?>]" value="no" <?php if(isset($file_fields['customer-has-approved']) && $file_fields['customer-has-approved'] == 'no') echo 'checked="checked"';?>> <?php echo $approve_no_text; ?></input><br/>
								
										<?php }
									}
							} ?>
					<!-- End Approval Box -->
							
					<?php  //Feedback
						  if(isset($file_fields['customer-feedback-enabled']) && $file_fields['customer-feedback-enabled'] == 'yes'): 
							$display_save_button = true;?>
							<label><?php echo $feedback_box_title ?></label>
							<textarea class="wcam-feedback-textfield" id="wcam-feedback-textfield-<?php echo $field_id; ?>" data-required-strategy="<?php echo $feedback_strategy; ?>" name ="wcam_customer_feedback[<?php echo $file_fields['id']; ?>]" <?php if($feedback_strategy == 'always') echo 'required="required"'; ?>><?php if(isset($file_fields['customer-feedback'])) echo $file_fields['customer-feedback']; ?></textarea>
					<?php endif; //End Feedback ?>
					
				<?php endif; //End is expired  ?>
				
				<?php if($wcam_order_model->get_attachment_expiration_date($order_id, $file_fields) != ""): ?>
						<strong class="wcam_expiring_title"><?php _e('Expires on','woocommerce-attach-me'); ?></strong>
						<?php echo $wcam_order_model->get_attachment_expiration_date($order_id, $file_fields); 
					endif; ?>
						
			</div> <!-- End responsive column -->
		<?php endforeach;?>
		<!-- End Attachments loop -->
		
		<?php if($display_save_button): ?>
			<input id="wcam-save-button" type="submit" class="button" value="<?php _e('Save', 'woocommerce-attach-me'); ?>"></input>
		<?php endif; ?>
	</form>
</div>

<?php endif /* end exists_at_least_one_attachment_to_show */  ?>
<script>
	 wcam_copy_to_clipboard_text = "<?php _e('Copy to clipboard: Ctrl+C, Enter','woocommerce-attach-me'); ?>";
</script>