<?php 
//Template version: 1.5

if(!empty($products_data)):
?>
<div id="wcam-frontend-products-attachments">
	<h2><?php echo $product_attachments_box_title; ?></h2>
	<?php foreach($products_data as $product_name => $attachments): ?>
	<div class="wcam_product_title_attachments_block wcam_responsive_column"> <!-- wcam_responsive_column -->
		<h3 class="wcam_product_name_title"><?php echo $product_name; ?></h3>
			<div class="wcam-products-attachments-list">
				<?php foreach((array)$attachments as $attachment): //get_status( ) ?>
					<div class="wcam-products-attachments-item wcam_responsive_column"><!-- wcam_responsive_column -->
						<h4><?php echo $attachment['file_name']; ?></h4>
						<?php echo $attachment['file_description']; ?>
							<!--	<h4><?php echo $attachment['file_name'];?></h4>	-->		
								
							<?php if(!$attachment['has_expired']): ?>
								<?php if($preview_image == 'yes' && wcam_is_image($attachment['file_url'])): ?>
										<a target="<?php echo $link_open_method; ?>" href="<?php echo get_site_url()."?wcam_attachment_id=".$order_id."-".$attachment['id']; ?>" <?php if($use_lightbox_preview) echo 'data-title="'.str_replace('"', "", $attachment['file_name']).'"  data-lightbox="wcam-attachments"';?>><img src="<?php echo get_site_url()."?wcam_attachment_id=".$order_id."-".$attachment['id']; ?>" width="<?php echo $preview_image_width; ?>" height="<?php echo $preview_image_height; ?>" class="wcam-image-preview"></img></a>
								<?php else: ?>
										<a target="<?php echo $link_open_method; ?>" class="button wcam-products-attachment-button" href="<?php echo get_site_url()."?wcam_attachment_id=".$order_id."-".$attachment['id']; ?>"><?php echo _e('Download/View','woocommerce-attach-me'); ?></a>
								<?php endif; ?>
							<?php endif; ?>
							
							<?php //Not used
								if($attachment['expiring_date'] != ""): ?>
								<strong class="wcam_expiring_title"><?php _e('Expires on','woocommerce-attach-me'); ?></strong>
								<?php echo wcam_format_dateime_obj_according_wordpress($attachment['expiring_date']);  
							endif; ?>
					</div>
				<?php endforeach; //end product attachment list ?>
			</div>
	 </div> <!-- end responsive column -->
	<?php endforeach; //end product ?>
</div>
<?php endif; ?>