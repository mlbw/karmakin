jQuery(document).ready(function()
{
	wcam_check_if_to_show_embed_links_section(null);
	jQuery(document).on('change', '#embed_links_to_complete_mail', wcam_check_if_to_show_embed_links_section);
});
function wcam_check_if_to_show_embed_links_section(event)
{
	if(jQuery('#embed_links_to_complete_mail').prop('checked'))
		jQuery('#embed_links_to_complete_mail_box').show();
	else
		jQuery('#embed_links_to_complete_mail_box').hide();
}