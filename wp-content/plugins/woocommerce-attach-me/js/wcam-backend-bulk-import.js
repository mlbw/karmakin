jQuery(document).ready(function()
{
	var error = null;
	var data = null;
	var current_index_data_to_send = 0;
	var total_data_to_send = 0;
	var max_row_chunk = 50;
	var current_row_chunk = 0;
	var last_row_chunk = 0;
	jQuery('#wcam-reimport-csv-button').fadeOut(0);
	jQuery('#wcam-import-csv-button').click(wcam_transition_out);
	jQuery('#wcam-file').on('change', wcam_load_csv_file);
	jQuery('#wcam-reimport-csv-button').on('click', function(){location.reload(); });
	
	function wcam_load_csv_file(evt)
	{
		
		if (!wcam_browserSupportFileUpload()) {
			alert('The File APIs are not fully supported in this browser!');
        } else {
            data = null;
            var file = evt.target.files[0];
            var reader = new FileReader();
            reader.readAsText(file);
            reader.onload = function(event) 
			{
                var csvData = event.target.result;
				try{
                data = jQuery.csv.toArrays(csvData);
				}catch(e){error = e;}
                if (data && data.length > 0) {
                  //alert('Imported -' + data.length + '- rows successfully!');
				  total_data_to_send = data.length;
				  //console.log();
                } else {
                    alert('No data to import! Error: '+error);
                }
            };
            reader.onerror = function() {
                alert('Unable to read ' + file.fileName);
            };
        }
	}
	function wcam_browserSupportFileUpload() 
	{
        var isCompatible = false;
        if (window.File && window.FileReader && window.FileList && window.Blob) {
        isCompatible = true;
        }
        return isCompatible;
    }
	function wcam_transition_out(e)
	{
		//console.log("Transition out...");
		if(error !=null || data == null)
		{
			 alert('File is not valid');
			return;
		}
		e.preventDefault();
		e.stopImmediatePropagation();
		current_index_data_to_send = 0;
		last_row_chunk = 1;
		current_row_chunk = max_row_chunk;
		jQuery('#wcam-result-box').delay(700).fadeIn(800);
		jQuery('#wcam-import-csv-button').fadeOut(800);
		jQuery('#wcam-file-upload-box').fadeOut(800, function(){setTimeout(wcam_start_upload_csv, 1000); });
		
		return false;
	}
	function wcam_start_upload_csv()
	{
		//console.log("Starting...");
		var dataToSend =  [];
		dataToSend.push(data[0]);
		for(var i = last_row_chunk;  i < current_row_chunk; i++)
		{
			//console.log("Row: "+i);
			dataToSend.push(data[i]);
		}
		
		wcam_upload_csv(dataToSend);
	}
	function wcam_upload_csv(dataToSend)
	{
		var formData = new FormData();
		formData.append('action', 'wcam_upload_attachments_csv'); 
		formData.append('csv', dataToSend.join("<#>")); 
		var perc_num = ((current_row_chunk/total_data_to_send)*100);
		perc_num = perc_num > 100 ? 100:perc_num;
		
		var perc = Math.floor(perc_num);
		jQuery('#ajax-progress').html("<p>computing data, please wait...<strong>"+perc+"% done</strong></p>");
		jQuery( "#progressbar" ).progressbar({
					  value: perc
					});
					
		jQuery.ajax({
			url: ajaxurl, //defined in php
			type: 'POST',
			data: formData,//{action: 'upload_csv', csv: data_to_send},
			async: true,
			success: function (data) {
				//alert(data);
				wcam_check_response(data);
			},
			error: function (request, error) {
				if(max_row_chunk > 10)
				{
					//console.log("error");
					max_row_chunk -= 10;
					current_row_chunk -= 10;
					wcam_start_upload_csv();
				}
				else
					alert("Server error, response: "+error);
			},
			cache: false,
			contentType: false,
			processData: false
		});
			
	}
	
	function wcam_check_response(data)
	{
		jQuery('#ajax-response').append("<p>"+data+"</p>");
		//console.log(current_row_chunk+" "+total_data_to_send);
		if(current_row_chunk < total_data_to_send)
		{
			last_row_chunk = current_row_chunk;
			current_row_chunk += max_row_chunk;
			if(current_row_chunk > total_data_to_send)
				current_row_chunk = total_data_to_send;
			
			//console.log("continuing...");
			wcam_start_upload_csv();
		}
		else
		{
			//console.log("End");
			jQuery( "#progressbar" ).progressbar({
			  value: 100
			});
			jQuery('#ajax-progress').append("<p>100% done</p> <h3>end!</h3>");
			jQuery('#wcam-reimport-csv-button').fadeIn(500);
		}
	}

});