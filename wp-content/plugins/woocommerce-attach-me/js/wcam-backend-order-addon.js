var wcaf_remove_index = 0;
var attachment_ids = new Array();
//var wcaf_index = <?php echo ++$counter ?>; defined in php
var wcam_attachments_list_already_populated = false; 
jQuery(document).ready(function()
{
	wcam_textarea_setup();
	jQuery(document).on('click',"#wcam-save-button", wcam_transition_out);
	jQuery(document).on('change', "#wcam-notify-checkbox", wcam_show_notify_box);
	jQuery(document).on('change', "#wcam-embed-links-checkbox", wcam_show_embed_links_box);
	jQuery(document).on('change', ".wcam-customer-approve-checkbox", wcam_feedback_approval_checbox);
	jQuery(document).on('change', ".wcam-customer-feedback-checkbox", wcam_feedback_approval_checbox);
	jQuery(document).on('change', ".wcam-customer-admin-notification-checkbox", wcam_feedback_approval_checbox);
	jQuery(document).on('change', ".wcam-attach-file-to-complete-order-email", wcam_feedback_approval_checbox);
	jQuery('#wcam-notify-checkbox').attr('checked', false);
	jQuery('.wcam-gallery-attachment').val('');
	jQuery(document).on('click','.wccm_advanced_option_button', wcam_on_adavanced_options_button_click);
	jQuery(document).on('change', '#wcam_load_preset_notification_email, #wcam_load_preset_embedded_email',wcam_load_emay_preset_body);
	
	wcam_show_embed_links_box(null);
	
	jQuery('#wcam-attachment-ids').select2({ width: '100%' }).on("select2:open", wcam_set_attachment_ids_list).on("select2:select", wcam_selected_id)
        .on("select2:unselect", wcam_removed_id).on("select2-opening", wcam_set_attachment_ids_list).on("select2-selecting", wcam_selected_id)
        .on("select2-removed", wcam_removed_id);
	
	jQuery(document).on('focus', '.file_input', function(event){ jQuery(event.currentTarget).parent().find('input[type="radio"]').eq(0).prop('checked', true);});
	jQuery(document).on('focus', '.wcam-external-url', function(event){ jQuery(event.currentTarget).parent().find('input[type="radio"]').eq(1).prop('checked', true);});
	jQuery(document).on('focus', '.wcam-browse-link, .wcam-gallery-input', function(event){jQuery(event.currentTarget).parent().find('input[type="radio"]').eq(2).prop('checked', true);});
	
	jQuery(document).on('click','.wcam_collapse_expand_button', wcam_collapse_or_expand_attachment_box);
	
	//Expiration
	wcam_init_datetime_picker();
	jQuery(document).on('change', '.wcam_expiration_strategy', wcam_show_expiration_time_selection);
	jQuery('#wcam_expiration_strategy_0').trigger('change');
	
	//Other
	wcam_collapse_boxes();
	wcam_preload_preset_text();
});
function wcam_show_expiration_time_selection(event)
{
	var id = jQuery(event.currentTarget).data('id');
	var value = jQuery(this).val();
	switch(value)
	{
		case 'never':
					jQuery('#wcam_expiration_relative_time_amout_selection_box_'+id+', #wcam_expiration_specific_date_selection_box_'+id).hide();
					jQuery('#wcam-secure-download-'+id).removeAttr('disabled');
					break;
		case 'specific_date':
					jQuery('#wcam_expiration_relative_time_amout_selection_box_'+id).hide();
					jQuery('#wcam_expiration_specific_date_selection_box_'+id).show();
					jQuery('#wcam-secure-download-'+id).prop('disabled', true);
					jQuery('#wcam-secure-download-'+id).prop('checked', true);
				break;
		case 'relative_date':
					jQuery('#wcam_expiration_relative_time_amout_selection_box_'+id).show();
					jQuery('#wcam_expiration_specific_date_selection_box_'+id).hide();
					jQuery('#wcam-secure-download-'+id).prop('disabled', true);
					jQuery('#wcam-secure-download-'+id).prop('checked', true);
				break;
	}
}
function wcam_preload_preset_text()
{
	if(jQuery('#wcam_load_preset_embedded_email').val() != "text_input")
		//dealy to wait tinyMCE has loaded
		setTimeout(function(){jQuery('#wcam_load_preset_embedded_email').trigger('change')},3000);
}
function wcam_collapse_boxes()
{
	jQuery('.wcam-attachment-box-content').each(function(index, elem)
	{
		if(jQuery(elem).data('already-collapsed') == 'yes')
		{
			jQuery(elem).hide();
		}
	});
}
function wcam_collapse_or_expand_attachment_box(event)
{
	var id = jQuery(event.target).data('id');
	if(jQuery('#wcam-attachment-box-content-'+id).is(":visible"))
	{
		jQuery('#wcam-attachment-box-content-'+id).hide();
		jQuery(event.target).html('+');
	}
	else 
	{
		jQuery('#wcam-attachment-box-content-'+id).show();
		jQuery(event.target).html('-');
	}
}
function wcam_load_emay_preset_body(event)
{
	event.stopImmediatePropagation();
	event.preventDefault();
	var id = jQuery(event.target).data('id');
	var selected_value = event.target.value;
	if(selected_value == "input_text")
		return false;
	//UI
	var loader = '<div class="wcam_temp_loader" style="margin-top:10px;">';
	loader += loader_path;
	loader += '</div>';
	jQuery(event.target).parent().append(loader);
	
	//get via ajax
	var formData = new FormData();
	formData.append('action', 'wcam_get_preset_email_text');
	formData.append('wcam-preset-id', selected_value);
	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		data: formData,
		async: false,
		success: function (data) {
			tinyMCE.get(id).setContent(data);
			jQuery('.wcam_temp_loader').remove();
		},
		error: function (data) {
			alert("error: "+data);
		},
		cache: false,
		contentType: false,
		processData: false
	});
}
function wcam_on_adavanced_options_button_click(event)
{
	event.preventDefault();
	event.stopImmediatePropagation();
	var box_id = jQuery(event.currentTarget).data('id');
	jQuery('#wcam-advanced-options-box-'+box_id).toggle();
	return false;
}
function wcam_selected_id(e)
{
	if(typeof e.params !== 'undefined')
		attachment_ids.push(e.params.data.id);
	else
		attachment_ids.push(e.val);
}
function wcam_removed_id(e)
{
	var index ;
	
	if(typeof e.params !== 'undefined')
		index = attachment_ids.indexOf(e.val);
	else
	    index =  attachment_ids.indexOf(e.params.data.id);
	if (index > -1) 
		attachment_ids.splice(index, 1);
	
}
function wcam_set_attachment_ids_list(e)
{
	if(wcam_attachments_list_already_populated)
		return;
	var already_uploaded;
	var id;
	var title;
	jQuery('#wcam-attachment-ids').empty();
	var updated_options = [];
	
	jQuery.each(jQuery('.wcam-attachment-box'), function(index,value)
	{
		already_uploaded = jQuery(this).data('already-uploaded');
		if(already_uploaded == 'yes' && jQuery(this).data('is-external') == 'no')
		{
			id = jQuery(this).data('id').toString();
			title = jQuery(this).find('#wcam_attachment_title_'+id+' .real-title').html();
			//if(jQuery.inArray(id, attachment_ids) == -1)
				jQuery('#wcam-attachment-ids').append('<option value="'+id+'">'+title+'</option>');
		}
		else if((jQuery(this).find('input[type="radio"]').eq(0).prop('checked') && jQuery(this).find('input[type="file"]').val() != "") || 
				(jQuery(this).find('input[type="radio"]').eq(2).prop('checked') && jQuery(this).find('.wcam-gallery-input').val() != ""))
		{
			id = jQuery(this).data('id').toString();
			title = jQuery(this).find('#wcam_attachment_title_'+id).val();
			//if(jQuery.inArray(id, attachment_ids) == -1)
			{
				jQuery('#wcam-attachment-ids').append('<option value="'+id+'">'+title+'</option>');
			}
		}
	});
	wcam_attachments_list_already_populated = true;
}
function wcam_textarea_setup()
{
	setTimeout(function(){  tinymce.PluginManager.load('code', wcam_plugin_path+'/js/tinymce/plugins/code/plugin.min.js');
							tinymce.init({selector:'#wcam-message,.wcam-embed-link',  
										  plugins : "paste,code",  
										 id: "wcam-text-editor",
										 body_id: "wcam-body-message",
										 paste_as_text: true }); }, 1500);
}
function wcam_feedback_approval_checbox(e)
{
if(jQuery(this).attr('checked'))
{
	jQuery(this).val("yes");
	jQuery(this).attr("value", "yes");
}
else
{
	jQuery(this).val("no");
	jQuery(this).attr("value", "no");
}
	
}
function wcam_show_notify_box(e)
{
	if(jQuery('#wcam-notify-checkbox').attr('checked'))
	{
		jQuery('#wcam-notify-subbox').fadeIn();
		jQuery('#wcam-notify-checkbox').val("yes");
		jQuery('#wcam-notify-checkbox').attr("value", "yes");
	}
	else
	{
		jQuery('#wcam-notify-subbox').fadeOut();
		jQuery('#wcam-notify-checkbox').val("no");
		jQuery('#wcam-notify-checkbox').attr("value", "no");
	}
	
}
function wcam_show_embed_links_box(e)
{
	if(jQuery('#wcam-embed-links-checkbox').attr('checked'))
	{
		jQuery('#wcam-embed-link-subbox').fadeIn();
		jQuery('#wcam-embed-links-checkbox').val("yes");
		jQuery('#wcam-embed-links-checkbox').attr("value", "yes");
	}
	else
	{
		jQuery('#wcam-embed-link-subbox').fadeOut();
		jQuery('wcam-embed-links-checkbox').val("no");
		jQuery('#wcam-embed-links-checkbox').attr("value", "no");
	}
	
}
function wcam_transition_out(e)
{
	
e.preventDefault();
e.stopImmediatePropagation();

if(jQuery('#wcam-notify-checkbox').attr('checked'))
{
	var can_send = 0;
	try{
		if (tinymce.editors['wcam-message'].getContent({format: 'text'}) != '')
			 can_send++;
	}catch(error){}
	
	if(jQuery('#wcam-notification-subject').val() != '')
		can_send++;
	if(can_send < 2)
	{
		alert(send_message_error);
		return false;
	}
}
	
var loader = '<div id="wcam-loading" style="margin-top:10px;">';
loader += loader_path;
loader += '</div>';
jQuery('html, body').animate({scrollTop: jQuery( '#woocommerce-attach-me' ).offset().top - 100}, 1000);
jQuery('#wcam-attachments-box').delay(1000).append(loader);
jQuery('#wcam-uploads-data').delay(1000).fadeOut('400', function(){jQuery('#wcam-loading').fadeIn('400', wcam_upload_data)});

return false;
}
function wcam_upload_data()
{
	var formData = new FormData();
	formData.append('action', 'upload_attachments');
	formData.append('wcam-orderid', jQuery('#wcam-orderid').val());

	//file
	jQuery('#wcam-attachments-box input[type="file"]').each(function() 
	{
		if (jQuery(this).val() != '' && jQuery(this).parent().find('input[type="radio"]').eq(0).prop('checked')) { 
			formData.append(jQuery(this).attr('name'), jQuery(this)[0].files[0]);
		}
	}); 
	jQuery('#wcam-attachments-box input[type="checkbox"]').each(function() 
	{
		if(jQuery(this).attr('checked'))
			formData.append(jQuery(this).attr('name'), jQuery(this).val() );
	}); 
	jQuery('#wcam-attachments-box input[type="number"]').each(function() 
	{
		formData.append(jQuery(this).attr('name'), jQuery(this).val() );
	});
	jQuery('#wcam-attachments-box input[type="text"]').each(function() 
	{
		formData.append(jQuery(this).attr('name'), jQuery(this).val() );
	});
	jQuery('#wcam-attachments-box select').each(function() 
	{
		formData.append(jQuery(this).attr('name'), jQuery(this).val() );
	}); 
	jQuery('.wcam-attachments-titles, #wcam-attachments-box input[type="hidden"]').each(function() 
	{
		formData.append(jQuery(this).attr('name'), jQuery(this).val() );
	}); 
	//ext url
	jQuery('#wcam-attachments-box .wcam-external-url').each(function() //,#wcam-attachments-box textarea
	{
		if (jQuery(this).val() != '' && jQuery(this).parent().find('input[type="radio"]').eq(1).prop('checked')) { 
			formData.append(jQuery(this).attr('name'), jQuery(this).val() );
		}
	}); 
	//media
	jQuery('#wcam-attachments-box .wcam-gallery-input').each(function()
	{
		if (jQuery(this).val() != '' && jQuery(this).parent().find('input[type="radio"]').eq(2).prop('checked')) { 
			formData.append(jQuery(this).attr('name'), jQuery(this).val() );
		}
	}); 
	if(jQuery('#wcam-notify-checkbox').attr('checked'))
	{
		  formData.append('wcam-notification-message', tinymce.editors['wcam-message'].getContent({format: 'raw'} ));
		  formData.append('wcam-notification-subject', jQuery('#wcam-notification-subject').val());
	}
	if(jQuery('#wcam-embed-links-checkbox').attr('checked'))
	{
		  formData.append('wcam-embed-links-to-complete-mail-text', tinymce.editors['wcam-embed-links-to-complete-mail-text'].getContent({format: 'raw'} ));
	}
	
	if(typeof attachment_ids !== 'undefined' && attachment_ids.length > 0)
		formData.append('wcam-attachment-ids',attachment_ids.join());


	jQuery.ajax({
		url: ajaxurl,
		type: 'POST',
		data: formData,
		async: false,
		success: function (data) {
			//alert(data);
			tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'wcam-message');
			tinymce.EditorManager.execCommand('mceRemoveEditor',true, 'wcam-embed-links-to-complete-mail-text');
			jQuery('#wcam-notify-checkbox').attr('checked', false);
			jQuery('#wcam-attachments-box').replaceWith(data);
			jQuery('#wcam-attachments-box').fadeIn();
			jQuery('#wcam-loading').fadeOut();
			tinymce.EditorManager.execCommand('mceAddEditor',true, 'wcam-message');
			tinymce.EditorManager.execCommand('mceAddEditor',true, 'wcam-embed-links-to-complete-mail-text');
			wcam_show_embed_links_box(null);
			attachment_ids = new Array();
			
		wcam_attachments_list_already_populated = false;	
			jQuery('#wcam-attachment-ids').select2({ width: '100%' }).on("select2:open", wcam_set_attachment_ids_list).on("select2:select", wcam_selected_id)
        .on("select2:unselect", wcam_removed_id)
		.on("select2-opening", wcam_set_attachment_ids_list).on("select2-selecting", wcam_selected_id)
        .on("select2-removed", wcam_removed_id);
	
		},
		error: function (data) {
			alert("error: "+data);
		},
		cache: false,
		contentType: false,
		processData: false
	});
	return false;
}
function wcam_init_datetime_picker()
{
	jQuery(".wcam_expiration_specific_date").datetimepicker({dateFormat:'yy/mm/dd', timeFormat: "HH:mm:ss"});
}
function wcam_remove_field(e) 
{ 
  /*  console.log(e.target); */
   e.preventDefault();
   if(confirm(confirm_message))
   {
	   jQuery("#wcam-attachments-box").append( '<input type="hidden" name="wcam_attachments_to_delete['+wcaf_remove_index+']" value="'+jQuery(e.target).data('fileid')+'"></input>');
	   jQuery(e.target).parent().parent().remove();
	   wcaf_remove_index++;
   }
}
