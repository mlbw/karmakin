jQuery(document).ready(function()
{
  var file_frame; 
  var file_frame_watermark; 
  var current_index = 0;
  var curr_id;
  jQuery('.wcam-browse-link').live('click', function( event )
  {
	curr_id = jQuery(this).data('id');
    event.preventDefault();

    // If the media frame already exists, reopen it.
    if ( file_frame ) {
      file_frame.open();
      return;
    }

    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: true  // Set to true to allow multiple files to be selected
    });

    file_frame.on( 'select', function() 
	{
 
		var selection = file_frame.state().get('selection');
	 	selection.map( function( attachment ) 
		{
		  attachment = attachment.toJSON();
		  jQuery('#wcam-gallery-link-'+curr_id).val(attachment.url);
		  jQuery('#gallery_media_id_'+curr_id).val(attachment.id);
		  current_index++;
		});
    });
    file_frame.open();
  });
});