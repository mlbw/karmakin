jQuery(document).ready(function()
{
	jQuery('table.shop_table.my_account_orders tbody tr.order').each(function(index)
	{
		var wcam_var = wcam.wc_ver /* parseFloat(wcam.wc_ver) */;
		
		var order_num = wcam_versionCompare(wcam_var, "3.0.0") < 0 ? jQuery(this).find('td.order-number a').html() : jQuery(this).find('td.woocommerce-orders-table__cell-order-number a').html();
		var main_element = wcam_versionCompare(wcam_var, "3.0.0")  < 0 ? jQuery(this).find('td.order-actions') : jQuery(this).find('td.woocommerce-orders-table__cell.woocommerce-orders-table__cell-order-actions');
		
		order_num = order_num.replace('#',' ');
		order_num = order_num.replace(/\s/g, ''); 
		try
		{
			var order_url = wcam.view_order_url.replace('wcam_order_id_place_holder',order_num)
			var button_element = jQuery('<a href="'+order_url+'/?wcam_view_attachments=true" class="button wcam-view-attachments-button" >'+wcam.view_attachments+'</a>');
			if(wcam.order_to_num_of_attachments[order_num].order_attachmentes > 0 || wcam.order_to_num_of_attachments[order_num].product_attachmentes > 0)
				main_element.append(button_element);	
		}catch(err){}
		
	});
});

function wcam_versionCompare(a, b) {
    var i, cmp, len, re = /(\.0)+[^\.]*$/;
    a = (a + '').replace(re, '').split('.');
    b = (b + '').replace(re, '').split('.');
    len = Math.min(a.length, b.length);
    for( i = 0; i < len; i++ ) {
        cmp = parseInt(a[i], 10) - parseInt(b[i], 10);
        if( cmp !== 0 ) {
            return cmp;
        }
    }
    return a.length - b.length;
}

function gteVersion(a, b) {
    return cmpVersion(a, b) >= 0;
}
function ltVersion(a, b) {
    return cmpVersion(a, b) < 0;
}