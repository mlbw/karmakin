jQuery(document).ready(function()
{
	jQuery(document).on('change', '.wcam_customer_approval', wcam_check_feedback_required_status);
	jQuery(document).on('click', '.wcam-clipboard-button', wcam_copyToClipboard);
	
	//smooth scrool
	params = wcam_getUrlVars();
	if(params["wcam_view_attachments"] !== undefined)
	{
		var selector =  jQuery('#wcam-frontend-box').length ? '#wcam-frontend-box' : '#wcam-frontend-products-attachments';
		if(!jQuery(selector).length)
			selector = '#wcam-no-attachments-message';
		if(!jQuery(selector).length)
			selector = "";
		
		if(selector != "")
			document.querySelector(selector).scrollIntoView({ 
			  behavior: 'smooth' 
			});
	}
});
function wcam_check_feedback_required_status(event)
{
	var id = jQuery(event.currentTarget).data('id');
	var selected_approval_value = jQuery(event.currentTarget).val();
	var feedback_textfield = jQuery('#wcam-feedback-textfield-'+id);
	var feedback_textfield_strategy = feedback_textfield.data('required-strategy');

	if(feedback_textfield_strategy == 'always' || feedback_textfield_strategy == 'never')
		return false;
	
	switch(feedback_textfield_strategy)
	{
		case 'only_yes': if(selected_approval_value == 'yes') feedback_textfield.prop('required', true); else feedback_textfield.prop('required', false); break;
		case 'only_no': if(selected_approval_value == 'no') feedback_textfield.prop('required', true); else feedback_textfield.prop('required', false); break;
	}
}
function wcam_copyToClipboard(event) 
{
	var file_url = jQuery(event.currentTarget).data('file-url');
	window.prompt(wcam_copy_to_clipboard_text, file_url);
	return false;
}
function wcam_getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}