<?php 
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_56a62076b22f6',
	'title' => 'Attach Me! - Automatic email attachments',
	'fields' => array (
		array (
			'key' => 'field_56a620e08ad7f',
			'label' => 'Attachments',
			'name' => 'wcam_automatic_email_attachments',
			'type' => 'repeater',
			'instructions' => 'Here you can configure the attachments that will be automatically included in outgoing WooCommerce emails. 
To add a new "Automatic attachment"click on the "New automatich attachment" button and then choose to which kind of WooCommerce email the file(s) has to be attached and upload a file.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => 'New automatic attachment',
			'collapsed' => 'field_56a621158ad80',
			'sub_fields' => array (
				array (
					'key' => 'field_56a621158ad80',
					'label' => 'Select in which email types has to be included',
					'name' => 'wcam_email_types',
					'type' => 'checkbox',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'layout' => 'vertical',
					'choices' => array (
						'customer_new_account' => 'New account',
						'new_order' => 'New order email (This email is sent to <strong>admin only</strong> when an order is placed)',
						'customer_on_hold_order' => 'Order placed email (on hold)',
						'customer_processing_order' => 'Processing order email',
						'customer_completed_order' => 'Complete order email',
						'customer_invoice' => 'Invoice email',
					),
					'default_value' => array (
					),
					'allow_custom' => 0,
					'save_custom' => 0,
					'toggle' => 0,
					'return_format' => 'value',
				),
				array (
					'key' => 'field_56a6216a8ad81',
					'label' => 'Select file',
					'name' => 'wcam_selected_file',
					'type' => 'file',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'id',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-automatic-emails-attachments-configurator',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_570678fc3710c',
	'title' => 'Attach Me! - Bulk Products attachments',
	'fields' => array (
		array (
			'key' => 'field_570678fc4173f',
			'label' => 'Attachments',
			'name' => 'wcam_products_attachments',
			'type' => 'repeater',
			'instructions' => 'Choose products and categories to which attach files. Attachments will be downloadable/viewable in the Order details and/or Product pages. In case of visibility on order details page, every file can be visible for different order statuses.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_57067f91e85cd',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Add new attachment bulk rule',
			'sub_fields' => array (
				array (
					'key' => 'field_57067f91e85cd',
					'label' => 'Id',
					'name' => 'wcam_rule_id',
					'type' => 'unique_id',
					'instructions' => 'Unique id used to identy bulk attachment rule. This value is autocatically assigned by the system after the first save.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
				),
				array (
					'key' => 'field_57067ec86cee8',
					'label' => 'Select product(s)',
					'name' => 'wcam_selected_products',
					'type' => 'post_object',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'post_type' => array (
						0 => 'product',
						1 => 'product_variation',
					),
					'taxonomy' => array (
					),
					'allow_null' => 0,
					'multiple' => 1,
					'return_format' => 'id',
					'ui' => 1,
				),
				array (
					'key' => 'field_57067ed36cee9',
					'label' => 'Select product category(ies)',
					'name' => 'wcam_selected_product_categories',
					'type' => 'taxonomy',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'taxonomy' => 'product_cat',
					'field_type' => 'multi_select',
					'multiple' => 0,
					'allow_null' => 1,
					'return_format' => 'id',
					'add_term' => 1,
					'load_terms' => 0,
					'save_terms' => 0,
				),
				array (
					'key' => 'field_57067ed76ceea',
					'label' => 'Assignment strategy',
					'name' => 'wcam_selected_strategy',
					'type' => 'radio',
					'instructions' => 'You can assign a scheduling rule to selected item(s)/category(ies) or to all excluding the selected ones.',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'layout' => 'vertical',
					'choices' => array (
						'all' => 'For all selected item(s)',
						'except' => 'Except the selected item(s)/category(ies)',
					),
					'default_value' => 'all',
					'other_choice' => 0,
					'save_other_choice' => 0,
					'allow_null' => 0,
					'return_format' => 'value',
				),
				array (
					'key' => 'field_57067edb6ceeb',
					'label' => 'Children categories',
					'name' => 'wcam_categories_children',
					'type' => 'radio',
					'instructions' => 'If at least one category has been selected, you can decide to apply the scheduling rule also to children categories items',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'layout' => 'vertical',
					'choices' => array (
						'selected_only' => 'Apply to selected categories only',
						'all_children' => 'Apply to selected categories and all its children',
					),
					'default_value' => 'selected_only',
					'other_choice' => 0,
					'save_other_choice' => 0,
					'allow_null' => 0,
					'return_format' => 'value',
				),
				array (
					'key' => 'field_570678fc465a2',
					'label' => 'Name',
					'name' => 'wcam_file_name',
					'type' => 'text',
					'instructions' => 'Type an name for the attachment. It wil be displayed in the front end next to the download/view button',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_570678fc466e8',
					'label' => 'File',
					'name' => 'wcam_file',
					'type' => 'file',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array (
					'key' => 'field_570678fc4682b',
					'label' => 'Description',
					'name' => 'wcam_file_description',
					'type' => 'textarea',
					'instructions' => 'Optionally you can type a short description',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'new_lines' => 'wpautop',
					'maxlength' => '',
					'placeholder' => '',
					'rows' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_59354757b91bb',
					'label' => 'Visibility',
					'name' => 'wcam_attachment_visibility',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'order_details_page' => 'Order details page',
						'product_page' => 'Product page',
						'all_pages' => 'Both',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_570678fc4696d',
					'label' => 'Order status visibility',
					'name' => 'wcam_order_status_visibility',
					'type' => 'order_staus_selector',
					'instructions' => 'Select order status for which the attachment(s) will be visible.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_value' => 'name',
					'allowed_order_statuses' => '',
					'field_type' => 'checkbox',
				),
				array (
					'key' => 'field_570678fc46aaf',
					'label' => 'Attach file to WooCoommerce outgoing email(s)?',
					'name' => 'wcam_attach_file_to_woocoommerce_status_email',
					'type' => 'select',
					'instructions' => 'The file is automatically attached to ANY outgoing WooCommerce email according to the current order status and the selected status on the previous <strong>Order status visibility</strong> section.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'yes' => 'Yes',
						'no' => 'No',
					),
					'default_value' => array (
						0 => 'no',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_5786385f1b66a',
					'label' => 'File is attached to outgoing emails only?',
					'name' => 'wcam_file_is_only_attachend_to_the_email',
					'type' => 'select',
					'instructions' => 'The file wil be only attached to the emails and will <strong>not be visible</strong> in order details page.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_570678fc46aaf',
								'operator' => '==',
								'value' => 'yes',
							),
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'no' => 'No',
						'yes' => 'Yes',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_570678fc46bf0',
					'label' => 'Expires?',
					'name' => 'wcam_expiring_strategy',
					'type' => 'select',
					'instructions' => 'Valid only for attachments visible in order details page',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'none' => 'Never',
						'specific_date' => 'At specific date',
						'relative_date' => 'After the selected time amount after purchase',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_570678fc46d32',
					'label' => 'Specific date',
					'name' => 'wcam_specific_date',
					'type' => 'date_picker',
					'instructions' => 'format: d/m/Y, if an order is placed after the selected date, the attachments will be no longer visible',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_570678fc46bf0',
								'operator' => '==',
								'value' => 'specific_date',
							),
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'display_format' => 'd/m/Y',
					'return_format' => 'd/m/Y',
					'first_day' => 1,
				),
				array (
					'key' => 'field_570678fc46e74',
					'label' => 'Time amount',
					'name' => 'wcam_time_amount',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_570678fc46bf0',
								'operator' => '==',
								'value' => 'relative_date',
							),
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => 1,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => 1,
					'max' => '',
					'step' => 1,
				),
				array (
					'key' => 'field_570678fc46fb6',
					'label' => 'Time amount type',
					'name' => 'wcam_time_amount_type',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_570678fc46bf0',
								'operator' => '==',
								'value' => 'relative_date',
							),
							array (
								'field' => 'field_59354757b91bb',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'second' => 'Seconds',
						'minute' => 'Minutes',
						'hour' => 'Hours',
						'day' => 'Days',
						'month' => 'Month',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-bulk-products-attachments',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_56c346fd2146c',
	'title' => 'Attach Me! - Product attachments',
	'fields' => array (
		array (
			'key' => 'field_56c34a36f210f',
			'label' => 'Attachments',
			'name' => 'wcam_products_attachments',
			'type' => 'repeater',
			'instructions' => 'Choose file to attach to the product. Attachments can be downloadable/viewable in the Order details and/or Product pages. In case of visibility on order details page, every file can be visible for different order statuses.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => 'field_56c3484a619a7',
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Add new attachment',
			'sub_fields' => array (
				array (
					'key' => 'field_5707b063817af',
					'label' => 'Id',
					'name' => 'wcam_rule_id',
					'type' => 'unique_id',
					'instructions' => 'Unique id used to identy attachment. This value is autocatically assigned by the system after the first save.',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
				),
				array (
					'key' => 'field_56c3854db7630',
					'label' => 'Name',
					'name' => 'wcam_file_name',
					'type' => 'text',
					'instructions' => 'Type an name for the attachment. It wil be displayed in the front end next to the download/view button',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_56c3484a619a7',
					'label' => 'File',
					'name' => 'wcam_file',
					'type' => 'file',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_format' => 'array',
					'library' => 'all',
					'min_size' => '',
					'max_size' => '',
					'mime_types' => '',
				),
				array (
					'key' => 'field_56c387b5459c0',
					'label' => 'Description',
					'name' => 'wcam_file_description',
					'type' => 'textarea',
					'instructions' => 'Optionally you can type a short description',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'new_lines' => 'wpautop',
					'maxlength' => '',
					'placeholder' => '',
					'rows' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_570779e2821fc',
					'label' => 'Variants',
					'name' => 'wcam_variants',
					'type' => 'product_variant_selector',
					'instructions' => 'In case of variable product, you can enable attachments only for specific variants. Leave unselected to apply to all',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_value' => 'id',
					'field_type' => 'multi_select',
					'allowed_variants' => '',
				),
				array (
					'key' => 'field_5935460f3b8ff',
					'label' => 'Visibility',
					'name' => 'wcam_attachment_visibility',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'order_details_page' => 'Order details page',
						'product_page' => 'Product page',
						'all_pages' => 'Both',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_56c3474fefdac',
					'label' => 'Order status visibility',
					'name' => 'wcam_order_status_visibility',
					'type' => 'order_staus_selector',
					'instructions' => 'Select order status for which the attachment(s) will be visible.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'return_value' => 'name',
					'allowed_order_statuses' => '',
					'field_type' => 'checkbox',
				),
				array (
					'key' => 'field_56c34efb41566',
					'label' => 'Attach file to WooCoommerce outgoing email(s)?',
					'name' => 'wcam_attach_file_to_woocoommerce_status_email',
					'type' => 'select',
					'instructions' => 'The file is automatically attached to ANY outgoing WooCommerce email according to the current order status and the selected status on the previous <strong>Order status visibility</strong> section.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'yes' => 'Yes',
						'no' => 'No',
					),
					'default_value' => array (
						0 => 'no',
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_578638bda5277',
					'label' => 'File is attached to outgoing emails only?',
					'name' => 'wcam_file_is_only_attachend_to_the_email',
					'type' => 'select',
					'instructions' => 'The file wil be only attached to the emails and will <strong>not be visible</strong> in order details page.',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_56c34efb41566',
								'operator' => '==',
								'value' => 'yes',
							),
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'no' => 'No',
						'yes' => 'Yes',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_56dd4f74ad1f7',
					'label' => 'Expires?',
					'name' => 'wcam_expiring_strategy',
					'type' => 'select',
					'instructions' => 'Valid only for attachments visible in order details page',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'none' => 'Never',
						'specific_date' => 'At specific date',
						'relative_date' => 'After the selected time amount after purchase',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
				array (
					'key' => 'field_56dd4fdcad1f8',
					'label' => 'Specific date',
					'name' => 'wcam_specific_date',
					'type' => 'date_picker',
					'instructions' => 'format: d/m/Y, if an order is placed after the selected date, the attachments will be no longer visible',
					'required' => 1,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_56dd4f74ad1f7',
								'operator' => '==',
								'value' => 'specific_date',
							),
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'display_format' => 'd/m/Y',
					'return_format' => 'd/m/Y',
					'first_day' => 1,
				),
				array (
					'key' => 'field_56dd506a25a13',
					'label' => 'Time amount',
					'name' => 'wcam_time_amount',
					'type' => 'number',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_56dd4f74ad1f7',
								'operator' => '==',
								'value' => 'relative_date',
							),
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'default_value' => 1,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => 1,
					'max' => '',
					'step' => 1,
				),
				array (
					'key' => 'field_56dd50f225a14',
					'label' => 'Time amount type',
					'name' => 'wcam_time_amount_type',
					'type' => 'select',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array (
						array (
							array (
								'field' => 'field_56dd4f74ad1f7',
								'operator' => '==',
								'value' => 'relative_date',
							),
							array (
								'field' => 'field_5935460f3b8ff',
								'operator' => '!=',
								'value' => 'product_page',
							),
						),
					),
					'wrapper' => array (
						'width' => '50',
						'class' => '',
						'id' => '',
					),
					'choices' => array (
						'second' => 'Seconds',
						'minute' => 'Minutes',
						'hour' => 'Hours',
						'day' => 'Days',
						'month' => 'Month',
					),
					'default_value' => array (
					),
					'allow_null' => 0,
					'multiple' => 0,
					'ui' => 0,
					'ajax' => 0,
					'return_format' => 'value',
					'placeholder' => '',
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_56cb31b6c4996',
	'title' => 'Attach Me! – Email preset texts',
	'fields' => array (
		array (
			'key' => 'field_56cb338fab1b4',
			'label' => 'Body texts',
			'name' => 'wcam_body_texts',
			'type' => 'repeater',
			'instructions' => 'Here you can create preset text that can used for the "Embedding download/view links to WooCommerce emails" and "User notification email" features in the Order details page.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'row',
			'button_label' => 'Add new text',
			'collapsed' => 'field_56cb33e6ab1b5',
			'sub_fields' => array (
				array (
					'key' => 'field_56cb3e08a5c53',
					'label' => 'Unique field id',
					'name' => 'wcam_body_text_unique_field_id',
					'type' => 'unique_id',
					'instructions' => 'Ignore this field, is for internal use only',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
				),
				array (
					'key' => 'field_56cb33e6ab1b5',
					'label' => 'Title',
					'name' => 'wcam_body_text_title',
					'type' => 'text',
					'instructions' => 'Needed to identify the preset text',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => 150,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
				array (
					'key' => 'field_56cb340cab1b6',
					'label' => 'Text',
					'name' => 'wcam_body_text_message',
					'type' => 'wysiwyg',
					'instructions' => '<p>Here you can enter the text to be embeddend in the WooCommerce email. NOTE: if you use HTML code, rembember that be compatible with html emails standards.</p>
<p>&nbsp;</p>
<p>You can also use the special <b>[wcam_order_page_link]</b> and <b>[wcam_download_link id={id}]</b> shortcodes.</p>',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'tabs' => 'all',
					'toolbar' => 'basic',
					'media_upload' => 0,
					'default_value' => '',
					'delay' => 0,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-email-preset-texts',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;
?>