<?php
class WCAM_OrderDetailsAddon
{
	public function __construct()
	{
		add_action( 'woocommerce_order_details_after_order_table', array( &$this, 'front_end_order_page_addon' ), 99 ); //99: in this way the attachments are putted after the 'woocommerce_order_again_button' action
	}
	private function saveCustomerFeedbacks($order_id, $file_order_metadata)
	{
		global $wcam_order_model;
		/* echo "<pre>";
		var_dump($_POST);
		echo "<pre>";  */
		$email_string = "";
		
		$wcam_option_model = new WCAM_Option();
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$options = $wcam_option_model->get_option();
		$order = new WC_Order($order_id);
		$feedback_has_been_saved = array();
		
		$approve_not_yet_text = isset($options['approve_not_yet_text']) && isset($options['approve_not_yet_text'][$curr_lang]) ? $options['approve_not_yet_text'][$curr_lang] : __('Not yet', 'woocommerce-attach-me');
		$approve_yes_text = isset($options['approve_yes_text']) && isset($options['approve_yes_text'][$curr_lang]) ? $options['approve_yes_text'][$curr_lang] : __('Yes', 'woocommerce-attach-me');
		$approve_no_text = isset($options['approve_no_text']) && isset($options['approve_no_text'][$curr_lang]) ? $options['approve_no_text'][$curr_lang] : __('No', 'woocommerce-attach-me');
		$email_approve_box_title = isset($options['email_approve_box_title']) && isset($options['email_approve_box_title'][$curr_lang]) ? $options['email_approve_box_title'][$curr_lang] : __('Customer has approved?', 'woocommerce-attach-me');
		$email_feedback_box_title = isset($options['email_feedback_box_title']) && isset($options['email_feedback_box_title'][$curr_lang]) ? $options['email_feedback_box_title'][$curr_lang] : __('Customer feedback', 'woocommerce-attach-me');
		$email_no_customer_feedback_message = isset($options['email_no_customer_feedback_message']) && isset($options['email_no_customer_feedback_message'][$curr_lang]) ? $options['email_no_customer_feedback_message'][$curr_lang] : __('No customer feedback', 'woocommerce-attach-me');
		$status_to_assign_for_approval = isset($options['status_to_assign_for_approval']) ? $options['status_to_assign_for_approval'] : 'unchanged';
		$status_to_assign_for_not_approval = isset($options['status_to_assign_for_not_approval']) ? $options['status_to_assign_for_not_approval'] : 'unchanged';
		$status_to_assign_for_waiting_for_approval = isset($options['status_to_assign_for_waiting_for_approval']) ? $options['status_to_assign_for_waiting_for_approval'] : 'unchanged';
		
		foreach($_POST['wcam_attachment_id'] as $key => $file_id)
		{
			if(isset($_POST['wcam_customer_approval']) && isset($_POST['wcam_customer_approval'][$file_id]))
			{
				$feedback_has_been_saved[$file_id] = true;
				$file_order_metadata[$file_id]['customer-has-approved'] = $_POST['wcam_customer_approval'][$file_id];
			}
			if(isset($_POST['wcam_customer_feedback']) &&  isset($_POST['wcam_customer_feedback'][$file_id]))
			{
				$feedback_has_been_saved[$file_id] = true;
				$file_order_metadata[$file_id]['customer-feedback'] = stripslashes($_POST['wcam_customer_feedback'][$file_id]);
			}
			
			//Email
			if($file_order_metadata[$file_id]['customer-admin-notification'] == 'yes')
			{
				$email_string .= '<h3>'.$file_order_metadata[$file_id]['title'].'</h3>';
				$approve_text;
				if(isset($file_order_metadata[$file_id]['customer-has-approved']))
				{
					switch($file_order_metadata[$file_id]['customer-has-approved'])
					{
						case "none": $approve_text = $approve_not_yet_text; break;
						case "yes": $approve_text = $approve_yes_text;break;
						case "no": $approve_text = $approve_no_text;break;
					}
					$email_string .=  '<strong>'.$email_approve_box_title.'</strong><br/>';
					$email_string .=  $approve_text.'<br/>';
				}
				//Feedback
				if(isset($file_order_metadata[$file_id]['customer-feedback']) && !empty($file_order_metadata[$file_id]['customer-feedback']))
				{
					$email_string .=  '<strong>'.$email_feedback_box_title.'</strong><br/>';
					$email_string .=  $file_order_metadata[$file_id]['customer-feedback'].'<br/>';
				}
				else
					$email_string .=  '<strong>'.$email_no_customer_feedback_message.'</strong><br/>';
			}
			
			//Status change
			if(isset($file_order_metadata[$file_id]['customer-has-to-be-approved']) && $file_order_metadata[$file_id]['customer-has-to-be-approved'] == 'yes' && isset($_POST['wcam_customer_approval'][$file_id]))
			 {
				 
				 switch($_POST['wcam_customer_approval'][$file_id])
				 {
					 case 'yes': if($status_to_assign_for_approval != 'unchanged') $order->update_status($status_to_assign_for_approval);
					 break;
					 case 'no': if($status_to_assign_for_not_approval != 'unchanged') $order->update_status($status_to_assign_for_not_approval);
					 break;
					 case 'none': if($status_to_assign_for_waiting_for_approval != 'unchanged') $order->update_status($status_to_assign_for_waiting_for_approval);
					 break;
				 }
			 }
		}
		
		if($email_string != "")
		{
			$email_sender = new WCAM_Email();
			$email_sender->trigger(get_option('admin_email'), __('New Approval(s)/Feedback(s) for order: ', 'woocommerce-attach-me').$order_id, $email_string );
		}
		
		
		//update_post_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		$wcam_order_model->update_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		return array('order_meta'=> $file_order_metadata, 'feedback_has_been_saved' => $feedback_has_been_saved);
	}
	public function front_end_order_page_addon( $order )
	{
		
		global $wcam_product_model,  $wcam_order_model;
		$default_width = get_option('thumbnail' . '_size_w' );
		$default_height = get_option('thumbnail' . '_size_h' );
		$feedback_has_been_saved = array();
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$order_id = WCAM_Order::get_id($order) ;	
		//$file_order_metadata = get_post_meta($order_id, '_wcam_attachments_meta'); //can be removed and used the WCAM_Option
		//$file_order_metadata = !$file_order_metadata ? array():$file_order_metadata[0];
		$file_order_metadata = $wcam_order_model->get_attachments_metadata($order_id, array());
		$options = new WCAM_Option();
		$all_options = $options->get_option();
		$preview_image = $options->get_option('dispaly_image_preview', 'no');
		$use_lightbox_preview = $options->get_option('use_lightbox_preview', false);
		$select_box_style = $options->get_option('approval_box_style', 'select');
		$preview_image_height = $options->get_option('thumb_height', $default_height);
		$preview_image_width = $options->get_option('thumb_width', $default_width);	
		$no_attachments_message = isset($all_options['no_attachments_message']) && isset($all_options['no_attachments_message'][$curr_lang]) ? $all_options['no_attachments_message'][$curr_lang] : "";
		$time_format= $options->get_option('exiring_date_time_format', 'd/m/Y');
		$link_open_method =  (bool)$options->get_option('open_link_in_same_window', false) ? '_self' : '_blank';
		$exists_at_least_one_attachment_to_show = 0;
		
		if(isset($_POST) && isset($_POST['wcam_save_customer_feedbacks']))
		{
			$save_data_result = $this->saveCustomerFeedbacks($order_id, $file_order_metadata );
			$file_order_metadata = $save_data_result['order_meta'];
			$feedback_has_been_saved = $save_data_result['feedback_has_been_saved'];
		}
		
		if($preview_image && $use_lightbox_preview)
		{
			wp_enqueue_script( 'wcam-lightbox', wcam_PLUGIN_PATH . '/js/lightbox.js', array( 'jquery' ));
			wp_enqueue_style( 'wcam-lightbox', wcam_PLUGIN_PATH. '/css/lightbox.css' );
		}
		wp_enqueue_style( 'css-wcam_frontend', wcam_PLUGIN_PATH . '/css/wcam-frontend.css' );
		wp_register_script('wcam-frontend-order-details',  wcam_PLUGIN_PATH . '/js/wcam-frontend-order-details.js', array( 'jquery' ));
		$js_params = array(
				'wc_ver' => WC_VERSION,
				);
		wp_localize_script( 'wcam-frontend-order-details', 'wcam', $js_params );
		wp_enqueue_script( 'wcam-frontend-order-details' );
		if(!empty($file_order_metadata))
		{
			//Texts and Miltilanguages
			$title = isset($all_options['title_box']) && isset($all_options['title_box'][$curr_lang]) ? $all_options['title_box'][$curr_lang] : __('Attachments', 'woocommerce-attach-me');			
			$download_view_button_text = isset($all_options['download_view_button_text']) && isset($all_options['download_view_button_text'][$curr_lang]) ? $all_options['download_view_button_text'][$curr_lang] : __('Download / View', 'woocommerce-attach-me');
			$approve_box_title = isset($all_options['approve_box_title']) && isset($all_options['approve_box_title'][$curr_lang]) ? $all_options['approve_box_title'][$curr_lang] : __('Do you approve?', 'woocommerce-attach-me');
			$approve_already_sent_message = isset($all_options['approve_already_sent_message']) && isset($all_options['approve_already_sent_message'][$curr_lang]) ? $all_options['approve_already_sent_message'][$curr_lang] : __('Approvation already sent. Selected value was:', 'woocommerce-attach-me');
			$approve_not_yet_text = isset($all_options['approve_not_yet_text']) && isset($all_options['approve_not_yet_text'][$curr_lang]) ? $all_options['approve_not_yet_text'][$curr_lang] : __('Not yet', 'woocommerce-attach-me');
			$approve_yes_text = isset($all_options['approve_yes_text']) && isset($all_options['approve_yes_text'][$curr_lang]) ? $all_options['approve_yes_text'][$curr_lang] : __('Yes', 'woocommerce-attach-me');
			$approve_no_text = isset($all_options['approve_no_text']) && isset($all_options['approve_no_text'][$curr_lang]) ? $all_options['approve_no_text'][$curr_lang] : __('No', 'woocommerce-attach-me');
			$feedback_box_title = isset($all_options['feedback_box_title']) && isset($all_options['feedback_box_title'][$curr_lang]) ? $all_options['feedback_box_title'][$curr_lang] : __('Please enter a feedback', 'woocommerce-attach-me');
			$copy_to_clipboard_button_text = isset($all_options['copy_to_clipboard_button_text']) && isset($all_options['copy_to_clipboard_button_text'][$curr_lang]) ? $all_options['copy_to_clipboard_button_text'][$curr_lang] : __('Copy file URL to clipboard', 'woocommerce-attach-me');
			$exists_at_least_one_attachment_to_show = $wcam_order_model->number_of_visible_attachments($order);
			
			if(file_exists ( get_template_directory()."/wcam/view_order_template.php" ))
				include get_template_directory()."/wcam/view_order_template.php";
			else
				include WCAM_PLUGIN_ABS_PATH.'/templates/view_order_template.php';
		}
		
		//Texts and Miltilanguages
		$product_attachments_box_title = isset($all_options['product_attachments_box']) && isset($all_options['product_attachments_box'][$curr_lang]) ? $all_options['product_attachments_box'][$curr_lang] : __('Downloads', 'woocommerce-attach-me');
		$products_data = $wcam_product_model->get_attachments_downloadable_in_order_details_page($order);
		if(file_exists ( get_template_directory()."/wcam/view_order_template_products_attachments.php" ))
			include get_template_directory()."/wcam/view_order_template_products_attachments.php";
		else
			include WCAM_PLUGIN_ABS_PATH.'/templates/view_order_template_products_attachments.php';
		
		//No attachments ($products_data is in view_order_template_products_attachments template)
		if((!isset($file_order_metadata) || empty($file_order_metadata) || $exists_at_least_one_attachment_to_show == 0 ) && empty($products_data) && $no_attachments_message!="")
				echo "<h4 id='wcam-no-attachments-message'>".$no_attachments_message."</h4>";
					
	}
}
?>