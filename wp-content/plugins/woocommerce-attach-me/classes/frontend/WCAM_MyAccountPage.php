<?php 
class WCAM_MyAccountPage
{
	var $order_to_num_of_attachments = array();
	public function __construct()
	{
			try{
			$wc_version = wcam_get_woo_version_number();
		}catch(Exception $e){}
		
		if(!isset($wc_version) || version_compare($wc_version , 2.6, '<') )
			add_action( 'woocommerce_after_my_account', array( &$this, 'add_order_table_extra_buttons'));
		if(isset($wc_version) && version_compare($wc_version , 2.6, '>=') )
			add_action( 'woocommerce_account_content', array( &$this,'add_order_table_extra_buttons'),99 );
		
		//add_action( 'woocommerce_my_account_my_orders_column_order-actions', array( &$this,'get_order_attachments_info'), 10, 1); //NO: overrides the "actions" column rendering
		add_filter( 'woocommerce_my_account_my_orders_actions', array( &$this,'get_order_attachments_info'), 10, 2);
	}
	public function get_order_attachments_info( $actions, $order)
	{
		global  $wcam_order_model, $wcam_product_model;
		
		$num_of_visible_order_attachments = $wcam_order_model->number_of_visible_attachments($order);
		$num_or_product_attachmentes = count($wcam_product_model->get_attachments_downloadable_in_order_details_page($order));
		$this->order_to_num_of_attachments[WCAM_Order::get_id($order) ] = array('order_attachmentes' => $num_of_visible_order_attachments, 'product_attachmentes' => $num_or_product_attachmentes);
		
		return $actions;
	}
	public function add_order_table_extra_buttons()
	{
		global $wp, $wcam_order_model;
		$can_render = false;
		if ( did_action( 'woocommerce_account_content' ) ) 
		{
				foreach ( $wp->query_vars as $key => $value ) 
				{
					if($key == 'orders')
						$can_render = true;
				}
		}
		else
			$can_render = true;
		
		/* $attachments =  $wcam_order_model->get_attachments_metadata($order_id, array());
		$can_render = empty($attachments) ? false : $can_render; */
		
		if(!$can_render)
			return false;
		
		if(!get_current_user_id()) //???
			return;
		
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$options = new WCAM_Option();
		$all_options = $options->get_option();
		$download_view_button_text = isset($all_options['view_attachments_button']) && isset($all_options['view_attachments_button'][$curr_lang]) ? $all_options['view_attachments_button'][$curr_lang] : __('View Attachments', 'woocommerce-attach-me');
		wp_enqueue_style('wcam-my-account', wcam_PLUGIN_PATH.'/css/wcam-frontend-my-account.css');
		wp_register_script('wcam-my-account-orders-table', wcam_PLUGIN_PATH.'/js/wcam-frontend-my-account-orders-table.js', array('jquery'));
		//include wcam_PLUGIN_ABS_PATH.'/templates/my_account_orders_table.php';
		
		$translation_array = array(
			'view_attachments' => $download_view_button_text,
			'view_order_url' => wc_get_endpoint_url( 'view-order', "wcam_order_id_place_holder", wc_get_page_permalink( 'myaccount' ) ),
			'order_to_num_of_attachments' => $this->order_to_num_of_attachments,
			'wc_ver' => WC_VERSION
		);
		wp_localize_script( 'wcam-my-account-orders-table', 'wcam', $translation_array );
		wp_enqueue_script( 'wcam-my-account-orders-table' );
	}
}
?>