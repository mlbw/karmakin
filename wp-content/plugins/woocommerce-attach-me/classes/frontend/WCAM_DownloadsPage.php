<?php 
class WCAM_DownloadsPage
{
	var $options;
	public function __construct()
	{
		//Add attachments to order downloads 
		add_filter('woocommerce_customer_get_downloadable_products', array(&$this, 'add_attachments_to_downloadable_products'));
		
		//Additional columns managment
		add_filter('woocommerce_account_downloads_columns', array(&$this, 'add_extra_colum'));
		add_action('woocommerce_account_downloads_column_download-file-wcam-order-id', array(&$this, 'add_order_id_column_content'));
		add_action('woocommerce_account_downloads_column_download-has-to-be-approved', array(&$this, 'add_approval_column_content'));
		add_filter('woocommerce_account_download_actions', array(&$this, 'remove_download_button_for_expired'), 99 ,2);
	}
	public function add_extra_colum($columns)
	{
		$options = new WCAM_Option();
		$this->options = $options->get_option();
		if(!isset($this->options['show_attachments_in_downloads_page']))
			return $columns;
		
		$new_columns = array();
		foreach($columns as $key => $content)
		{
			$new_columns[$key] = $content;
			/* if($key == 'download-file')
				$new_columns['download-file-wcam-order-id'] = __( 'Order id', 'woocommerce-attach-me' ); */
			if($key == 'download-expires')
				$new_columns['download-has-to-be-approved'] = __( 'Approval required', 'woocommerce-attach-me' );
		}
		return $new_columns;
	}
	public function add_order_id_column_content($download)
	{
		$order_page_url = '<a href="'.add_query_arg('view-order', $download['order_id'], get_permalink( get_option( 'woocommerce_myaccount_page_id' ) )  ).'" target="_blank" >#'.$download['order_id'].'</a>';
		echo $order_page_url;
	}
	public function add_approval_column_content($download)
	{
		$order_page_url = '<a href="'.add_query_arg('view-order', $download['order_id'], get_permalink( get_option( 'woocommerce_myaccount_page_id' ) )  ).'" target="_blank">'.__('Yes', 'woocommerce-attach-me').'</a>';
		
		if(isset($download["customer-has-to-be-approved"]) && $download["customer-has-to-be-approved"])
			echo $order_page_url;
		else
			_e( 'No', 'woocommerce-attach-me' );
		
	}
	public function remove_download_button_for_expired($actions, $download)
	{
		foreach ( $actions as $key => $action ) 
			if($action['url'] != "#wcam_expired")
				echo '<a href="' . esc_url( $action['url'] ) . '" class="button woocommerce-Button ' . sanitize_html_class( $key ) . '">' . esc_html( $action['name'] ) . '</a>';
			else
				_e('Expired','woocommerce-attach-me'); 				
	}
	public function add_attachments_to_downloadable_products($downloads)
	{
		if(!isset($this->options))
		{	
			$options = new WCAM_Option();
			$this->options = $options->get_option();
		}
		if(!isset($this->options['show_attachments_in_downloads_page']))
			return $downloads;
		
		global $wcam_order_model, $wcam_product_model ;
		$user_id = get_current_user_id();
		$user_orders = $wcam_order_model->get_user_orders($user_id);
		
		if($user_orders)
			foreach((array)$user_orders as $user_order)
			{
				$order = new WC_Order($user_order->ID);
				$product_attachments = $wcam_product_model->get_attachments_downloadable_in_order_details_page($order);
				$order_attachments = $wcam_order_model->get_attachments_metadata(WCAM_Order::get_id($order) , array());
				/* wcam_var_dump($user_order->ID);
				wcam_var_dump($order_attachments);
				wcam_var_dump($product_attachments);
				wcam_var_dump("*********************"); */
				
				if($product_attachments && is_array($product_attachments) && !empty($product_attachments))
				{
					$product_attachments = array_reverse($product_attachments);
					foreach($product_attachments as $product_name => $attached_files)
					{
						foreach($attached_files as $product_attachment)
						{
							if(isset($downloads[$product_attachment["id"]]))
								continue;
							
							$attachment = array();
							$attachment['download_url'] = $product_attachment['has_expired'] ? "#wcam_expired" : $product_attachment["file_url"];
							$attachment['download_id'] = $product_attachment["id"];
							$attachment['download_name'] = $product_attachment["file_name"];
							$attachment['name'] = $product_attachment["file_name"];
							$attachment['product_name'] = $product_name;
							$attachment['order_id'] = WCAM_Order::get_id($order);
							$attachment['product_id'] = "";
							$attachment['downloads_remaining'] = "";
							$attachment['order_key'] = "";
							//$attachment['access_expires'] = null;
							$attachment['file'] = array();
							$attachment['file']['name'] = $product_attachment["file_name"];
							$attachment['access_expires'] = wcam_format_dateime_obj_according_wordpress($product_attachment['expiring_date']);
							//$attachment['download_name'] = $product_attachment['has_expired'] ? $attachment['download_name']. __(' - Expired','woocommerce-attach-me') : $attachment['download_name'];
							/* wcam_var_dump($product_attachment);
							wcam_var_dump($attachment); */
							$downloads[$product_attachment["id"]] = $attachment; //id to avoid to attach same file associated to different products
							}
					}
				}
				
				if($order_attachments && is_array($order_attachments) && !empty($order_attachments))
				{
					$order_attachments = array_reverse($order_attachments);
					foreach($order_attachments as $order_attachment)
					{
						//ToDo: is_attachment_expired()
						/* if($wcam_order_model->is_attachment_expired($order_attachment, $user_order->post_date))
							continue; */
						$is_expired = $wcam_order_model->is_attachment_expired($order_attachment, $user_order->post_date);
						$attachment = array();
						$attachment['download_url'] =  $is_expired ? "#wcam_expired" : $order_attachment["url"];
						$attachment['customer-has-to-be-approved'] = $order_attachment["customer-has-to-be-approved"] == 'yes' ? true : false;
						$attachment['download_id'] = WCAM_Order::get_id($order)."-".$order_attachment["id"];
						$attachment['download_name'] = $order_attachment["title"];
						$attachment['name'] = $order_attachment["title"];
						$attachment['product_name'] = sprintf(__( 'Order #%s', 'woocommerce-attach-me' ),$user_order->ID);//$order_attachment["title"];
						$attachment['order_id'] = WCAM_Order::get_id($order);
						$attachment['product_id'] = "";
						$attachment['downloads_remaining'] = "";
						$attachment['order_key'] = "";
						$attachment['access_expires'] = null;
						$attachment['file'] = array();
						$attachment['file']['name'] = $order_attachment["title"];
						$attachment['access_expires'] = $wcam_order_model->get_attachment_expiration_date(WCAM_Order::get_id($order), $order_attachment);
						//wcam_var_dump($downloads);
						$downloads[] = $attachment; //id to avoid to attach same file associated to different products
					}
				}
				
			}
		
		
		return $downloads;
	}
}
?>