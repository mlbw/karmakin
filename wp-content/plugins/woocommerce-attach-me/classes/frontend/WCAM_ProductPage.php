<?php 
class WCAM_ProductPage
{
	var $products_data;
	public function __construct()
	{
		add_filter( 'woocommerce_product_tabs', array(&$this, 'add_attachments_tab') );
	}
	function add_attachments_tab( $tabs ) 
	{
	
		global $wcam_product_model, $product;
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$options = new WCAM_Option();
		$all_options = $options->get_option();
		$this->products_data = $wcam_product_model->get_attachments_downloadable_in_product_page($product);
		$title = isset($all_options['product_tab_title']) && isset($all_options['product_tab_title'][$curr_lang]) ? $all_options['product_tab_title'][$curr_lang] : __('Attachments', 'woocommerce-attach-me');		
		
		if(!empty($this->products_data))
			$tabs['test_tab'] = array(
				'title' 	=> $title,
				'priority' 	=> 50,
				'callback' 	=> array(&$this, 'attachments_tab_content')
			);

	return $tabs;

	}
	function attachments_tab_content() 
	{
		global $wcam_product_model, $product;
		
		$options = new WCAM_Option();
		
		$preview_image = $options->get_option('dispaly_image_preview', 'no');
		$use_lightbox_preview = $options->get_option('use_lightbox_preview', false);
		$link_open_method =  (bool)$options->get_option('open_link_in_same_window', false) ? '_self' : '_blank';
		
		if($preview_image && $use_lightbox_preview)
		{
			wp_enqueue_script( 'wcam-lightbox', wcam_PLUGIN_PATH . '/js/lightbox.js', array( 'jquery' ));
			wp_enqueue_style( 'wcam-lightbox', wcam_PLUGIN_PATH. '/css/lightbox.css' );
		}
		wp_enqueue_style( 'css-wcam_frontend', wcam_PLUGIN_PATH . '/css/wcam-frontend-product-page.css' );
		
		/* echo '<h2>New Product Tab</h2>';
		echo '<p>Here\'s your new product tab.</p>'; */
	
		//$products_data = $wcam_product_model->get_attachments_downloadable_in_product_page($product);
		if(!isset($this->products_data))
			return;
		$products_data = $this->products_data;
		if(file_exists ( get_template_directory()."/wcam/product_page_template_products_attachments.php" ))
			include get_template_directory()."/wcam/product_page_template_products_attachments.php";
		else
			include WCAM_PLUGIN_ABS_PATH.'/templates/product_page_template_products_attachments.php';
		
	}
}
?>