<?php 
class WCAM_Notifier
{
	var $current_notification_name;
	var $hooks;
	var $attachments_to_email_already_performed = false;
	public function __construct()
	{
		//woocommerce_order_status_completed_notification
		/* $this->hooks = array();
		$statuses = $this->get_order_statuses();
		//$this->var_debug($statuses);
		foreach($statuses['statuses'] as $status)
		{
			$action = 'woocommerce_order_status_'.$status['status'].'_notification'; //'woocommerce_'.$status['status'].'_order_status'; 
			add_action( $action,  array( $this, 'set_current_notification_name' ) );
			$this->hooks[] = array('hook' => $action, 'id' => $status['id']);
		} */
		add_action( 'woocommerce_email_before_order_table', array( &$this, 'add_wcam_data' ), 10,4 );
		add_filter( 'woocommerce_email_attachments', array( &$this, 'add_wcam_attachment' ), 10, 3);
	}
	private function get_order_options($order)
	{
		//embed-links-to-complete-mail-text
		//embed-links-to-complete-mail
		$wcam_options = get_post_meta( WCAM_Order::get_id($order), '_wcam_options');
		$wcam_options = isset($wcam_options) && isset($wcam_options[0]) ? $wcam_options[0]:null;
		return $wcam_options;
	}
	/* private function get_order_attachments_metadata($order)
	{
		$wcam_meta_data = get_post_meta($order->id, '_wcam_attachments_meta');
		$wcam_meta_data = isset($wcam_meta_data) && isset($wcam_meta_data[0]) ? $wcam_meta_data[0]:null;
		return $wcam_meta_data;
	} */
	private function get_order_products_attachments_metadata($order)
	{
		global $wcam_product_model;
		return $wcam_product_model->get_attachments_per_order_id(WCAM_Order::get_id($order));
	}
	public function get_order_statuses()
	{
		
		$result = array();
		if(function_exists( 'wc_get_order_statuses' ))
		{
			
			$result['version'] = 2.2;
			//[slug] => name
			$result['statuses'] = array();
			$temp  = wc_get_order_statuses();
			foreach($temp as $slug => $title)
			{
					//array_push($result['statuses'], $slug);
					$result['statuses'][] = array('status' => 'wc-' === substr( $slug, 0, 3 ) ? substr( $slug, 3 ) : $slug, 'id' =>  $slug);
			}
		}
		else
		{
			$args = array(
				'hide_empty'   => false, 
				'fields'            => 'id=>slug', 
			);
			$result['version'] = 2.1;
			$result['statuses'] = array();
			$temp = get_terms('shop_order_status', $args);
			foreach($temp as $id => $slug)
			{
					$result['statuses'][] = array('status' => $slug, 'id' =>  $id);
			}
		}
		return $result;
	}
	public function get_order_statuses_id_to_name()
	{
		$result = array();
		if(function_exists( 'wc_get_order_statuses' ))
		{
			
			$result['version'] = 2.2;
			//[slug] => name
			$result['statuses'] = wc_get_order_statuses();
		}
		else
		{
			$args = array(
				'hide_empty'   => false, 
				'fields'            => 'id=>name', 
			);
			$result['version'] = 2.1;
			//[id] => name
			$result['statuses'] =  get_terms('shop_order_status', $args);
		}
		return $result;
	}
	public function set_current_notification_name($order_id)
	{
		foreach ( $this->hooks as $hooks ) 
		{
            if( did_action( $hooks['hook'] ) > 0 ) 
			{
 
              $this->current_notification_name = $hooks['id'] ;
            } 
			
        } 
	}
	static function send_user_email($meta_info, $file_order_metadata, $order_id)
	{
		global $wcam_order_model;
		if(isset($meta_info['wcam-notification-message']) && isset($meta_info['wcam-notification-subject']))
			{
				$item_to_attach = isset($meta_info['wcam-attachment-ids']) ? explode(",",$meta_info['wcam-attachment-ids']):null;
				$email_sender = new WCAM_Email();
				$order = new WC_Order( $order_id );
				$general_options_model = new WCAM_Option();
				/* $user_id = $order->user_id;
				$user = get_userdata( $user_id ); */
				$user_email = $order->get_billing_email();
				//$message = $meta_info['wcam-notification-message'];
				$message = /* !isset($meta_info['wcam-notification-preset-email-text-id']) || $meta_info['wcam-notification-preset-email-text-id'] == 'no' || $meta_info['wcam-notification-preset-email-text-id'] =='input_text' ? */ $meta_info['wcam-notification-message'] /* : $general_options_model->get_emails_preset_text_by_id($meta_info['wcam-notification-preset-email-text-id'] ) */;
				$attachments = array();
				
				//Download links 
				foreach($file_order_metadata as $id => $meta)
				{
					$id = (int)$id;
					$replaced_shortcode = /* $wcam_order_model->is_attachment_expired($meta, $order->order_date) ? "" : */ '[wcam_download_link attachment_id="'.$id.'" order_id="'.WCAM_Order::get_id($order).'"]';
					$message = str_replace('[wcam_download_link id=\"'.$id.'\"]', $replaced_shortcode, htmlspecialchars_decode($message));
					//$message = str_replace('[wcam_download_link id=\"'.$id.'\"]', '[wcam_download_link attachment_id="'.$id.'" order_id="'.$order->id.'"]',htmlspecialchars_decode($message));
				}
				
				//order link embed
				$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
				//$message = str_replace('[wcam_order_page_link]', '<a href="'.get_permalink( $myaccount_page_id ).'&view-order='.$order_id .'">'.__('Order link', 'woocommerce-attach-me').'</a>', htmlspecialchars_decode($message));
				$message = str_replace('[wcam_order_page_link]', '[wcam_order_page_link page_id="'.$myaccount_page_id.'" order_id="'.WCAM_Order::get_id($order).'"]', htmlspecialchars_decode($message));
				
				//attachment
				if(isset($item_to_attach))
					foreach($item_to_attach as $id)
					{
						if($wcam_order_model->is_attachment_expired($file_order_metadata[$id], WCAM_Order::get_date_created($order)))
							continue;
						
						if(isset($file_order_metadata[$id]['absolute_path']) && file_exists($file_order_metadata[$id]['absolute_path'])) //uploaded file
							array_push($attachments, $file_order_metadata[$id]['absolute_path']); 
						else if(isset($file_order_metadata[$id]['media_from_gallery']) && $file_order_metadata[$id]['media_from_gallery'] == 'yes' && isset($file_order_metadata[$id]['media_id']))
						{
							array_push($attachments, get_attached_file( $file_order_metadata[$id]['media_id'] ));
						}
						
					}
				$email_sender->trigger($user_email/* $user->user_email */,$meta_info['wcam-notification-subject'],  do_shortcode($message), $attachments);
				$results = __('Message has been sent successfully', 'woocommerce-attach-me');
			}
			else 
				$errors = __('Subject and message cannot be empty.', 'woocommerce-attach-me');
			
		return isset($results) ? $results : $errors;
	}
	public function add_wcam_data($order, $sent_to_admin = false, $plain_text = false, $email = null )
	{
		if(!isset($order))
			return;
		
		global $wcam_order_model, $wcam_shortcode_model;
		$general_options_model = new WCAM_Option();
		$wcam_options = $this->get_order_options($order);
		$wcam_meta_data = $wcam_order_model->get_attachments_metadata(WCAM_Order::get_id($order));
		$current_status = "wc-".$order->get_status( );
		$order_id = WCAM_Order::get_id($order);
		
		if(isset($wcam_options) && $wcam_options['embed-links-to-complete-mail'] == "yes" && 
		   !isset($wcam_options['embed-links-hide-by-status']) || empty($wcam_options['embed-links-hide-by-status']) || !in_array($current_status, $wcam_options['embed-links-hide-by-status']))
		{
			$message = /* !isset($wcam_options['embed-links-preset-email-text-id']) || $wcam_options['embed-links-preset-email-text-id'] == 'no' || $wcam_options['embed-links-preset-email-text-id'] =='input_text' ? */ $wcam_options['embed-links-to-complete-mail-text'] /* : $general_options_model->get_emails_preset_text_by_id($wcam_options['embed-links-preset-email-text-id'] ) */;
			//Download links 
			if(isset($wcam_meta_data))
				foreach($wcam_meta_data as $id => $meta)
				{
					$id = (int)$id;
					$replaced_shortcode = /* $wcam_order_model->is_attachment_expired($meta, $order->order_date) ? "" : */ '[wcam_download_link attachment_id="'.$id.'" order_id="'.WCAM_Order::get_id($order).'"]';
					$message = str_replace('[wcam_download_link id="'.$id.'"]', $replaced_shortcode, htmlspecialchars_decode($message));
				}
				
			//order link embed
			$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
			$message = str_replace('[wcam_order_page_link]', '[wcam_order_page_link page_id="'.$myaccount_page_id.'" order_id="'.WCAM_Order::get_id($order).'"]', htmlspecialchars_decode($message));
				
			//echo "<br/>";
			echo do_shortcode($message);
			//echo "<br/>";
		}
	}
	public function add_wcam_attachment( $attachments , $status, $order ) 
	{
		global $wcam_order_model;
		if($this->attachments_to_email_already_performed)
			return $attachments;
		if(get_class($order) != 'WC_Order')
			return;
		/* 
			new_order
			customer_on_hold_order
			customer_processing_order
			customer_completed_order
			customer_invoice
			customer_note
			low_stock
			no_stock
			backorder
			customer_new_account
			customer_invoice_paid
			*/
		$allowed_statuses = array(  /* 'new_order', */ 'customer_invoice', 'customer_processing_order',  'customer_completed_order' );
		
		
		if( isset( $status ) && in_array ( $status, $allowed_statuses ) ) 
		{
			$wcam_meta_data = $wcam_order_model->get_attachments_metadata(WCAM_Order::get_id($order));
			//attachment
			if(isset($wcam_meta_data))
				foreach($wcam_meta_data as $id => $meta)
				{
					if( ( $status == 'customer_processing_order' && isset($meta['attach-file-to-processing-order-email']) && $meta['attach-file-to-processing-order-email'] == "yes") ||
					   ($status == 'customer_completed_order' && isset($meta['attach-file-to-complete-order-email']) && $meta['attach-file-to-complete-order-email'] == "yes") ||
					   ($status == 'customer_invoice' && isset($meta['attach-file-to-customer-invoice-email']) && $meta['attach-file-to-customer-invoice-email'] == "yes"))
					{
						
						if($wcam_order_model->is_attachment_expired($meta, WCAM_Order::get_date_created($order)))
							continue;
							
						if(isset($meta['absolute_path']) && file_exists($meta['absolute_path'])) //uploaded file
							array_push($attachments, $meta['absolute_path']); 
						else if(isset($meta['media_from_gallery']) && $meta['media_from_gallery'] == 'yes' && isset($meta['media_id']))
						{
								array_push($attachments, get_attached_file( $meta['media_id'] ));
						}
					}
					
				}	
		}
		
		//Automatic products attachments
		$all_attachments = $this->get_order_products_attachments_metadata($order);
		
		if(!empty($all_attachments ) && (!isset( $status ) || $status != 'new_order' ))
		{
			foreach($all_attachments as $product_attachments)
			{
				foreach($product_attachments as $product_attachment)
					 if(!$product_attachment['has_expired'] && $product_attachment['attachment_visibility'] != 'product_page')
						$attachments[$product_attachment['id']] = $product_attachment['file_path'];
			}
		}
		
		/* wcam_var_dump($order->get_items());
		wp_die(); */
		//Automatic attachment
		$option = new WCAM_Option();
		$automatic_attachments = $option->get_automatic_attacments_options();
		if(!empty($automatic_attachments))
			foreach($automatic_attachments["automatic_email_attachments"] as $automatic_attachment)
			{
				if( isset( $status ) && in_array ( $status, $automatic_attachment['email_type'] ) ) 
				{
					array_push($attachments, get_attached_file( $automatic_attachment['selected_file'] ) );
				}
			}
		$this->attachments_to_email_already_performed = true;
		return $attachments;
	}
	private function var_debug($var)
	{
		echo "<pre>";
		var_dump($var);
		echo "</pre>";
	}
}
?>