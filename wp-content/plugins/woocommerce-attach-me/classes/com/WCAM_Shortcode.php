<?php 
class WCAM_Shortcode
{
	public function __construct()
	{
		add_shortcode( 'wcam_last_order_attachments', array(&$this, 'last_order_attachments' ));
		add_shortcode( 'wcam_download_link', array(&$this, 'download_link' ));
		add_shortcode( 'wcam_order_page_link', array(&$this, 'order_page_link' ));
	}
	
	public function last_order_attachments($atts)
	{
		global $wcam_woocommerce_order_details_addon, $wcam_customer_model;
		
		$last_order_id = $wcam_customer_model->get_current_customer_last_order_id();
		ob_start();
		if($last_order_id > 0)
			$wcam_woocommerce_order_details_addon->front_end_order_page_addon(new WC_Order($last_order_id));
		else
			_e('You have no orders or you are not logged','woocommerce-files-upload');
		return ob_get_clean();
	}
	public function download_link($atts, $content = null)
	{
		/*  $a = shortcode_atts( array(
			'id' => get_current_user_id(),
			), $atts ); */
		if(!isset($atts['attachment_id']) || !isset($atts['order_id']))
			return "";
		global $wcam_order_model;
		
		$wcam_meta_data = $wcam_order_model->get_attachments_metadata($atts['order_id']);
		$order = new WC_Order($atts['order_id']);
		ob_start();
		if(isset($wcam_meta_data))
				foreach($wcam_meta_data as $id => $meta)
				{
					if($id == $atts['attachment_id'] && !$wcam_order_model->is_attachment_expired($meta, WCAM_Order::get_date_created($order)))
					{
						if(!isset($content) || empty($content))
							echo '<a href="'.$meta['url'].'">'.__('download', 'woocommerce-attach-me').'</a>';
						else
							echo '<a href="'.$meta['url'].'">'.$content.'</a>';
					}
				}
		return ob_get_clean();
		
	}
	public function order_page_link($atts, $content = null)
	{
		if(!isset($atts['page_id']) || !isset($atts['order_id']))
			return "";
		//global $wcam_order_model;
		//$wcam_meta_data = $wcam_order_model->get_attachments_metadata($atts['order_id']);
		
		ob_start();
		/* if(isset($wcam_meta_data))
				foreach($wcam_meta_data as $id => $meta) */
				{
					if(!isset($content) || empty($content))
						//echo '<a href="'.get_permalink( $atts['page_id'] ).'&view-order='.$atts['order_id'].'">'.__('Order link', 'woocommerce-attach-me').'</a>';
						echo '<a href="'.add_query_arg('view-order', $atts['order_id'], get_permalink( $atts['page_id'] )  ).'">'.__('Order link', 'woocommerce-attach-me').'</a>';
					else
						//echo '<a href="'.get_permalink( $atts['page_id'] ).'&view-order='.$atts['order_id'].'">'.$content.'</a>';
						echo '<a href="'.add_query_arg('view-order', $atts['order_id'], get_permalink( $atts['page_id'] )  ).'">'.$content.'</a>';
				}
		return ob_get_clean();
		
	}
}
?>