<?php class WCAM_Option
{
	var $cache_options;
	public function __construct()
	{
		
	}
	public function save_options($options)
	{
		$old_options = $this->get_option(); 
		
		//backslash issue fix
		//if (get_magic_quotes_gpc()) 
		{
			$process = array(&$options);//array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST)
			while (list($key, $val) = each($process)) {
				foreach ($val as $k => $v) {
					unset($process[$key][$k]);
					if (is_array($v)) {
						$process[$key][stripslashes($k)] = $v;
						$process[] = &$process[$key][stripslashes($k)];
					} else {
						$process[$key][stripslashes($k)] = stripslashes($v);
					}
				}
			}
			unset($process);
		}
		if(isset($old_options))
		{
			if(isset($old_options["product_tab_title"]))
			{
				$text = array_merge($old_options["product_tab_title"],  (array)$options["product_tab_title"]);
				$options['product_tab_title'] = $text;
			} 
			if(isset($old_options["title_box"]))
			{
				$text = array_merge($old_options["default_attachment_title"],  (array)$options["default_attachment_title"]);
				$options['default_attachment_title'] = $text;
			} 
			if(isset($old_options["title_box"]))
			{
				$text = array_merge($old_options["title_box"],  (array)$options["title_box"]);
				$options['title_box'] = $text;
			} 
			if(isset($old_options["download_view_button_text"]))
			{
				$text = array_merge($old_options["download_view_button_text"],  (array)$options["download_view_button_text"]);
				$options['download_view_button_text'] = $text;
			} 
			if(isset($old_options["approve_box_title"]))
			{
				$text = array_merge($old_options["approve_box_title"],  (array)$options["approve_box_title"]);
				$options['approve_box_title'] = $text;
			} 
			if(isset($old_options["approve_already_sent_message"]))
			{
				$text = array_merge($old_options["approve_already_sent_message"],  (array)$options["approve_already_sent_message"]);
				$options['approve_already_sent_message'] = $text;
			}
			if(isset($old_options["approve_not_yet_text"]))
			{
				$text = array_merge($old_options["approve_not_yet_text"],  (array)$options["approve_not_yet_text"]);
				$options['approve_not_yet_text'] = $text;
			} 
			if(isset($old_options["approve_yes_text"]))
			{
				$text = array_merge($old_options["approve_yes_text"],  (array)$options["approve_yes_text"]);
				$options['approve_yes_text'] = $text;
			} 
			if(isset($old_options["approve_no_text"]))
			{
				$text = array_merge($old_options["approve_no_text"],  (array)$options["approve_no_text"]);
				$options['approve_no_text'] = $text;
			} 
			if(isset($old_options["feedback_box_title"]))
			{
				$text = array_merge($old_options["feedback_box_title"],  (array)$options["feedback_box_title"]);
				$options['feedback_box_title'] = $text;
			} 
			if(isset($old_options["email_approve_box_title"]))
			{
				$text = array_merge($old_options["email_approve_box_title"],  (array)$options["email_approve_box_title"]);
				$options['email_approve_box_title'] = $text;
			} 
			if(isset($old_options["email_feedback_box_title"]))
			{
				$text = array_merge($old_options["email_feedback_box_title"],  (array)$options["email_feedback_box_title"]);
				$options['email_feedback_box_title'] = $text;
			}
			if(isset($old_options["email_no_customer_feedback_message"]))
			{
				$text = array_merge($old_options["email_no_customer_feedback_message"],  (array)$options["email_no_customer_feedback_message"]);
				$options['email_no_customer_feedback_message'] = $text;
			} 
			//automatic attachment emails
			if(isset($old_options["automatic_customer_new_account_email_attachments_title"]))
			{
				$text = array_merge($old_options["automatic_customer_new_account_email_attachments_title"],  (array)$options["automatic_customer_new_account_email_attachments_title"]);
				$options['automatic_customer_new_account_email_attachments_title'] = $text;
			} 
			if(isset($old_options["automatic_customer_new_account_email_attachments_body"]))
			{
				$text = array_merge($old_options["automatic_customer_new_account_email_attachments_body"],  (array)$options["automatic_customer_new_account_email_attachments_body"]);
				$options['automatic_customer_new_account_email_attachments_body'] = $text;
			} 
			if(isset($old_options["automatic_new_order_email_attachments_title"]))
			{
				$text = array_merge($old_options["automatic_new_order_email_attachments_title"],  (array)$options["automatic_new_order_email_attachments_title"]);
				$options['automatic_new_order_email_attachments_title'] = $text;
			} 
			if(isset($old_options["automatic_new_order_email_attachments_body"]))
			{
				$text = array_merge($old_options["automatic_new_order_email_attachments_body"],  (array)$options["automatic_new_order_email_attachments_body"]);
				$options['automatic_new_order_email_attachments_body'] = $text;
			} 
			if(isset($old_options["automatic_customer_processing_order_email_attachments_title"]))
			{
				$text = array_merge($old_options["automatic_customer_processing_order_email_attachments_title"],  (array)$options["automatic_customer_processing_order_email_attachments_title"]);
				$options['automatic_customer_processing_order_email_attachments_title'] = $text;
			} 
			if(isset($old_options["automatic_customer_processing_order_email_attachments_body"]))
			{
				$text = array_merge($old_options["automatic_customer_processing_order_email_attachments_body"],  (array)$options["automatic_customer_processing_order_email_attachments_body"]);
				$options['automatic_customer_processing_order_email_attachments_body'] = $text;
			} 
			if(isset($old_options["automatic_customer_completed_order_email_attachments_title"]))
			{
				$text = array_merge($old_options["automatic_customer_completed_order_email_attachments_title"],  (array)$options["automatic_customer_completed_order_email_attachments_title"]);
				$options['automatic_customer_completed_order_email_attachments_title'] = $text;
			} 
			if(isset($old_options["automatic_customer_completed_order_email_attachments_body"]))
			{
				$text = array_merge($old_options["automatic_customer_completed_order_email_attachments_body"],  (array)$options["automatic_customer_completed_order_email_attachments_body"]);
				$options['automatic_customer_completed_order_email_attachments_body'] = $text;
			} 
			if(isset($old_options["automatic_customer_invoice_email_attachments_title"]))
			{
				$text = array_merge($old_options["automatic_customer_invoice_email_attachments_title"],  (array)$options["automatic_customer_invoice_email_attachments_title"]);
				$options['automatic_customer_invoice_email_attachments_title'] = $text;
			} 
			if(isset($old_options["automatic_customer_invoice_email_attachments_body"]))
			{
				$text = array_merge($old_options["automatic_customer_invoice_email_attachments_body"], (array)$options["automatic_customer_invoice_email_attachments_body"]);
				$options['automatic_customer_invoice_email_attachments_body'] = $text;
			} 
			if(isset($old_options["product_attachments_box"]))
			{
				$text = array_merge($old_options["product_attachments_box"], (array)$options["product_attachments_box"]);
				$options['product_attachments_box'] = $text;
			} 
			if(isset($old_options["no_attachments_message"]))
			{
				$text = array_merge($old_options["no_attachments_message"], (array)$options["no_attachments_message"]);
				$options['no_attachments_message'] = $text;
			} 
			if(isset($old_options["view_attachments_button"]))
			{
				$text = array_merge($old_options["view_attachments_button"], (array)$options["view_attachments_button"]);
				$options['view_attachments_button'] = $text;
			} 
			if(isset($old_options["copy_to_clipboard_button_text"]))
			{
				$text = array_merge($old_options["copy_to_clipboard_button_text"], (array)$options["copy_to_clipboard_button_text"]);
				$options['copy_to_clipboard_button_text'] = $text;
			} 
		}
		update_option('wcam_general_options', $options);
	}
	public function get_option($option_name = null, $default_value = null)
	{
		$options = /* isset($this->cache_options) ? $this->cache_options : */ get_option('wcam_general_options'); 
		$options = isset($options) ? $options : array(); 
		//$options = isset($options) ? $options: $default_value;
		//$result = $default_value;
		
		//init 
		$options['status_to_assign_for_approval'] = isset($options['status_to_assign_for_approval']) ? $options['status_to_assign_for_approval'] : 'unchanged';
		$options['status_to_assign_for_not_approval'] = isset($options['status_to_assign_for_not_approval']) ? $options['status_to_assign_for_not_approval'] : 'unchanged';
		$options['status_to_assign_for_waiting_for_approval'] = isset($options['status_to_assign_for_waiting_for_approval']) ? $options['status_to_assign_for_waiting_for_approval'] : 'unchanged';
		
		if($option_name)
		{
			$result = isset($options[$option_name]) ? $options[$option_name] : $default_value;
			
		}
		else
			$result = $options;
		
		/* if(!isset($this->cache_options))
			$this->cache_options = $options; */
		
		return $result;
	}
	public function cl_acf_set_language() 
	{
	  return acf_get_setting('default_language');
	}
	public function get_emails_preset_text_by_id($id)
	{
		$texts = $this->get_emails_preset_texts();
		foreach((array)$texts as $text)
		{
			if($text['body_text_unique_field_id'] === $id)
				return $text['body_text_message'];
		}
		
		return "";
	}
	public function get_emails_preset_texts()
	{
		add_filter('acf/settings/current_language',  array(&$this, 'cl_acf_set_language'), 100);
		
		$all_data = array(); 
	
		if( have_rows('wcam_body_texts', 'option') )
			while ( have_rows('wcam_body_texts', 'option') ) 
			{
				the_row();
				$text = array();
				$text['body_text_title'] = get_sub_field('wcam_body_text_title'); //Check if value exists: if( $value )
				$text['body_text_message'] = nl2br(get_sub_field('wcam_body_text_message', false)); 
				$text['body_text_unique_field_id'] = get_sub_field('wcam_body_text_unique_field_id'); 
			
				$all_data[] = $text;
			}
			
		remove_filter('acf/settings/current_language', array(&$this,'cl_acf_set_language'), 100);
		return $all_data;
	}
	public function get_automatic_attacments_options($option_name = null, $default_value = null)
	{
		add_filter('acf/settings/current_language',  array(&$this, 'cl_acf_set_language'), 100);
		
		$all_data = array();
		//$all_data['hour_offset'] = get_field('wcst_hour_offset', 'option'); 
		$all_data['automatic_email_attachments'] = array(); 
	
		if( have_rows('wcam_automatic_email_attachments', 'option') )
			while ( have_rows('wcam_automatic_email_attachments', 'option') ) 
			{
				the_row();
				$automatic_attachments = array();
				$automatic_attachments['email_type'] = get_sub_field('wcam_email_types', 'option'); //Check if value exists: if( $value )
				$automatic_attachments['selected_file'] = get_sub_field('wcam_selected_file', 'option'); 
			
				$all_data['automatic_email_attachments'][] = $automatic_attachments;
			}
			
		remove_filter('acf/settings/current_language', array(&$this,'cl_acf_set_language'), 100);
		
		if(isset($option_name))
		{
			return isset($all_data[$option_name]) ? $all_data[$option_name] : $default_value;
		}
		
		/* Format
		array(1) {
		  ["automatic_email_attachments"]=>
		  array(1) {
			[0]=>
			array(2) {
			  ["email_type"]=>
			  array(2) {
				[0]=>
				string(20) "customer_new_account"
				[1]=>
				string(9) "new_order"
			  }
			  ["selected_file"]=>
			  int(1334)
			}
		  }
}
		*/
		
		return $all_data;
	}
	
}
?>