<?php 
function wcam_format_datetime_according_wordpress_settings($datetime)
{
	if(!isset($datetime) || $datetime == "")
		return $datetime;
	
	$date_format = get_option( 'date_format' )." ".get_option( 'time_format' );
	$date = DateTime::createFromFormat("Y/m/d H:i:s", $datetime );
	//wcam_var_dump($date);
	if(is_object($date))
			$datetime = $date->format($date_format);
	return $datetime;
}
function wcam_get_absolute_date_from_time_amout_plus_date($date, $time_amount, $time_type)
{
	$date_format = get_option( 'date_format' )." ".get_option( 'time_format' );
	$datetime = '+'.$time_amount." ".$time_type;
	$date  = date_create_from_format ("d/m/Y H:i:s", date('d/m/Y H:i:s', strtotime('+'.$time_amount.' '.$time_type, strtotime($date))));
	if(is_object($date))
			$datetime = $date->format($date_format);
	return $datetime;
}
function wcam_format_dateime_obj_according_wordpress($datetime)
{
	if(!is_object($datetime))
		return "";
	$date_format = get_option( 'date_format' )." ".get_option( 'time_format' );
	return $datetime->format($date_format);
}
function wcam_is_expiring_date_valid($expiring_strategy, $order_date, $format = "d/m/Y H:i:s", $specific_date = null, $time_amount = null,$time_amount_type = null)
{
	$options = new WCAM_Option();
	$time_offset = $options->get_option('time_offset', 0);
	$now = date_create_from_format ("d/m/Y H:i:s", date("d/m/Y H:i:s", strtotime($time_offset.' minute')));
	$expiring_date = "";
	$has_expired = false;
	if($expiring_strategy == 'specific_date')
	{
		$specific_date = strpos($specific_date, ':') === false ? $specific_date." 00:00:00" : $specific_date;
		$expiring_date =  date_create_from_format($format, $specific_date);
		$has_expired = $now < $expiring_date ? false : true;
	}
	else if($expiring_strategy == 'relative_date')
	{
		$order_date = strtotime($order_date);
		$expiring_date = date_create_from_format ("d/m/Y H:i:s", date($format, strtotime('+'.$time_amount.' '.$time_amount_type, $order_date)));
		$has_expired = $now < $expiring_date ? false : true;
	}
	return array( 'expiring_date' =>$expiring_date , 'has_expired'=> $has_expired); 
}