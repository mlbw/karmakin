<?php 
class WCAM_Product
{
	var $product_attachments_cache = array();
	public function __construct(){}
	
	public static function get_id($product)
	{
		if(version_compare( WC_VERSION, '2.7', '<' ))
			return $product->id;
		
		return $product->get_id();
	}
	public function get_attachments($product_id, $order = null, $file_id = null, $check_if_can_be_attached_to_email = false, $exclude_bulk_attachemnts = false)
	{
		global $wcam_bulk_products_attachments_options;
		
		$all_data = isset($this->product_attachments_cache[$product_id]) ? $this->product_attachments_cache[$product_id] : array();
		if(isset($order))
		{
			$order_status = $order->get_status( );
			$status = "wc-".$order_status;
		}
		$product_type = 'simple';
		$parent_product = wp_get_post_parent_id($product_id);
		$variations = $parent_product == false ? $this->get_variations($product_id) : array();
		$variation_id = 0;
		if($parent_product != false)
		{
			$variation_id = $product_id;
			$product_id = $parent_product;
			$product_type = 'variation';
		}
		else if($parent_product == false && count($variations) > 0)
		{
			$product_type = 'variable';
		}
		
		$options = new WCAM_Option();
		if( !isset($this->product_attachments_cache[$product_id]) && have_rows('wcam_products_attachments',$product_id) )
		{
			while ( have_rows('wcam_products_attachments', $product_id) ) 
			{
				the_row();
				$file_data = get_sub_field('wcam_file');				
				$allowed_variants = get_sub_field('wcam_variants') ? get_sub_field('wcam_variants') : array();
				if( $file_data  && 
				   (!isset($order) || in_array($status, get_sub_field('wcam_order_status_visibility'))) &&
				   ($product_type != 'variation' || empty($allowed_variants) || in_array($variation_id, $allowed_variants))
				  )
				{
					/* format:
					array(7) {
					  ["id"]=>
					  string(32) "a9707f10c9a7b33fad0659f4ca35f458"
					  ["file_name"]=>
					  string(4) "test"
					  ["file_description"]=>
					  string(0) ""
					  ["file_url"]=>
					  string(99) "http://xxx.com/woocommerce/wp-content/uploads/2015/07/xxxx.jpg"
					  ["file_path"]=>
					  string(132) "/var/directory/file.jpg"
					  ["order_status_visibility"]=>
					  array(2) {
						[0]=>
						string(10) "wc-on-hold"
						[1]=>
						string(0) ""
					  }
					  ["attach_file_to_woocoommerce_status_email"]=>
					  string(3) "yes"
					}
					*/					
					//$time_offset = $options->get_option('time_offset', 0);
					$rule_id = get_sub_field('wcam_rule_id');
					$can_be_attached = get_sub_field('wcam_attach_file_to_woocoommerce_status_email');
					$visible_only_in_emails = get_sub_field('wcam_file_is_only_attachend_to_the_email');
					$expiring_strategy = get_sub_field('wcam_expiring_strategy');
					$specific_date = get_sub_field('wcam_specific_date');
					$time_amount = get_sub_field('wcam_time_amount'); 
					$time_amount_type = get_sub_field('wcam_time_amount_type');
					//validation					
					$expiring_strategy = $expiring_strategy ? $expiring_strategy : 'none';
					$specific_date = $specific_date ? $specific_date : 'none';
					$time_amount = $time_amount ? $time_amount : 1; 
					$time_amount_type = $time_amount_type ? $time_amount_type : 'none';

					$has_expired = $expiring_strategy == 'none' ? false : true;
					//$now = date_create_from_format ("d/m/Y H:i:s", date("d/m/Y H:i:s", strtotime($time_offset.' minute')));
					$expiring_date = "";
					
					/*backward compatibility*/
					$visible_only_in_emails = $visible_only_in_emails ? $visible_only_in_emails : 'no';
					
					if(!isset($order))
					{
						$has_expired  = false;
					}
					else
					{
						$result = wcam_is_expiring_date_valid($expiring_strategy, WCAM_Order::get_date_created($order), "d/m/Y H:i:s", $specific_date, $time_amount ,$time_amount_type);	
						$has_expired = $result['has_expired'];
						$expiring_date = $result['expiring_date'];
					}
					
					if((!isset($file_id) || $file_id == @md5($file_data['url']/* @file_get_contents($file_data['url']) */)) &&
						(!$check_if_can_be_attached_to_email || $can_be_attached == 'yes') 
						//&& (!$has_expired) //$file_data['filename']
						)
					{
						$attachment = array();
						//$attachment['id'] =@md5(@file_get_contents($file_data['url'])); //Check if value exists: if( $value ) 
						$attachment['id'] =@md5($file_data['url']); //Check if value exists: if( $value ) 
						//$attachment['rule_id'] = $rule_id ? $rule_id : @md5(@file_get_contents($file_data['url']));
						$attachment['rule_id'] = $rule_id ? $rule_id : @md5($file_data['url']);
						$attachment['file_name'] = get_sub_field('wcam_file_name'); 
						$attachment['file_description'] = get_sub_field('wcam_file_description'); 
						$attachment['file_url'] = $has_expired ? "" : $file_data['url']; 
						$attachment['file_path'] =  $has_expired ? "" : get_attached_file( $file_data['id'] ); 
						$attachment['attachment_visibility'] = get_sub_field('wcam_attachment_visibility'); // order_details_page || product_page || all_pages
						$attachment['attachment_visibility'] = $attachment['attachment_visibility'] ? $attachment['attachment_visibility'] : 'order_details_page';
						$attachment['order_status_visibility'] = get_sub_field('wcam_order_status_visibility'); 
						$attachment['attach_file_to_woocoommerce_status_email'] = /* $has_expired ? 'no' : */ $can_be_attached; 
						$attachment['visible_only_in_emails'] = $visible_only_in_emails; 
						$attachment['expiring_strategy'] = $expiring_strategy; //none, specific_date, relative_date
						$attachment['specific_date'] = $specific_date; //d/m/Y
						$attachment['time_amount'] = $time_amount; 
						$attachment['time_amount_type'] = $time_amount_type; //secods, minutes, hours, days, month
						$attachment['expiring_date'] = $expiring_date; 
						$attachment['has_expired'] = $has_expired; 
						
						$all_data[$attachment['rule_id']] = $attachment;
					}
				}
			}
			$this->product_attachments_cache[$product_id] = $all_data;
		}
			
		//Include main product attachments
		if($product_type == 'simple' || $product_type == 'variation')
		{
			$main_product_id = $product_type == 'simple' ? $product_id : $variation_id;
			$bulk_assigned_rules = $wcam_bulk_products_attachments_options->get_rules_by_product_id($main_product_id, $order, $file_id, $check_if_can_be_attached_to_email);
			if(is_array($bulk_assigned_rules) && !empty($bulk_assigned_rules))
				$all_data = $all_data + $bulk_assigned_rules;//array_merge($all_data,$bulk_assigned_rules);
		}	
		//Include children product attachments in case of variable product
		else if($product_type == 'variable')
		{
			foreach((array)$variations as $variation)
			{
				$bulk_assigned_rules = $wcam_bulk_products_attachments_options->get_rules_by_product_id($variation->ID, $order, $file_id, $check_if_can_be_attached_to_email);
				if(is_array($bulk_assigned_rules) && !empty($bulk_assigned_rules))
					$all_data = $all_data + $bulk_assigned_rules;//array_merge($all_data,$bulk_assigned_rules);
			}
		}
		
		return $all_data;
	}
	public function get_attachments_downloadable_in_product_page($product)
	{
		$products_data = array();
		$data = array();
		$data_temp = $this->get_attachments(WCAM_Product::get_id($product)); 
		
		//Filtering attachment visible only as emails attachment
		foreach((array)$data_temp as $new_attachemnt)
		{
			if( ($new_attachemnt['attach_file_to_woocoommerce_status_email'] == 'yes' && $new_attachemnt['visible_only_in_emails'] == 'yes') ||
				 $new_attachemnt['attachment_visibility'] == 'order_details_page'
			)
			continue; 
						
			$data[] = $new_attachemnt;
		}
		
		if(!empty($data) )
		{
			if(!isset($products_data[$product->get_name()]))
				$products_data[$product->get_name()] = $data; 
			else
			{
				//$products_data[$product['name']] = array_merge($products_data[$product['name']],$data);
				foreach($data as $new_attachemnt) //useless, can be used the merge method
				{
					$already_exists = false;
					foreach((array)$products_data[$product->get_name()] as $attachment)
						if($new_attachemnt['rule_id'] == $attachment['rule_id'])
							$already_exists = true;
						
					if(!$already_exists )
						$products_data[$product->get_name()][] = $new_attachemnt;
				} 
			}
		}
		return $products_data;
	}
	public function get_attachments_downloadable_in_order_details_page($order)
	{
		$products_data = array();
		foreach($order->get_items() as $product)
		{		
				$product_id = $product['variation_id'] == 0 ? $product['product_id'] : $product['variation_id'];
				//wcam_var_dump($product_id);
				$data_temp = $this->get_attachments($product_id, $order);  //product_id variation_id
				$data = array();
					
				//Filtering attachment visible only as emails attachment
				foreach((array)$data_temp as $new_attachemnt)
				{
					if( ($new_attachemnt['attach_file_to_woocoommerce_status_email'] == 'yes' && $new_attachemnt['visible_only_in_emails'] == 'yes') ||
						$new_attachemnt['attachment_visibility'] == 'product_page')
								continue; 
							
					$data[] = $new_attachemnt;
				}
				
				if(!empty($data) )
				{
					if(!isset($products_data[$product['name']]))
						$products_data[$product['name']] = $data; 
					else
					{
						//$products_data[$product['name']] = array_merge($products_data[$product['name']],$data);
						foreach($data as $new_attachemnt) //useless, can be used the merge method
						{
							$already_exists = false;
							foreach((array)$products_data[$product['name']] as $attachment)
								if($new_attachemnt['rule_id'] == $attachment['rule_id'])
									$already_exists = true;
								
							if(!$already_exists)
								$products_data[$product['name']][] = $new_attachemnt;
						} 
					}
				}
		}
		return $products_data;
	}
	public function get_attachments_per_order_id($order_id)
	{
		$order = new WC_Order($order_id);
		$files = array();
		//$status = isset($status) ? $status : $order->get_status( );
		foreach($order->get_items() as $product)
		{		
			$product_id = $product['variation_id'] == 0 ? $product['product_id'] : $product['variation_id'];
			$attachments = $this->get_attachments($product_id, $order, null, true);  //product_id variation_id
			if(!empty($attachments) )
			{
				$files[] = $attachments;
			}					
		}
		return $files;
	}
	public function get_order_product_attachment_by_id($order_id, $attachment_id)
	{
		$order = new WC_Order($order_id);
		$file = null;
		if(current_user_can( 'manage_options' ) || current_user_can( 'manage_woocommerce' ) ||  WCAM_Order::get_customer_id($order) == get_current_user_id())
		{
			foreach($order->get_items() as $product)
			{		
				$product_id = $product['variation_id'] == 0 ? $product['product_id'] : $product['variation_id'];
				$file = $this->get_attachments($product_id , $order, $attachment_id);  //product_id variation_id
				if(!empty($file) )
				{
					$file = array_shift($file);
					return $file;	
				}					
			}
		}
		return $file;
	}
	public static function get_variations($product_id)
	 {
		global $wpdb;
		$wpml = new WCAM_Wpml();
		if($wpml->is_wpml_active())
			$product_id = $wpml->get_original_id($product_id);
		
		 $query = "SELECT products.ID
		           FROM {$wpdb->posts} AS products 
				   WHERE products.post_parent = {$product_id} AND products.post_type = 'product_variation' "; //_regular_price
		 $result =  $wpdb->get_results($query); 
		 //wcpst_var_dump($result);
		 return isset($result) ? $result : array();		 
	 }
	public function get_complementry_ids($ids_to_exclude/* , $post_type = "product" */) //product_variation
	{
		global $wpdb;
		$results = array();
		$query = "SELECT posts.ID 
				  FROM {$wpdb->posts} AS posts
				  WHERE (posts.post_type = 'product' OR posts.post_type = 'product_variation')
				  AND posts.ID NOT IN('".implode("','",$ids_to_exclude)."') ";
		$ids = $wpdb->get_results($query, ARRAY_A);
		foreach($ids as $id)
			$results[] = (int)$id['ID'];
		return $results;
	}
	public function get_products_ids_using_categories($category_type_name, $selected_categories, $get_post_belonging_to_children_categories, $strategy)
	{
		//$get_post_belonging_to_children_categories : "selected_only" || "all_children"
		
		global $wpdb;
		$not_suffix = $strategy == "all" ? "  " : " NOT ";
		$results = $additional_categories_ids = array();
		
		//Retrieve children categories id
		if($get_post_belonging_to_children_categories == 'all_children')
		{
			foreach($selected_categories as $current_category)
			{
				$args = array(
						'type'                     => 'post',
						'child_of'                 => $current_category,
						'parent'                   => '',
						'orderby'                  => 'name',
						'order'                    => 'ASC',
						'hide_empty'               => 1,
						'hierarchical'             => 1,
						'exclude'                  => '',
						'include'                  => '',
						'number'                   => '',
						'taxonomy'                 => $category_type_name,
						'pad_counts'               => false

					); 

					$categories = get_categories( $args );
					//wctbp_var_dump($categories);
					foreach($categories as $result)
					{
						if(!is_array($result))
							$additional_categories_ids[] = (int)$result->term_id;
					}
			}
		}
		if(!empty($additional_categories_ids))
			$selected_categories = array_merge($selected_categories, $additional_categories_ids);
		
		//GROUP_CONCAT(posts.ID)
		$wpdb->query('SET group_concat_max_len=5000000'); 
		$wpdb->query('SET SQL_BIG_SELECTS=1');
		$query = "SELECT DISTINCT posts.ID
				 FROM {$wpdb->posts} AS posts 
				 INNER JOIN {$wpdb->term_relationships} AS term_rel ON term_rel.object_id = posts.ID
				 INNER JOIN {$wpdb->term_taxonomy} AS term_tax ON term_tax.term_taxonomy_id = term_rel.term_taxonomy_id 
				 INNER JOIN {$wpdb->terms} AS terms ON terms.term_id = term_tax.term_id
				 WHERE  terms.term_id {$not_suffix} IN ('" . implode( "','", $selected_categories). "')  
				 AND term_tax.taxonomy = '{$category_type_name}' "; 
		$ids = $wpdb->get_results($query, ARRAY_A);
	
		foreach($ids as $id)
			$results[] = $id['ID'];
		return $results;
	}
}
?>