<?php 
class WCAM_Order
{
	public function __construct()
	{
		
	}
	static function get_customer_id($order)
	{
		if(version_compare( WC_VERSION, '2.7', '<' ))
			return $order->customer_user;
		
		return $order->get_customer_id();
	}
	public static function get_id($order)
	{
		if(version_compare( WC_VERSION, '2.7', '<' ))
			return $order->id;
		
		return $order->get_id();
	}
	public static function get_date_created($order)
	{
		if(version_compare( WC_VERSION, '2.7', '<' ))
			return $order->order_date;
		
		$object = $order->get_date_created();
		return $object->date('Y-m-d H:i');
	}
	public function get_user_orders($user_id)
	{
		$customer_orders = get_posts( array(
				'numberposts' => -1,
				'meta_key'    => '_customer_user',
				'meta_value'  => $user_id,
				'post_type'   => wc_get_order_types(),
				'post_status' => array_keys( wc_get_order_statuses() ),
			) );
			
		return $customer_orders;
	}
	public function get_attachments_metadata($order_id, $default_value = null)
	{
		$wcam_meta_data = get_post_meta($order_id, '_wcam_attachments_meta');
		$wcam_meta_data = isset($wcam_meta_data) && isset($wcam_meta_data[0]) ? $wcam_meta_data[0]:$default_value;
		return $wcam_meta_data;
	}
	public function number_of_visible_attachments($order)
	{
		$exists_at_least_one_attachment_to_show = 0;
		$file_order_metadata = $this->get_attachments_metadata(WCAM_Order::get_id($order), array());
		foreach($file_order_metadata as $file_fields)
		{
			$current_status = "wc-".$order->get_status( );
			
			if( !$this->is_attachment_expired($file_fields, WCAM_Order::get_date_created($order)) && !isset($file_fields['order-attachment-hide-by-status']) || !in_array($current_status, $file_fields['order-attachment-hide-by-status']))
			{
				$exists_at_least_one_attachment_to_show++;
			}
		}
		
		return $exists_at_least_one_attachment_to_show;
	}
	public function get_attachmets_meta($order_id)
	{
		$file_order_metadata = get_post_meta($order_id, '_wcam_attachments_meta');
		return isset($file_order_metadata[0]) ? $file_order_metadata[0]:array();
	}
	public function get_post_options($order_id)
	{
		$wcam_options = get_post_meta( $order_id, '_wcam_options');
		return isset($wcam_options) && isset($wcam_options[0]) ? $wcam_options[0]:null;
	}
	public function update_meta($order_id, $key, $value)
	{
		update_post_meta( $order_id, $key, $value);
	}
	public function process_upload_csv_chunk($csv_array)
	{
		$attachmets_added = 0;
		$errors = array();
		$messages = array();
		$columns_names = array("order_id",
								"title",
								"url",
								"customer-has-to-be-approved",
								"customer-can-reapprove",
								"customer-feedback-enabled",
								"customer-admin-notification"/* ,
								"attach-file-to-complete-order-email" */);
		$colum_index_to_name = array();
		$row = 1;
		$real_row = 1;
		if($csv_array != null)
		{
			foreach($csv_array as $csv_row)
			{
				$data = str_getcsv($csv_row);
				$num = count($data);
				$attachment = array();
				
				for ($c=0; $c < $num; $c++) 
				{						
					if($row == 1)
					{
						foreach( $columns_names as $title)
							if($title == $data[$c])
									$colum_index_to_name[$c] = $title;
					}
					else
					{
						if(isset($colum_index_to_name[$c]))
						{
							//echo $colum_index_to_name[$c] . "<br />\n";
							$attachment[$colum_index_to_name[$c]] = $data[$c];
						}
					}
					
				}
				if($attachment != null && !empty($attachment) && isset($attachment['order_id']))
				{
					//wcam_var_dump($attachment);
					if ( isset($attachment['order_id']) && isset($attachment['title']) && isset($attachment['url']))
					{
						$attachments_meta = get_post_meta($attachment['order_id'], '_wcam_attachments_meta');
						$id = 0;
						$file_order_metadata = array();
						$file_order_metadata[$id] = array();
						//var_dump($attachments_meta);
						if(isset($attachments_meta[0]))
						{
							foreach($attachments_meta[0] as $attachment_meta)		
								$id = $attachment_meta['id'] > $id ? $attachment_meta['id'] : $id; 
							$id++;
							
							$file_order_metadata = $attachments_meta[0];
						}
						
						/* $file_order_metadata[$id]['title'] = $attachment['title'];
						$file_order_metadata[$id]['id'] = $id;
						$file_order_metadata[$id]['url'] = $attachment['url'];
						$file_order_metadata[$id]['secure-download'] = isset($attachment['secure-download']) ? $attachment['secure-download']: "no";
						$file_order_metadata[$id]['customer-has-to-be-approved'] = isset($attachment['customer-has-to-be-approved']) ? $attachment['customer-has-to-be-approved']: "no";
						$file_order_metadata[$id]['customer-can-reapprove'] = isset($attachment['customer-can-reapprove']) ? $attachment['customer-can-reapprove']: "no";
						$file_order_metadata[$id]['customer-feedback-enabled'] = isset($attachment['customer-feedback-enabled']) ? $attachment['customer-feedback-enabled'] : "no";
						$file_order_metadata[$id]['customer-admin-notification'] = isset($attachment['customer-admin-notification']) ? $attachment['customer-admin-notification']: "no";
						$file_order_metadata[$id]['attach-file-to-processing-order-email'] = isset($attachment['attach-file-to-processing-order-email']) ? $attachment['attach-file-to-processing-order-email'] : "no";
						$file_order_metadata[$id]['attach-file-to-complete-order-email'] = isset($attachment['attach-file-to-complete-order-email']) ? $attachment['attach-file-to-complete-order-email'] : "no";
						$file_order_metadata[$id]['attach-file-to-customer-invoice-email'] = isset($attachment['attach-file-to-customer-invoice-email']) ? $attachment['attach-file-to-customer-invoice-email'] : "no"; */
						$this->save_single_attachment_meta_data($id, $attachment, $file_order_metadata, array($id => $attachment['title']), $attachment['order_id'], true, $attachment['url']);
						//update_post_meta( $attachment['order_id'], '_wcam_attachments_meta', $file_order_metadata);
						$this->update_meta( $attachment['order_id'], '_wcam_attachments_meta', $file_order_metadata);
						//wcam_var_dump($attachment);
						$attachmets_added++;
					}
					else
					{
						if(!isset($attachment['title']))
							array_push( $errors, new WP_Error('order', sprintf(__("Order attachment title is not valid for %d.", 'woocommerce-attach-me'), $real_row)));
						else 
							array_push( $errors, new WP_Error('order', sprintf(__("Order attachment url is not valid for %d.", 'woocommerce-attach-me'), $real_row)));
						
					}
						
						$real_row++;
				}
				$row++;
			}
			array_push( $messages, sprintf(__('Added %d attachments!', 'woocommerce-attach-me'),  $attachmets_added ));
			//fclose($handle);
		}
		return array('messages' => $messages, 'errors' => $errors);
	}
	public function save_attachments($meta_info, $files)
	{
		$results = $errors = "";
		global $wcam_file_model;
		$order_id = $meta_info["wcam-orderid"];
		$file_order_metadata = $this->get_attachmets_meta($order_id );
		
		
		//Delete
		if(isset($meta_info['wcam_attachments_to_delete']))
		{
			foreach($meta_info['wcam_attachments_to_delete'] as $value)
			{
				$file_order_metadata = $wcam_file_model->delete_file(intval($value), $file_order_metadata, $order_id);
			}
		} 
		
		$titles = isset($meta_info['wcam_attachment_title']) ? $meta_info['wcam_attachment_title'] : array();
		//wcam_var_dump($meta_info);
		
		//Upload
		if(isset($files))
		{
			$wcam_file_model->upload_files($order_id, $file_order_metadata, $files, $titles, $meta_info);
		}
		if(isset($meta_info['wcam_attachment_external_url']))
		{	
			$this->bulk_save_meta_data($titles, $meta_info, $file_order_metadata, 'wcam_attachment_external_url', $order_id);	
		}
		if(isset($meta_info['wcam_attachment_gallery_url']))
		{	
			$this->bulk_save_meta_data($titles, $meta_info, $file_order_metadata, 'wcam_attachment_gallery_url', $order_id);
		}
		
		
		//update_post_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		$this->update_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		
		//general options
		$wcam_options = array();
		$wcam_options['embed-links-preset-email-text-id'] = isset($meta_info['wcam-embed-links-preset-email-text-id']) ? $meta_info['wcam-embed-links-preset-email-text-id'] : "no";
		$wcam_options['notification-preset-email-text-id'] = isset($meta_info['wcam-notification-preset-email-text-id']) ? $meta_info['wcam-notification-preset-email-text-id'] : "no";
		$wcam_options['embed-links-to-complete-mail'] = isset($meta_info['wcam-embed-links-to-complete-mail']) ? $meta_info['wcam-embed-links-to-complete-mail'] : "no";
		$wcam_options['embed-links-hide-by-status'] = isset($meta_info['wcam-embed-links-hide-by-status']) ? $meta_info['wcam-embed-links-hide-by-status'] : array();
		$wcam_options['embed-links-to-complete-mail-text'] = isset($meta_info['wcam-embed-links-to-complete-mail-text']) ? $meta_info['wcam-embed-links-to-complete-mail-text'] : ""; 
		//update_post_meta( $order_id, '_wcam_options', $wcam_options);
		$this->update_meta( $order_id, '_wcam_options', $wcam_options);
		
		//Email notification
		if(isset($meta_info['wcam-notify-via-mail']) && $meta_info['wcam-notify-via-mail'] == 'yes')
		{
			$results = WCAM_Notifier::send_user_email($meta_info, $file_order_metadata,$order_id);
		}	
		return array('results' => $results, 'errors'=> $errors);		
	}
	public function bulk_save_meta_data($titles, $meta_info, &$file_order_metadata, $type, $order_id)
	{
		foreach($meta_info[$type] as $index => $url)
		{
			if($url == "")
				continue;
			
			$id = $meta_info['wcam_id'][$index];
			$this->save_single_attachment_meta_data($id, $meta_info, $file_order_metadata, $titles, $order_id);
		
			/* if('wcam_attachment_file')
			{
				$file_order_metadata[$id]['url'] = $file_order_metadata[$id]['secure-download'] == "no" ? $movefile['url'] :  get_site_url()."?wcam_id=".$order_id."-".$id;
			} */
			if($type == 'wcam_attachment_external_url')
			{
				$file_order_metadata[$id]['absolute_path'] = "";
				$file_order_metadata[$id]['url'] = $url;
			}
			
			if($type == 'wcam_attachment_gallery_url')
			{
				$file_order_metadata[$id]['url'] = $file_order_metadata[$id]['secure-download'] == "no" ? $url :  get_site_url()."?wcam_id=".$order_id."-".$id;
				$file_order_metadata[$id]['media_from_gallery'] = "yes";
				$file_order_metadata[$id]['media_id'] = isset($meta_info['wcam_attachment_gallery_media_id']) && isset($meta_info['wcam_attachment_gallery_media_id'][$id]) ? $meta_info['wcam_attachment_gallery_media_id'][$id] : -1;
				$file_order_metadata[$id]['absolute_path'] = $file_order_metadata[$id]['media_id'] !=0 ? get_attached_file( $file_order_metadata[$id]['media_id'] ) : "";
			}
		}
	}
	public function save_single_attachment_meta_data($id, $meta_info, &$file_order_metadata, $titles, $order_id, $absolute_path = null, $url = null)
	{
		//Common metadata
		$file_order_metadata[$id]['title'] = !empty($titles) && isset($titles[$id]) ? $titles[$id] : "";
		$file_order_metadata[$id]['id'] = $id;
		$file_order_metadata[$id]['secure-download'] = isset($meta_info['wcam-secure-download'][$id]) ? $meta_info['wcam-secure-download'][$id] : "no" ;
		$file_order_metadata[$id]['customer-has-to-be-approved'] = isset($meta_info['wcam-customer-approve-checkbox'][$id]) ? $meta_info['wcam-customer-approve-checkbox'][$id] : "no";
		$file_order_metadata[$id]['customer-can-reapprove'] = isset($meta_info['wcam-customer-reapprove-checkbox'][$id]) ? $meta_info['wcam-customer-reapprove-checkbox'][$id]: "no";
		$file_order_metadata[$id]['customer-feedback-enabled'] = isset($meta_info['wcam-customer-feedback-checkbox'][$id]) ? $meta_info['wcam-customer-feedback-checkbox'][$id] : "no";
		$file_order_metadata[$id]['customer-feedback-strategy'] = isset($meta_info['wcam-customer-feedback-strategy'][$id]) ? $meta_info['wcam-customer-feedback-strategy'][$id] : "never";
		//$file_order_metadata[$id]['customer-feedback-required'] = isset($meta_info['wcam-customer-feedback-required'][$id]) ? $meta_info['wcam-customer-feedback-required'][$id] : "no";
		$file_order_metadata[$id]['customer-admin-notification'] = isset($meta_info['wcam-customer-admin-notification-checkbox'][$id]) ? $meta_info['wcam-customer-admin-notification-checkbox'][$id] : "no";
		$file_order_metadata[$id]['attach-file-to-processing-order-email'] = isset($meta_info['wcam-attach-file-to-processing-order-email'][$id]) ? $meta_info['wcam-attach-file-to-processing-order-email'][$id] : "no";
		$file_order_metadata[$id]['attach-file-to-complete-order-email'] = isset($meta_info['wcam-attach-file-to-complete-order-email'][$id]) ? $meta_info['wcam-attach-file-to-complete-order-email'][$id] : "no";
		$file_order_metadata[$id]['attach-file-to-customer-invoice-email'] = isset($meta_info['wcam-attach-file-to-customer-invoice-email'][$id]) ? $meta_info['wcam-attach-file-to-customer-invoice-email'][$id] : "no";
		$file_order_metadata[$id]['order-attachment-hide-by-status'] = isset($meta_info['wcam-order-attachment-hide-by-status'][$id]) ? $meta_info['wcam-order-attachment-hide-by-status'][$id] : array();
		$file_order_metadata[$id]['display-copy-to-clipboard-button'] = isset($meta_info['wcam-display-copy-to-clipboard-button'][$id]) ? $meta_info['wcam-display-copy-to-clipboard-button'][$id] : "no";
		//expiration
		$file_order_metadata[$id]['expiration-strategy'] = isset($meta_info['wcam_expiration_strategy'][$id]) ? $meta_info['wcam_expiration_strategy'][$id] : "never";
		$file_order_metadata[$id]['expiration-relative-time-amount'] = isset($meta_info['wcam_expiration_relative_time_amount'][$id]) ? $meta_info['wcam_expiration_relative_time_amount'][$id] : "";
		$file_order_metadata[$id]['expiration-relative-time-type'] = isset($meta_info['wcam_expiration_relative_time_type'][$id]) ? $meta_info['wcam_expiration_relative_time_type'][$id] : "";
		$file_order_metadata[$id]['expiration-specific-date'] = isset($meta_info['wcam_expiration_specific_date'][$id]) ? $meta_info['wcam_expiration_specific_date'][$id] : "";
		
		if(isset($absolute_path))
		{
			$file_order_metadata[$id]['absolute_path'] = $absolute_path;
			$file_order_metadata[$id]['url'] = $file_order_metadata[$id]['secure-download'] == "no" ? $url :  get_site_url()."?wcam_id=".$order_id."-".$id;
		}
		//wcam_var_dump($file_order_metadata[$id]);
	}
	public function is_attachment_expired($attachment_data, $order_date)
	{
		
		if(!isset($attachment_data['expiration-strategy']))
			return false;
		
		$format = $attachment_data['expiration-strategy'] == 'specific_date' ? 'Y/m/d H:i:s' : "d/m/Y H:i:s";
		/* $options = new WCAM_Option();
		$time_offset = $options->get_option('time_offset', 0);
		$now = date_create_from_format ("d/m/Y H:i:s", date("d/m/Y H:i:s", strtotime($time_offset.' minute')));
		$has_expired = false;
		if($attachment_data['expiration-strategy'] == 'specific_date')
		{
			$expiring_date =  new DateTime(date('Y/m/d H:i:s', strtotime($attachment_data['expiration-specific-date'])));
			$has_expired = $now < $expiring_date ? false : true;
		}
		else if($attachment_data['expiration-strategy'] == 'relative_date')
		{
			$order_date = strtotime($order_date);
			$expiring_date = date_create_from_format ("d/m/Y H:i:s", date('d/m/Y H:i:s', strtotime('+'.$attachment_data['expiration-relative-time-amount']." ".$attachment_data['expiration-relative-time-type'], $order_date)));
			$has_expired = $now < $expiring_date ? false : true;
		} */
		$result = wcam_is_expiring_date_valid($attachment_data['expiration-strategy'], $order_date, $format, $attachment_data['expiration-specific-date'], $attachment_data['expiration-relative-time-amount'] ,$attachment_data['expiration-relative-time-type']);	
		$has_expired = $result['has_expired'];
		$expiring_date = $result['expiring_date'];
						
		return $has_expired;
	}
	public function get_attachment_expiration_date($order_id, $attachment_meta)
	{
		$order = new WC_Order($order_id);
		$date = "";
		if(isset($attachment_meta['expiration-strategy']))
			switch($attachment_meta['expiration-strategy'])
			{
				case 'specific_date': $date = wcam_format_datetime_according_wordpress_settings($attachment_meta['expiration-specific-date']); 
				break;
				case 'relative_date': $date = wcam_get_absolute_date_from_time_amout_plus_date(WCAM_Order::get_date_created($order), $attachment_meta['expiration-relative-time-amount'], $attachment_meta['expiration-relative-time-type']); 
				break;
			} 
		return $date;
	}
}
?>