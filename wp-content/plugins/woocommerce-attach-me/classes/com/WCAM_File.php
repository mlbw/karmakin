<?php 
class WCAM_File
{
	public function __construct()
	{
		add_action( 'before_delete_post', array( &$this, 'delete_all_order_attachments' ), 10 );
		add_action('init', array( &$this, 'process_secure_download_request' ));
	}
	 private function real_filesize($file) {

      $fmod = filesize($file);
      if ($fmod < 0) $fmod += 2.0 * (PHP_INT_MAX + 1);
      $i = 0;
      $myfile = fopen($file, "r");
      while (strlen(fread($myfile, 1)) === 1) {
        fseek($myfile, PHP_INT_MAX, SEEK_CUR);
        $i++;
      }
      fclose($myfile);
      if ($i % 2 == 1) $i--;

      return ((float)($i) * (PHP_INT_MAX + 1)) + $fmod;

    }
	public function process_secure_download_request()
	{
		if(!isset($_GET['wcam_id']) && !isset($_GET['wcam_login']) && !isset($_GET['wcam_attachment_id']))
			return;
			
		if(isset($_GET['wcam_login']))
		{
			wc_add_notice( __( 'You have to be logged for that kind of request.', 'woocommerce-attach-me' ), 'notice' );
			return;
		}
		
		list($order_id, $file_id) = explode("-", isset($_GET['wcam_id']) ? $_GET['wcam_id'] : $_GET['wcam_attachment_id'] );
		if(!isset($order_id) || !isset($file_id))
			return;
		
		if(!is_user_logged_in())
		{
			//wc_add_notice( __( 'You have to be logged for that kind of request.', 'woocommerce-attach-me' ), 'notice' );
            wp_redirect(add_query_arg('wcam_login', 'yes', get_permalink( get_option('woocommerce_myaccount_page_id') )  )); 
            exit; 
		}
		
		if(isset($_GET['wcam_id']))
			$this->process_order_attachment($order_id, $file_id);
		else if(isset($_GET['wcam_attachment_id']))
			$this->process_order_product_attachments($order_id, $file_id);
	}
	private function process_order_product_attachments($order_id, $file_id)
	{
		global $wcam_product_model;
		$file = $wcam_product_model->get_order_product_attachment_by_id($order_id, $file_id);
		if(isset($file))
		{
			$this->output_file($file['file_path']);
			wp_die();
			//wc_add_notice(__( 'File is not longer accessible due order status change or because it has been deleted.', 'woocommerce-attach-me' ), 'error');
		}
		else
			wc_add_notice(__( 'You are not authorized to view this file.', 'woocommerce-attach-me' ), 'error');
	}
	private function process_order_attachment($order_id, $file_id)
	{
		global $wcam_order_model;
		$order = new WC_Order($order_id);
		//wcam_var_dump($order->get_customer_id());		
		if(current_user_can( 'manage_options' ) || current_user_can( 'manage_woocommerce' ) ||  WCAM_Order::get_customer_id($order) == get_current_user_id())
		{
			$attachments_meta = get_post_meta($order_id, '_wcam_attachments_meta');
			$attachments_meta = isset($attachments_meta) ? $attachments_meta[0] : false;
			if($attachments_meta == false || $wcam_order_model->is_attachment_expired($attachments_meta[$file_id], WCAM_Order::get_date_created($order)))
				return;
			else
			{
				//wcam_var_dump($attachments_meta[$file_id]);
				$this->output_file($attachments_meta[$file_id]['absolute_path']);
				wp_die();
			}
		}
		else
			wc_add_notice(__( 'You are not authorized to view this file.', 'woocommerce-attach-me' ), 'error');
	}
	private function output_file($path)
	{
		/* $path = $attachments_meta[$file_id]['absolute_path'];*/
		$size = filesize($path); 
		$fileName = basename($path);
		
		header("Content-length: ".$size);
		header("Content-type: application/octet-stream");
		header("Content-disposition: attachment; filename=".$fileName.";" );
		
		//header('Content-Transfer-Encoding: binary');
		header('Content-Transfer-Encoding: chunked');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header("Content-Type: application/download");
		header('Content-Description: File Transfer');
		header('Content-Type: application/force-download');
		//echo $content;
		readfile($path);
		/* if ($fd = fopen ($path, "r")) 
		{

				set_time_limit(0);
				ini_set('memory_limit', '1024M');
			
			if (ob_get_contents()) ob_clean();
			while(!feof($fd)) {
				echo fread($fd, 4096);
			}   
			flush();
			ob_end_flush();
		} */
		exit();
	}
	public function wcam_ovveride_upload_directory( $dir ) 
	{
		$options = new WCAM_Option();
		$folder_name = $options->get_option('folder_name', 'wcam');
		return array(
			'path'   => $dir['basedir'] . '/'.$folder_name.'/'.$this->current_order,//.get_current_user_id(),
			'url'    => $dir['baseurl'] . '/'.$folder_name.'/'.$this->current_order,//.get_current_user_id(),
			'subdir' => '/'.$folder_name.'/'.$this->current_order//.get_current_user_id(),
		) + $dir;
	}
	/* public function wcam_ovveride_upload_secure_directory( $dir ) {
		return array(
			'path'   => $dir['basedir'] . '/wcam/secure/'.$this->current_order,//.get_current_user_id(),
			'url'    => $dir['baseurl'] . '/wcam/secure/'.$this->current_order,//.get_current_user_id(),
			'subdir' => '/wcam/'.$this->current_order//.get_current_user_id(),
		) + $dir;
	} */
	public function generate_unique_file_name($dir, $name, $ext)
	{
		$general_options_model = new WCAM_Option();
		$disable_prefix = $general_options_model->get_option('disable_random_number_prefix', 'no');
		$complete_name = substr_count($name, $ext) == 1 ? $name : $name.$ext;
		return $disable_prefix == 'no' ? rand(0,10000)."_".$complete_name  : $complete_name ;
	}
	function upload_files($order_id, &$file_order_metadata, $attachments, $attachments_titles, $meta_info = null)
	{
		global $wcam_order_model;
		//base folder creation
		$options = new WCAM_Option();
		$folder_name = $options->get_option('folder_name', 'wcam');
		$upload_dir = wp_upload_dir();
		if (!file_exists($upload_dir['basedir']."/".$folder_name)) 
				mkdir($upload_dir['basedir']."/".$folder_name, 0775, true);
			
		foreach($attachments as $prefix_id => $file_data)
		 {
			list($prefix, $id) = explode("-", $prefix_id );
			 if($file_data["name"] != '')
			 {
				$this->current_order = $order_id;
				
				//upload
				$upload_overrides = array( 'test_form' => false, 'unique_filename_callback' => array( $this , 'generate_unique_file_name') );
				add_filter( 'upload_dir', array( &$this,'wcam_ovveride_upload_directory' ));				
				$movefile = wp_handle_upload( $file_data, $upload_overrides );
				remove_filter( 'upload_dir', array( &$this,'wcam_ovveride_upload_directory' ));
				
				if ( $movefile && !isset( $movefile['error'] ) ) 
				{
					//creation of fake indexes
					//$secure_sub = "";
					
					if( !file_exists ($upload_dir['basedir'].'/'.$folder_name.'/index.html'))
						touch ($upload_dir['basedir'].'/'.$folder_name.'/index.html');
					if( !file_exists ($upload_dir['basedir'].'/'.$folder_name.'/'.$order_id.'/index.html'))
						touch ($upload_dir['basedir'].'/'.$folder_name.'/'.$order_id.'/index.html');
					
					$meta_info_temp = isset($meta_info) ? $meta_info : $_POST;
					
					$wcam_order_model->save_single_attachment_meta_data($id, $meta_info_temp, $file_order_metadata, $attachments_titles, $order_id, $movefile['file'], $movefile['url']);
					
					//var_dump($file_order_metadata[$id]);
				}
				else
				{
					
					//var_dump($movefile['error']);
				}					
			 }
		 }
		//update_post_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		return $file_order_metadata;
	}
	public function delete_all_order_attachments($order_id)
	{
		$post = get_post($order_id);
		if ($post->post_type == 'shop_order')
		{
			$file_order_metadata = get_post_meta($order_id, '_wcam_attachments_meta');
			$file_order_metadata = !$file_order_metadata ? array():$file_order_metadata[0];
			
			foreach($file_order_metadata as $file_to_delete)
			{
				try{
					if(isset($file_to_delete['absolute_path']) && !isset($file_to_delete[$id]['media_from_gallery']))
						@unlink($file_to_delete['absolute_path']);
				}catch(Exception $e){};
			}
			delete_post_meta( $order_id, '_wcam_attachments_meta');
		}
	}
	function delete_file($id, $file_order_metadata, $order_id)
	{
		if(isset($file_order_metadata[$id]) )
		{
			try{
				if(isset($file_order_metadata[$id]['absolute_path']) && !isset($file_order_metadata[$id]['media_from_gallery']))
					@unlink($file_order_metadata[$id]['absolute_path']);
			}catch(Exception $e){};
			unset($file_order_metadata[$id]);
			//update_post_meta( $order_id, '_wcam_attachments_meta', $file_order_metadata);
		}
		return $file_order_metadata; 
	}
	
}
?>