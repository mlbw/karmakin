<?php 
class WCAM_BulkProductsAttachments
{
	var $product_attachments_cache = array();
	var $rule_cache;
	public function __construct()
	{
	}
	public function cl_acf_set_language() 
	{
	  return acf_get_setting('default_language');
	}
	private function retrieve_rules()
	{
		if(isset($this->rule_cache))
			return $this->rule_cache;
		$this->rule_cache = array();
		
		if(have_rows('wcam_products_attachments', 'option') )
			while ( have_rows('wcam_products_attachments', 'option') ) 
			{
				the_row();
				$new_rule = array();
				$new_rule['wcam_file'] = get_sub_field('wcam_file');
				$new_rule['wcam_selected_products'] = get_sub_field('wcam_selected_products', 'option');
				$new_rule['wcam_selected_product_categories'] = get_sub_field('wcam_selected_product_categories', 'option');
				$new_rule['wcam_selected_strategy'] = get_sub_field('wcam_selected_strategy', 'option');
				$new_rule['wcam_categories_children'] = get_sub_field('wcam_categories_children', 'option');
				$new_rule['wcam_order_status_visibility'] = get_sub_field('wcam_order_status_visibility');
				//
				$new_rule['wcam_rule_id'] = get_sub_field('wcam_rule_id', 'option');
				$new_rule['wcam_attach_file_to_woocoommerce_status_email'] = get_sub_field('wcam_attach_file_to_woocoommerce_status_email', 'option');
				$new_rule['wcam_file_is_only_attachend_to_the_email'] = get_sub_field('wcam_file_is_only_attachend_to_the_email');
				$new_rule['wcam_expiring_strategy'] = get_sub_field('wcam_expiring_strategy', 'option');
				$new_rule['wcam_specific_date'] = get_sub_field('wcam_specific_date', 'option');
				$new_rule['wcam_time_amount'] = get_sub_field('wcam_time_amount', 'option');
				$new_rule['wcam_time_amount_type'] = get_sub_field('wcam_time_amount_type', 'option');
				//
				$new_rule['wcam_file_name'] = get_sub_field('wcam_file_name', 'option');
				$new_rule['wcam_file_description'] = get_sub_field('wcam_file_description', 'option');
				$new_rule['wcam_attachment_visibility'] = get_sub_field('wcam_attachment_visibility');
				$new_rule['wcam_order_status_visibility'] = get_sub_field('wcam_order_status_visibility', 'option');
				
				$this->rule_cache[] = $new_rule;
			}
		
		return $this->rule_cache;
	}
	public function get_rules_by_product_id($product_id, $order = null, $file_id = null, $check_if_can_be_attached_to_email = false)
	{
		if(isset($this->product_attachments_cache[$product_id]))
		{
			//wcam_var_dump("cache2");
			return $this->product_attachments_cache[$product_id];
		}
		
		add_filter('acf/settings/current_language',  array(&$this, 'cl_acf_set_language'), 100);
		
		global $wcam_product_model;
		$all_data = array();
		if(isset($order))
		{
			$order_status = $order->get_status( );
			$status = "wc-".$order_status;
		}
		$options = new WCAM_Option();
		$parent_product = wp_get_post_parent_id($product_id);
		$rules = $this->retrieve_rules();
		//if(have_rows('wcam_products_attachments', 'option') )
			//while ( have_rows('wcam_products_attachments', 'option') ) 
			foreach((array)$rules as $rule)
			{
				//the_row();
				$file_data = $rule['wcam_file'];
				$allowed_product_ids = $rule['wcam_selected_products'];
				$allowed_categories_ids = $rule['wcam_selected_product_categories'];
				$selected_strategy = $rule['wcam_selected_strategy']; //all || except 
				$categories_children = $rule['wcam_categories_children']; //selected_only || all_children
				$additional_ids = array();

				//validation
				$allowed_product_ids = $allowed_product_ids ? $allowed_product_ids : array() ;
				$allowed_categories_ids = $allowed_categories_ids ? $allowed_categories_ids : false ;
				$selected_strategy = 	$selected_strategy ? 	$selected_strategy : 'all'; //all || except 
				$categories_children = $categories_children  ? $categories_children  : 'selected_only'; //selected_only || all_children
				
				if(!empty($allowed_product_ids) && $selected_strategy != 'all')
				{
					$allowed_product_ids = $wcam_product_model->get_complementry_ids($allowed_product_ids);
					//$allowed_product_ids = $wcam_product_model->get_complementry_ids($allowed_product_ids, "product_variation");
					
					if($parent_product != false)
					{
						$variations = $wcam_product_model->get_variations($parent_product);
						if(!empty($variations))
						{
							foreach($variations as $variation)
								if(($key = array_search($variation->ID, $allowed_product_ids)) !== false) 
									unset($allowed_product_ids[$key]);						
						}
						//with "except" rule if has been specified a varition id, its father will considered. To avoid this force partent inlcusion to false;
						$parent_product = false; 
					}
				}
					
				//get remaining posts ids from selected categories
				if($allowed_categories_ids)
					$additional_ids = $wcam_product_model->get_products_ids_using_categories('product_cat',$allowed_categories_ids, $categories_children, $selected_strategy);
				
				if(!empty($additional_ids))
					$allowed_product_ids = array_merge($allowed_product_ids, $additional_ids);
				
				if(is_array($allowed_product_ids) && (in_array($product_id, $allowed_product_ids) || ($parent_product != false && in_array($parent_product, $allowed_product_ids)) ))
					if($file_data  && (!isset($order) || isset($rule['wcam_order_status_visibility']) && in_array($status, $rule['wcam_order_status_visibility'])))
					{
						
						/* format:
						array(7) {
						  ["id"]=>
						  string(32) "a9707f10c9a7b33fad0659f4ca35f458"
						  ["file_name"]=>
						  string(4) "test"
						  ["file_description"]=>
						  string(0) ""
						  ["file_url"]=>
						  string(99) "http://xxx.com/woocommerce/wp-content/uploads/2015/07/xxxx.jpg"
						  ["file_path"]=>
						  string(132) "/var/directory/file.jpg"
						  ["order_status_visibility"]=>
						  array(2) {
							[0]=>
							string(10) "wc-on-hold"
							[1]=>
							string(0) ""
						  }
						  ["attach_file_to_woocoommerce_status_email"]=>
						  string(3) "yes"
						}
						*/
						
						//$time_offset = $options->get_option('time_offset', 0);					
						$id = $rule['wcam_rule_id'];
						$can_be_attached = $rule['wcam_attach_file_to_woocoommerce_status_email'];
						$visible_only_in_emails = $rule['wcam_file_is_only_attachend_to_the_email'];
						$expiring_strategy = $rule['wcam_expiring_strategy'];
						$specific_date = $rule['wcam_specific_date'];
						$time_amount = $rule['wcam_time_amount']; 
						$time_amount_type = $rule['wcam_time_amount_type'];
						//validation	
						$expiring_strategy = $expiring_strategy ? $expiring_strategy : 'none';
						$specific_date = $specific_date ? $specific_date  : 'none';
						$time_amount = $time_amount ? $time_amount : 1; 
						$time_amount_type = $time_amount_type ? $time_amount_type : 'none';

						$has_expired = $expiring_strategy == 'none' ? false : true;
						//$now = date_create_from_format ("d/m/Y H:i:s", date("d/m/Y H:i:s", strtotime($time_offset.' minute')));
						$expiring_date = "";
						
						/*backward compatibility*/
						$visible_only_in_emails = $visible_only_in_emails ? $visible_only_in_emails : 'no';
					
						if(!isset($order))
						{
							$has_expired  = false;
						}
						else
						{
							$result = wcam_is_expiring_date_valid($expiring_strategy, WCAM_Order::get_date_created($order), "d/m/Y H:i:s", $specific_date, $time_amount ,$time_amount_type);	
							$has_expired = $result['has_expired'];
							$expiring_date = $result['expiring_date'];
						}
						
						if((!isset($file_id) || $file_id == @md5($file_data['url']/* @file_get_contents($file_data['url']) */)) &&
							(!$check_if_can_be_attached_to_email || $can_be_attached == 'yes') //&&
							//(!$has_expired)
							) //$file_data['filename']
						{
							$attachment = array();
							$attachment['id'] = @md5($file_data['url']); //Check if value exists: if( $value ) 
							//$attachment['id'] = md5(file_get_contents($file_data['url'])); //Check if value exists: if( $value ) 
							//$attachment['id'] = $id ? $id : @md5_file($file_data['path']); 
							$attachment['rule_id'] = $id; 
							$attachment['file_name'] = $rule['wcam_file_name']; 
							$attachment['file_description'] = $rule['wcam_file_description']; 
							$attachment['file_url'] = $has_expired ? "" :$file_data['url']; 
							$attachment['file_path'] = $has_expired ? "" : get_attached_file( $file_data['id'] );
							$attachment['attachment_visibility'] = $rule['wcam_attachment_visibility']; // order_details_page || product_page || all_pages
							$attachment['attachment_visibility'] = $attachment['attachment_visibility'] ? $attachment['attachment_visibility'] : 'order_details_page'; 
							$attachment['order_status_visibility'] = $rule['wcam_order_status_visibility']; 
							$attachment['attach_file_to_woocoommerce_status_email'] = /* $has_expired ? 'no' : */ $can_be_attached; 
							$attachment['visible_only_in_emails'] = $visible_only_in_emails; 
							$attachment['expiring_strategy'] = $expiring_strategy; //none, specific_date, relative_date
							$attachment['specific_date'] = $specific_date; //d/m/Y
							$attachment['time_amount'] = $time_amount; 
							$attachment['time_amount_type'] = $time_amount_type; //secods, minutes, hours, days, month
							$attachment['expiring_date'] = $expiring_date; 
							$attachment['has_expired'] = $has_expired; 
						
							$all_data[$id] = $attachment;
						}
						
					}
			}
	
		remove_filter('acf/settings/current_language', array(&$this,'cl_acf_set_language'), 100);	
		$this->product_attachments_cache[$product_id] = $all_data;
		return $all_data;
	}
}
?>