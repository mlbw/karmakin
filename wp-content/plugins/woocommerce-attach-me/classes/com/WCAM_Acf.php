<?php 
if ( ! function_exists( 'is_plugin_active' ) ) 
{
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
}
$wcam_hide_menu = true;
$wcam_active_plugins = get_option('active_plugins');
$wcam_acf_pro = 'advanced-custom-fields-pro/acf.php';
$wcam_acf_pro_is_aleady_active = in_array($wcam_acf_pro, $wcam_active_plugins) || class_exists('acf') ? true : false;
if(!$wcam_acf_pro_is_aleady_active)
	include_once( WCAM_PLUGIN_ABS_PATH . '/classes/acf/acf.php' );

add_action('admin_init', 'wcam_acf_settings_init');
function wcam_acf_settings_init()
{
	/* if(version_compare( WC_VERSION, '2.7', '>=' ))
		acf_update_setting('select2_version', 4); */
}

/* Checks to see if the acf pro plugin is activated  */
if ( is_plugin_active('advanced-custom-fields-pro/acf.php') )  {
	$wcam_hide_menu = false;
}

/* Checks to see if the acf plugin is activated  */
if ( is_plugin_active('advanced-custom-fields/acf.php') ) 
{
	add_action('plugins_loaded', 'wcam_load_acf_standard_last', 10, 2 ); //activated_plugin
	add_action('deactivated_plugin', 'wcam_detect_plugin_deactivation', 10, 2 ); //activated_plugin
	$wcam_hide_menu = false;
	//deactivate_plugins( plugin_basename( WP_PLUGIN_DIR.'/advanced-custom-fields/acf.php') );
	//activate_plugin(plugin_basename( WP_PLUGIN_DIR.'/advanced-custom-fields/acf.php'));
}

function wcam_detect_plugin_deactivation(  $plugin, $network_activation ) { //after
   // $plugin == 'advanced-custom-fields/acf.php'
	//wcam_var_dump("wcam_detect_plugin_deactivation");
	$acf_standard = 'advanced-custom-fields/acf.php';
	if($plugin == $acf_standard)
	{
		$active_plugins = get_option('active_plugins');
		$this_plugin_key = array_keys($active_plugins, $acf_standard);
		if (!empty($this_plugin_key)) 
		{
			//array_splice($active_plugins, $this_plugin_key, 1);
			foreach($this_plugin_key as $index)
				unset($active_plugins[$index]);
			update_option('active_plugins', $active_plugins);
			//forcing
			deactivate_plugins( plugin_basename( WP_PLUGIN_DIR.'/advanced-custom-fields/acf.php') );
		}
	}
} 
function wcam_load_acf_standard_last($plugin, $network_activation = null) { //before
	$acf_standard = 'advanced-custom-fields/acf.php';
	$active_plugins = get_option('active_plugins');
	//$this_plugin_key = array_search($acf_standard, $active_plugins);
	$this_plugin_key = array_keys($active_plugins, $acf_standard);
	//if ($this_plugin_key) { // if it's 0 it's the first plugin already, no need to continue
	if (!empty($this_plugin_key)) 
	{ 
		foreach($this_plugin_key as $index)
			//array_splice($active_plugins, $index, 1);
			unset($active_plugins[$index]);
		//array_unshift($active_plugins, $acf_standard); //first
		array_push($active_plugins, $acf_standard); //last
		update_option('active_plugins', $active_plugins);
	} 
}

if(!$wcam_acf_pro_is_aleady_active)
	add_filter('acf/settings/path', 'wcam_acf_settings_path');
function wcam_acf_settings_path( $path ) 
{
 
    // update path
    $path = WCAM_PLUGIN_ABS_PATH. '/classes/acf/';
    
    // return
    return $path;
    
}
if(!$wcam_acf_pro_is_aleady_active)
	add_filter('acf/settings/dir', 'wcam_acf_settings_dir');
function wcam_acf_settings_dir( $dir ) {
 
    // update path
    $dir = wcam_PLUGIN_PATH . '/classes/acf/';
    
    // return
    return $dir;
    
}

function wcam_acf_init() {
    
    include WCAM_PLUGIN_ABS_PATH . "/assets/fields.php";
    
}
add_action('acf/init', 'wcam_acf_init'); 

//hide acf menu
if($wcam_hide_menu)
	add_filter('acf/settings/show_admin', '__return_false');

//********************************************
//custom role field
/**
 * ACF 5 Field
 *
 * Loads the field for ACF 5
 *
 */
function wcam_include_field_types_order_status_selector( $version ) {
	if(!class_exists('acf_order_status_selector'))
		include_once(WCAM_PLUGIN_ABS_PATH.'/classes/com/vendor/acf-order-status-field/acf-order-status-v5.php');
}
add_action('acf/include_field_types', 'wcam_include_field_types_order_status_selector');

function wcam_include_field_types_unique_id( $version ) {
	if(!class_exists('acf_field_unique_id'))
		include_once(WCAM_PLUGIN_ABS_PATH.'/classes/com/vendor/acf-field-unique-id/acf-unique_id-v5.php');
}
add_action('acf/include_field_types', 'wcam_include_field_types_unique_id');

function wcam_include_field_types_variant_selector( $version ) {
 if(!class_exists('acf_field_product_variant_selector'))
	include_once(WCAM_PLUGIN_ABS_PATH.'/classes/com/vendor/acf-product-variant-field/acf-product_variant_selector.php');
}
add_action('acf/include_field_types', 'wcam_include_field_types_variant_selector');

?>