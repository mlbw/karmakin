<?php 
class WCAM_Wpml
{
	var $curr_lang;
	public function __construct()
	{
	}
	public function get_default_language()
	{
		global $sitepress;
		return $sitepress->get_default_language();
	}
	public function get_current_language()
	{
		if($this->is_wpml_active())
		{
			global $sitepress;
			if(ICL_LANGUAGE_CODE  == $sitepress->get_default_language())
				return 'default';
			
			return ICL_LANGUAGE_CODE;
		}
		return 'default';
	}
	public function is_wpml_active()
	{
		if(class_exists('SitePress'))
			return true;
		
		return false;
	}
	public function switch_language($language_code)
	{
		if(class_exists('SitePress'))
		{
			global $sitepress;
			$sitepress->switch_lang($language_code, true);
			return true;
		}
		return false;
	}
	public function switch_to_default_language()
	{
		if(!$this->is_wpml_active())
			return;
		global $sitepress;
		$this->curr_lang = ICL_LANGUAGE_CODE ;
		$sitepress->switch_lang($sitepress->get_default_language());
	
	}
	public function switch_to_current_language()
	{
		if(!$this->is_wpml_active())
			return;
		
		global $sitepress;
		$sitepress->switch_lang($this->curr_lang);
	}
	public function get_original_id($item_id, $post_type = "product", $return_original = true)
	{
		if(!class_exists('SitePress'))
			return false;
		
		global $sitepress;
		if(function_exists('icl_object_id'))
			$item_translated_id = icl_object_id($item_id, $post_type, $return_original, $sitepress->get_default_language());
		else
			$item_translated_id = apply_filters( 'wpml_object_id', $item_id, $post_type, $return_original, $sitepress->get_default_language() );
		
		return $item_translated_id;
	}
}
?>