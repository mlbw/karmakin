<?php 
class WCAM_OrderAddon
{
	var $current_order = 0;
	/* var $email_sender; */
	var $errors;
	var $messages;
	var $results;
	public function __construct()
	{
		add_action( 'add_meta_boxes', array( &$this, 'woocommerce_attachment_metaboxes' ) );
		//add_action( 'woocommerce_process_shop_order_meta', array( &$this, 'woocommerce_process_shop_ordermeta' ), 5, 2 );
		add_action( 'wp_ajax_upload_attachments', array( &$this, 'woocommerce_process_shop_ordermeta' ) );
		add_action( 'wp_ajax_wcam_upload_attachments_csv', array( &$this, 'import_attachments_csv_chunk' ) );
	}
	
	function import_attachments_csv_chunk()
	{
		global $wcam_order_model;
		$csv_array = explode("<#>", $_POST['csv']);
		$results = $wcam_order_model->process_upload_csv_chunk($csv_array);
		$this->messages = $results['messages'];
		$this->errors = $results['errors'];
		
		 if ( isset($this->errors) && count($this->errors) > 0):  ?>
			<div class="error">
					<ul>
				<?php foreach($this->errors as $error)
				
			 if ( is_wp_error($error) ) : ?>
				
					<?php
						foreach ( $error->get_error_messages() as $err )
							echo "<li>$err</li>\n";
					endif; ?>
					</ul>
				</div>
			<?php endif;

			if ( ! empty( $this->messages ) ) {
				foreach ( $this->messages as $msg )
					echo '<div id="message" class="updated"><p>' . $msg . '</p></div>';
			} 
		wp_die();
	}
	//Save uploads and options
	public function woocommerce_process_shop_ordermeta( ) 
	{
		if(!isset($_POST["wcam-orderid"]))
			return;
		
		global $wcam_order_model;
		$results = $wcam_order_model->save_attachments($_POST, $_FILES);
		$this->errors = $results['errors'];
		$this->results = $results['results'];
		
		$this->woocommerce_order_attachments_box(get_post( $_POST["wcam-orderid"] )); 
		wp_die();
	}
	public function woocommerce_attachment_metaboxes()
	{
		add_meta_box( 'woocommerce-attach-me', __('Attachments box', 'woocommerce-attach-me'), array( &$this, 'woocommerce_order_attachments_box' ), 'shop_order', 'normal', 'default');
	}
	public function woocommerce_order_attachments_box($order)
	{
		global $wcam_order_model;
		$attachments_meta = $wcam_order_model->get_attachmets_meta($order->ID);
		$wcam_options = $wcam_order_model->get_post_options($order->ID);
		
		$general_options_model = new WCAM_Option();
		$general_options = $general_options_model->get_option();
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$order_statuses = wc_get_order_statuses();
		//var_dump($wcam_options);
		wp_enqueue_script('jquery-ui-datepicker');
		wp_enqueue_script( 'wcam-backend-orderaddon',wcam_PLUGIN_PATH.'/js/wcam-backend-order-addon.js', array( 'jquery' ) );
		wp_enqueue_script( 'wcam-backend-browsegallery', wcam_PLUGIN_PATH.'/js/wcam-backend-order-addon-browse-gallery.js', array( 'jquery' ) );
		wp_enqueue_script( 'wcam-backend-select', wcam_PLUGIN_PATH.'/js/select2.min.js', array( 'jquery' ) );
		wp_enqueue_style( 'wcam-backend-orderaddon', wcam_PLUGIN_PATH.'/css/wcam-backend.css' );
		wp_enqueue_style( 'wcam-backend-orderaddon-select', wcam_PLUGIN_PATH.'/css/select2.css' );
		wp_enqueue_style( 'wcam-backend-tooltip', wcam_PLUGIN_PATH.'/css/wcam-tooltip.css' );
		$js_src = includes_url('js/tinymce/') . 'tinymce.min.js';
		$css_src = includes_url('css/') . 'editor.css';
		echo '<script src="' . $js_src . '" type="text/javascript"></script>';
		wp_register_style('tinymce_css', $css_src);
		wp_enqueue_style('tinymce_css');
		wp_enqueue_media();
		
		//init 		
		$general_options['default_attachment_title'][$curr_lang] = !isset($general_options['default_attachment_title'][$curr_lang]) ? __('Attachment', 'woocommerce-attach-me'): $general_options['default_attachment_title'][$curr_lang];
		?>
		<div id="wcam-attachments-box">	
		<?php if(isset($this->errors))
			echo '<h4 style="color:red">'.$this->errors.'</h4>'; ?>
		<?php if(isset($this->results))
			echo '<h4 style="color:green">'.$this->results.'</h4>'; ?>
		 <div id="wcam-uploads-data">
		 
			<!-- <button class="button button-primary wcam_primary_button" id="wcam-save-button"><?php _e('Save', 'woocommerce-attach-me'); ?></button>
			 <div class="wcam_spacer"></div> -->
			 
			<ul id="wcam-attachments-list">
			<input type="hidden" name="wcam-orderid" id="wcam-orderid" value="<?php echo $order->ID; ?>"></input>
			<?php $counter =  $max_id = 0;
			if(!$attachments_meta || empty($attachments_meta)): 
						//echo '<p><strong>'.__('No attachments.', 'woocommerce-attach-me').'</strong></p>'; 
						?>
						<li class="wcam-attachment-box" data-id="0" data-already-uploaded="no">
							<input type="hidden" name="wcam_id[0]" value="0" ></input>
							<h2 ><div class="button wcam_collapse_expand_button" data-id="0">-</div><?php _e('Attachment #', 'woocommerce-attach-me'); ?>0</h2>
							
							<div class="wcam-attachment-box-content" id="wcam-attachment-box-content-0" data-already-collapsed="no">
								<h4 ><?php _e('Shortcode', 'woocommerce-attach-me'); ?></h4>
								<a href="#" class="wcam_tooltip">
									[wcam_download_link id="0"]
									<span>
										<?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?> <b>[wcam_download_link id="0"]</b>  <?php _e('in the notification email body (see down below) to embed a direct link to download/view the file.', 'woocommerce-attach-me'); ?>
									</span>
								</a>
								
								
								<!--<strong><?php _e('Note:', 'woocommerce-attach-me'); ?></strong> <?php _e('External file cannot be attached to the notification email.', 'woocommerce-attach-me'); ?>
								<h5><?php _e('Attachment', 'woocommerce-attach-me'); ?></h5>
								<p><?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?><strong>[wcam_attachment id="0"]</strong> <?php _e('in the notification email body (see down below) to attach the file to the email.', 'woocommerce-attach-me'); ?></p>
								-->
								<h4 ><?php _e('Attachment title', 'woocommerce-attach-me'); ?></h4>
								<input style="display:block;" type="text"  class="wcam-attachments-titles" id="wcam_attachment_title_0" name="wcam_attachment_title[0]" placeholder="<?php _e('Title/description', 'woocommerce-attach-me'); ?>" value="<?php if(isset($general_options['default_attachment_title'][$curr_lang])) echo $general_options['default_attachment_title'][$curr_lang];?>"></input>
								<div id="wcam-file-data-box">
									<fieldset>
										<input type="radio" id="wcam-radio-file-0" name="wcam-attachment-type0" value="file" checked="checked">	</input>
										<label><?php _e('Upload a file', 'woocommerce-attach-me'); ?></label>
										<input style="display:block; margin-bottom:15px;" type="file" class="file_input" name="wcam_attachment_file-0" ></input>
									
										<input type="radio" id="wcam-radio-ext-0" name="wcam-attachment-type0" value="ext"></input>
										<label><?php _e('Or attach an external URL:', 'woocommerce-attach-me'); ?></label>
										<input style="display:block; margin-bottom:15px;" class="wcam-external-url" id="wcam-external-link-0" type="text" name="wcam_attachment_external_url[0]" placeholder="<?php _e('Ex: http://www.gdocs.com/file.jpg', 'woocommerce-attach-me'); ?>"></input>
										
										<input type="radio" id="wcam-radio-gallery-0" name="wcam-attachment-type0"  value="file"></input>
										<label><?php _e('Or attach from gallery:', 'woocommerce-attach-me'); ?></label>
										<input id="wcam-gallery-link-0"  class="wcam-gallery-input"  type="text" class="wcam-gallery-attachment" name="wcam_attachment_gallery_url[0]" placeholder="<?php _e('click on browse button', 'woocommerce-attach-me'); ?>" readonly></input>
										<input type="hidden" id="gallery_media_id_0" name="wcam_attachment_gallery_media_id[0]"></input>
										<input type="button" data-id="0" class="wcam-browse-link button" value="<?php _e('Browse from Media gallery', 'woocommerce-attach-me') ?>" ></input>								
									</fieldset>
								</div>
								
								<button class="button wccm_advanced_option_button" data-id="0" ><?php _e('Show/Hide advanced options', 'woocommerce-attach-me'); ?></button>
								<div class="wccm_advanced_option_box" id="wcam-advanced-options-box-0">
								
									<input type="checkbox" name="wcam-secure-download[0]" id="wcam-secure-download-0" class="wcam-customer-approve-checkbox" value="yes" <?php if(isset($general_options['secure_download']) && $general_options['secure_download'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Secure download (Only the owner of the order can view/download the file). This option will not work with external  links.', 'woocommerce-attach-me'); ?></label>
									<br/>									
									<input type="checkbox" name="wcam-customer-approve-checkbox[0]" class="wcam-customer-approve-checkbox" value="yes" <?php if(isset($general_options['approve_checkbox_default_value']) && $general_options['approve_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Let customer approves? ', 'woocommerce-attach-me'); ?></label>
									<br/>
									<input type="checkbox"  name="wcam-customer-reapprove-checkbox[0]" class="wcam-customer-reapprove-checkbox" value="yes"<?php if(isset($general_options['reapprove_checkbox_default_value']) && $general_options['reapprove_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Can customer change approval value after submit?', 'woocommerce-attach-me'); ?></label>
									<br/>
									<input type="checkbox" name="wcam-customer-feedback-checkbox[0]"  class="wcam-customer-feedback-checkbox" value="yes" <?php if(isset($general_options['feedback_checkbox_default_value']) && $general_options['feedback_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Let customer leave a text feedback?', 'woocommerce-attach-me'); ?></label>
									<br/>
									<!-- <input type="checkbox" value="yes" name="wcam-customer-feedback-required[0]"  value="yes" <?php if(isset($general_options['feedback_checkbox_default_value']) && $general_options['feedback_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>><?php _e('Is feedback required?', 'woocommerce-attach-me'); ?></input>-->
										<?php _e('... and feedback is required when (it works only if the previous feedback option has been enabled)', 'woocommerce-attach-me'); ?>
										<br/><select type="checkbox" name="wcam-customer-feedback-strategy[0]"  value="yes">
											<option value="always" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'always') echo 'selected="selected"';?>><?php _e('Always', 'woocommerce-attach-me'); ?></option>
											<option value="only_yes" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'only_yes') echo 'selected="selected"';?>><?php _e('If customer approves', 'woocommerce-attach-me'); ?></option>
											<option value="only_no" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'only_no') echo 'selected="selected"';?>><?php _e('If customer does not approve', 'woocommerce-attach-me'); ?></option>
											<option value="never" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'never') echo 'selected="selected"';?>><?php _e('Never', 'woocommerce-attach-me'); ?></option>
										</select>
									<br/>
									<input type="checkbox" name="wcam-customer-admin-notification-checkbox[0]" class="wcam-customer-admin-notification-checkbox" value="yes" <?php if(isset($general_options['customer_admin_notification_checkbox_default_value']) && $general_options['customer_admin_notification_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Receive an email when the customer approve or leaves a feedback? ', 'woocommerce-attach-me'); ?></label>
									<br/>
									<input type="checkbox" <?php if(isset($general_options['display_copy_to_clipboard_button']) && $general_options['display_copy_to_clipboard_button'] == 'yes') echo 'checked="checked"';?> name="wcam-display-copy-to-clipboard-button[0]"  value="yes"></input>
									<label><?php _e('Display also the "Copy attached file url to clipboard" button next the "Download/View" button on Order details page?', 'woocommerce-attach-me'); ?></label>
									<br/>
										
									<h4><?php _e('Select for which order status the attachment has to be hidden', 'woocommerce-attach-me'); ?></h4>
									<?php 
									 
									 foreach((array)$order_statuses as $code => $status): 
										
										$hide_on_current_status= isset($general_options['order_attachment_hide_by_status']) && in_array($code, $general_options['order_attachment_hide_by_status']) ? true : false;
									 ?>
										<input type="checkbox" <?php if($hide_on_current_status) echo 'checked="checked"';?>
										   name="wcam-order-attachment-hide-by-status[0][]"  value="<?php echo $code; ?>"><?php _e('Hide on',''); ?> <?php echo $status ?></input>
										<br/>
									<?php endforeach; ?>
									
									<h4 class="wcam_email_attachments_title"><?php _e('Email attachments (attachments will not work with exteran file/url)', 'woocommerce-attach-me'); ?></h4>
									<input type="checkbox" class="wcam-attach-file-to-complete-order-email" name="wcam-attach-file-to-processing-order-email[0]"  value="yes" <?php if(isset($general_options['attach_file_to_processing_order_email_default_value']) && $general_options['attach_file_to_processing_order_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Attach file to the Processing order email', 'woocommerce-attach-me'); ?></label>
									<br/>
									<input type="checkbox" class="wcam-attach-file-to-complete-order-email" name="wcam-attach-file-to-complete-order-email[0]"  value="yes" <?php if(isset($general_options['attach_file_to_complete_order_email_default_value']) && $general_options['attach_file_to_complete_order_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Attach file to the Complete order email', 'woocommerce-attach-me'); ?></label>
									<br/>
									<input type="checkbox" class="wcam-attach-file-to-complete-order-email" name="wcam-attach-file-to-customer-invoice-email[0]"  value="yes" <?php if(isset($general_options['attach_file_to_customer_invoice_email_default_value']) && $general_options['attach_file_to_customer_invoice_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>
									<label ><?php _e('Attach file to the Customer Invoice email', 'woocommerce-attach-me'); ?></label>
									<br/>
									
									<h4 class="wcam_expiration_strategy_title"><?php _e('Expires?', 'woocommerce-attach-me'); ?></h4>
									<p><?php _e('If enabled will automatically enable the "Secure download" options. It is required to make this feature to properly work.', 'woocommerce-attach-me'); ?></p>
									<select class="wcam_expiration_strategy" name="wcam_expiration_strategy[0]" id="wcam_expiration_strategy_0" data-id="0">
										<option value="never"><?php _e('Never', 'woocommerce-attach-me'); ?></option>
										<option value="specific_date"><?php _e('At specific date', 'woocommerce-attach-me'); ?></option>
										<option value="relative_date"><?php _e('After the selected time amount after order date', 'woocommerce-attach-me'); ?></option>
									</select>
									
									<div id="wcam_expiration_relative_time_amout_selection_box_0" class="wcam_expiration_relative_time_amout_selection_box">
										<label><?php _e('Time amount: ', 'woocommerce-attach-me'); ?></label>
										<input type="number" min="1" step="1" name="wcam_expiration_relative_time_amount[0]" value="1"></input>
										
										<label class="wcam_expiration_relative_time_time_type_label"><?php _e('Time type: ', 'woocommerce-attach-me'); ?></label>
										<select class="wcam_expiration_strategy" name="wcam_expiration_relative_time_type[0]" data-id="0">
											<option value="second"><?php _e('Seconds', 'woocommerce-attach-me'); ?></option>
											<option value="minute"><?php _e('Minutes', 'woocommerce-attach-me'); ?></option>
											<option value="hour"><?php _e('Hours', 'woocommerce-attach-me'); ?></option>
											<option value="day"><?php _e('Days', 'woocommerce-attach-me'); ?></option>
											<option value="month"><?php _e('Months', 'woocommerce-attach-me'); ?></option>
										</select>
									</div>
									
									<div id="wcam_expiration_specific_date_selection_box_0" class="wcam_expiration_specific_date_selection_box">
										<label><?php _e('Date and time: ', 'woocommerce-attach-me'); ?></label>
										<input type="text" class="wcam_expiration_specific_date" name="wcam_expiration_specific_date[0]" ></input>
									</div>
									
								</div>
								<button style="display:block; margin-top:15px;" class="button delete_button" data-fileid="0" onclick="wcam_remove_field(event);" ><?php _e('Delete', 'woocommerce-attach-me'); ?></button>
							</div>
						</li>
			<?php else:
			?>
			<div id="wcam-edit-box">
			 <?php foreach((array)$attachments_meta as $attachment_meta): 			
			 $counter = $attachment_meta['id']; 
			 $max_id = $max_id < $counter ? $counter : $max_id;
			 $is_external_link = (!isset($attachment_meta['absolute_path']) && !isset($attachment_meta['media_id']))? "yes":"no";
			 $is_expired = $wcam_order_model->is_attachment_expired($attachment_meta, $order->post_date);
			 $is_expired_title = $is_expired ? __('Yes', 'woocommerce-attach-me') : __('No', 'woocommerce-attach-me');
			?>
				<li class="wcam-attachment-box" data-id="<?php echo $counter; ?>" data-already-uploaded="yes" data-is-external="<?php echo $is_external_link;?>">
					<h2><div class="button wcam_collapse_expand_button" data-id="<?php echo $counter; ?>">+</div><?php _e('Attachment #', 'woocommerce-attach-me'); ?><?php echo $counter; ?> <?php if(isset($attachment_meta['title'])) echo " - ".$attachment_meta['title']. " - ".__('Is expired: ', 'woocommerce-attach-me').$is_expired_title; ?></h2>
					<div style="display:none;" class="wcam-attachment-box-content" id="wcam-attachment-box-content-<?php echo $counter; ?>" data-already-collapsed="yes">
						<h4 id="wcam_attachment_title_<?php echo $counter; ?>"><?php _e('Title: ', 'woocommerce-attach-me');?><span class="real-title"><?php if(!empty($attachment_meta['title'])) echo $attachment_meta['title']; else _e('No title', 'woocommerce-attach-me');?></span></h4>
						<?php 
							
							//if(isset($attachment_meta['secure-download']))
							{
								$secure_download = isset($attachment_meta['secure-download']) && $attachment_meta['secure-download'] == "yes" ? __('Enabled', 'woocommerce-attach-me'): __('Disabled', 'woocommerce-attach-me');
								echo '<strong>'.__('Secure download', 'woocommerce-attach-me').': </strong>'.$secure_download.'<br/><br/>';
							}
							if(isset($attachment_meta['expiration-strategy']))
							{
								 $is_expired_string = $is_expired ? '<span style="color:red">'.__('Yes', 'woocommerce-attach-me').'</span>' : '<span style="color:green">'.__('No', 'woocommerce-attach-me').'</span>';
			
								switch($attachment_meta['expiration-strategy'])
								{
									case 'never': _e('Expires: Never', 'woocommerce-attach-me'); break;
									case 'specific_date': echo "<strong>".__('Is expired: ', 'woocommerce-attach-me')."</strong>".$is_expired_string." - ".__('Expiration date: ', 'woocommerce-attach-me').$wcam_order_model->get_attachment_expiration_date($order->ID, $attachment_meta); break;
									case 'relative_date': echo "<strong>".__('Is expired: ', 'woocommerce-attach-me')."</strong>".$is_expired_string." - ".__('Expiration date: ', 'woocommerce-attach-me').$wcam_order_model->get_attachment_expiration_date($order->ID, $attachment_meta); break;
								} 
								echo '<br/>';
							}
							if(isset($attachment_meta['customer-has-to-be-approved']) && $attachment_meta['customer-has-to-be-approved'] == 'yes')
							{
								if(!isset($attachment_meta['customer-has-approved']) || $attachment_meta['customer-has-approved'] == 'none')
									echo '<h5 style="color:grey">'.__('Waiting for customer approval', 'woocommerce-attach-me').'</h5>';
								else if($attachment_meta['customer-has-approved'] == 'yes')
									echo '<h5 style="color:green">'.__('Customer has approved', 'woocommerce-attach-me').'</h5>';
								else
									echo '<h5 style="color:red">'.__('Customer has not approved', 'woocommerce-attach-me').'</h5>';
							}
							if(isset($attachment_meta['customer-can-reapprove']))
							{
								$value = $attachment_meta['customer-can-reapprove'] == 'yes' ? __('Yes','woocommerce-attach-me') : __('No','woocommerce-attach-me');
								echo '<strong>'.__('Can customer change approval value after submit: ','woocommerce-attach-me').'</strong>'.$value;
								echo '<br/>';
							}
							if(isset($attachment_meta['customer-feedback-enabled']) && $attachment_meta['customer-feedback-enabled']=='yes')
							{
								$feedback = isset($attachment_meta['customer-feedback']) ? $attachment_meta['customer-feedback']:" ";
								echo '<strong>'.__('Feedback', 'woocommerce-attach-me').':</strong>';
								echo '<p>'.$feedback.'</p>';
								/* $feedback_required = isset($attachment_meta['customer-feedback-required']) && $attachment_meta['customer-feedback-required'] == 'yes' ? __('Yes','woocommerce-attach-me'):__('No','woocommerce-attach-me');
								echo '<strong>'.__('Is feedback required', 'woocommerce-attach-me').': </strong>'.$feedback_required;
								echo '<br/>'; */
								$feedback_strategy = isset($attachment_meta['customer-feedback-strategy']) ? $attachment_meta['customer-feedback-strategy']:"none";
								switch($feedback_strategy)
								{
									case 'none': $feedback_strategy = __('Never','woocommerce-attach-me'); break;
									case 'always': $feedback_strategy = __('Always','woocommerce-attach-me'); break;
									case 'only_yes': $feedback_strategy = __('Only If customer approves','woocommerce-attach-me'); break;
									case 'only_no': $feedback_strategy = __('Only If customer does not approve','woocommerce-attach-me'); break;
									case 'never': $feedback_strategy = __('Never','woocommerce-attach-me'); break;
								}
								echo '<strong>'.__('Feedback strategy', 'woocommerce-attach-me').':</strong> '.$feedback_strategy;
							}
							echo "<p><br/>";
							echo "<p>";
							$order_statuses = wc_get_order_statuses();
							
							foreach((array)$order_statuses as $code => $status)
							{
								$hide_on_current_status = isset($attachment_meta['order-attachment-hide-by-status']) && in_array($code, $attachment_meta['order-attachment-hide-by-status']) ? true : false;
								if($hide_on_current_status)
									echo '<strong>'.__('Hide attachment for order status: ', 'woocommerce-attach-me').':</strong> '.$status.'<br/>';
							}
							echo "</p><br/>";		
							if(isset($attachment_meta['customer-admin-notification']))
							{
								$admin_email_notofication = $attachment_meta['customer-admin-notification'] == "yes" ? __('Enabled', 'woocommerce-attach-me'): __('Disabled', 'woocommerce-attach-me');
								echo '<strong>'.__('Admin notification', 'woocommerce-attach-me').':</strong> '.$admin_email_notofication.'<br/>';
							}
							if(isset($attachment_meta['attach-file-to-processing-order-email']))
							{
								$attach_file_to_processing_order_email = $attachment_meta['attach-file-to-processing-order-email'] == "yes" ? __('Enabled', 'woocommerce-attach-me'): __('Disabled', 'woocommerce-attach-me');
								echo '<strong>'.__('Attach file to processing order email', 'woocommerce-attach-me').':</strong> '.$attach_file_to_processing_order_email.'<br/>';
							}
							if(isset($attachment_meta['attach-file-to-complete-order-email']))
							{
								$attach_file_to_complete_order_email = $attachment_meta['attach-file-to-complete-order-email'] == "yes" ? __('Enabled', 'woocommerce-attach-me'): __('Disabled', 'woocommerce-attach-me');
								echo '<strong>'.__('Attach file to complete order email', 'woocommerce-attach-me').':</strong> '.$attach_file_to_complete_order_email.'<br/>';
							}
							if(isset($attachment_meta['attach-file-to-customer-invoice-email']))
							{
								$attach_file_to_customer_invoice_email = $attachment_meta['attach-file-to-customer-invoice-email'] == "yes" ? __('Enabled', 'woocommerce-attach-me'): __('Disabled', 'woocommerce-attach-me');
								echo '<strong>'.__('Attach file to customer invoice email', 'woocommerce-attach-me').':</strong> '.$attach_file_to_customer_invoice_email.'<br/>';
							}	
							echo "</p>";						
						  ?>
					
						<h4 ><?php _e('Shortcode', 'woocommerce-attach-me'); ?></h4>
						<p>
						<a href="#" class="wcam_tooltip">
									[wcam_download_link id="<?php echo $counter; ?>"]
									<span>
										<?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?> <b>[wcam_download_link id="<?php echo $counter; ?>"]</b>  <?php _e('in the notification email body (see down below) to embed a direct link to download/view the file.', 'woocommerce-attach-me'); ?>
									</span>
								</a>
						</p>
						<!--<p><strong><?php _e('Note:', 'woocommerce-attach-me'); ?></strong> <?php _e('External file cannot be attached to the notification email.', 'woocommerce-attach-me'); ?>
						<h5><?php _e('Attachment', 'woocommerce-attach-me'); ?></h5>
						<p><?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?><strong>[wcam_attachment id="<?php echo $counter; ?>"]</strong> <?php _e('in the notification email body (see down below) to attach the file to the email.', 'woocommerce-attach-me'); ?>
						-->
						<a target="_blank" class="button button-primary wcam_primary_button" style="text-decoration:none; color:white;" href="<?php echo $attachment_meta['url']; ?>"><?php _e('Download / View', 'woocommerce-attach-me'); ?></a>
						<input type="submit" class="button delete_button" data-fileid="<?php echo $attachment_meta['id'] ?>" value="<?php _e('Delete', 'woocommerce-attach-me'); ?>" onclick="wcam_remove_field(event);" ></input>
					</div>
				</li>
			  <?php endforeach;?>
		     </div>
			<?php endif; ?>
			</ul>
			<button style="margin-top:15px;" class="button button-primary wcam_primary_button" onclick="wcam_add_field(event);" ><?php _e('Add more', 'woocommerce-attach-me'); ?></button>
			
			<h2 id="wcam-notification-title"><?php _e('Notification & Link embedding', 'woocommerce-attach-me'); ?></h2>
			<div id="wcam-notify-box">
			<?php 
			//if $wcam_options['embed-links-to-complete-mail'] == no, user has unchecked, otherwise if not setted can be override by default setting
			//Default options managment
			if(!isset($wcam_options['embed-links-to-complete-mail'] ) && isset($general_options['embed_links_to_complete_mail']) && $general_options['embed_links_to_complete_mail'] == 'yes')
			{
				$wcam_options['embed-links-to-complete-mail'] = 'yes';
				
				if(isset($general_options['embed_links_hide_by_status']))
					$wcam_options['embed-links-hide-by-status'] = $general_options['embed_links_hide_by_status'];
				if(isset($general_options['embed_links_preset_email_text_id']))
					$wcam_options['embed-links-preset-email-text-id'] = $general_options['embed_links_preset_email_text_id'];
				if(isset($general_options['embed_links_preset_email_text']) && (!isset($general_options['embed_links_preset_email_text_id']) || $general_options['embed_links_preset_email_text_id'] == 'input_text'))
					$wcam_options['embed-links-to-complete-mail-text'] = $general_options['embed_links_preset_email_text'];
			}
			?>
			<input type="checkbox" name="wcam-embed-links-to-complete-mail" id="wcam-embed-links-checkbox" <?php if(isset($wcam_options['embed-links-to-complete-mail']) && $wcam_options['embed-links-to-complete-mail'] == 'yes') {echo 'value="yes" checked="checked"';} else echo 'value="no"'; ?> ></input>
			<label for="wcam-embed-links-to-complete-email-checkbox"><?php _e('Embed download/view links to WooCommerce emails?', 'woocommerce-attach-me'); ?></label>
			<div id="wcam-embed-link-subbox" style="display:none; clear:both; margin-top:10px;">
				<label><?php _e('If embedding option is enable by default links are embedded to ALL outgoing WooCommerce status change emails. However the embedding can be disable according to the current order status:', 'woocommerce-attach-me'); ?></label><br/> 
				<?php foreach((array)$order_statuses as $code => $status): ?>
					<input type="checkbox" <?php if(isset($wcam_options['embed-links-hide-by-status']) && in_array($code, $wcam_options['embed-links-hide-by-status'])) echo 'checked="checked"'; ?>
					   name="wcam-embed-links-hide-by-status[]"  value="<?php echo $code; ?>" ><?php _e('Hide for',''); ?> <?php echo $status ?></input>
					<br/>
				<?php endforeach; ?>
				<br/><br/> 					
				<label><?php _e('Preset body', 'woocommerce-attach-me'); ?></label><br/> 
				<p class="wcam_instruction_paragraph">
					<?php _e('You can use as message body a preset text. Use the following dropdown menu to choose which preset text to load:', 'woocommerce-attach-me'); ?>
				<br/> 	
				<select name="wcam-embed-links-preset-email-text-id" id="wcam_load_preset_embedded_email" data-id="wcam-embed-links-to-complete-mail-text">
					<option value="input_text" <?php if(isset($wcam_options['embed-links-preset-email-text-id']) && $wcam_options['embed-links-preset-email-text-id'] == 'input_text') echo 'selected="selected"'; ?>><?php _e('Use input text', 'woocommerce-attach-me'); ?></option>
					<?php 
						$preset_email_texts = $general_options_model->get_emails_preset_texts();
						foreach((array)$preset_email_texts as $text_data): ?>
							<option value="<?php echo $text_data['body_text_unique_field_id']; ?>" <?php if(isset($wcam_options['embed-links-preset-email-text-id']) && $wcam_options['embed-links-preset-email-text-id'] == $text_data['body_text_unique_field_id']) echo 'selected="selected"'; ?> ><?php echo $text_data['body_text_title']; ?></option>
						<?php endforeach; ?>
				</select>
				</p>
				<br/>
				<label for="wcam-embed-links-to-complete-mail-text"><?php _e('The following text will be included in the WooCommerce emails','woocommerce-attach-me');?></label>
				<p class="wcam_instruction_paragraph">
					<?php _e('Use the <span class="wcam_highlight_text">[wcam_order_page_link]</span> to embed a link to the order page. To customize link text use the following format: <span class="wcam_highlight_text">[wcam_order_page_link]Custom link text[/wcam_order_page_link]</span>.<br/>Use the attachments download/view shortcodes to link the attachment. Ex.: Download your <span class="wcam_highlight_text">[wcam_download_link id="0"]invoice[/wcam_download_link]</span> and <span class="wcam_highlight_text">[wcam_download_link id="1"]digital manual[/wcam_download_link]</span>.<br/>If you want to use headings, note that the <span class="wcam_highlight_text">h3</span> is the default tile tag used in WooCoommerce emails. ', 'woocommerce-attach-me'); ?>
				</p>
				<textarea rows="4" cols="30" id="wcam-embed-links-to-complete-mail-text" name="wcam-embed-links-to-complete-mail-text" class="wcam-embed-link" placeholder="<?php _e('Ex.: To view, click on the follow link [wcam_download_link]', 'woocommerce-attach-me'); ?>" name="wcam-complete-order-email-text">
					<?php if(isset($wcam_options['embed-links-to-complete-mail-text'])) echo  $wcam_options['embed-links-to-complete-mail-text']; ?>
				</textarea>
			</div>			
			<br/>
			<input type="checkbox" name="wcam-notify-via-mail" id="wcam-notify-checkbox" value="no"></input>
			<label for="wcam-notify-checkbox"><?php _e('Send a notification email to the customer? (Hitting save button will send the email)', 'woocommerce-attach-me'); ?></label>
				<div id="wcam-notify-subbox" style="display:none; clear:both; margin-top:10px;">
					<h4 class="wcam_instruction_title"><?php _e('Subject', 'woocommerce-attach-me'); ?></h4>
					<p class="wcam_instruction_paragraph">
						<input type="text" placeholder="<?php _e('Ex.: New Invoice', 'woocommerce-attach-me'); ?>" id="wcam-notification-subject" name="wcam-notification-subject"></input>
					</p>
					<h4 class="wcam_instruction_title"><?php _e('Attachments', 'woocommerce-attach-me'); ?></h4>
						<p class="wcam_instruction_paragraph"><small><?php _e('Only uploaded and files from media gallery can be attacched to the notificaton email.<br/>Remember that some some server email provider will not receive emails with attachments bigger than 10MB (<a target="_blank" href="https://www.outlook-apps.com/maximum-email-size/">Gmail: 25MB, Outlook and Hotmail 10MB,...</a>)', 'woocommerce-attach-me'); ?></small></p>
					<select id="wcam-attachment-ids" multiple="multiple"></select>
					<br/>
					<h4 class="wcam_instruction_title"><?php _e('Preset body', 'woocommerce-attach-me'); ?></h4>
					<p class="wcam_instruction_paragraph">
						<label><?php _e('You can use as message body a preset text. Use the following dropdown menu to choose which preset text to load:', 'woocommerce-attach-me'); ?></label><br/> 
			
							<select name="wcam-notification-preset-email-text-id" id="wcam_load_preset_notification_email" data-id="wcam-message">
								<option value="input_text" <?php if(isset($wcam_options['notification-preset-email-text-id']) && $wcam_options['notification-preset-email-text-id'] == 'input_text') echo 'selected="selected"'; ?>><?php _e('Select a preset text', 'woocommerce-attach-me'); ?></option>
								<?php 
									$preset_email_texts = $general_options_model->get_emails_preset_texts();
									foreach((array)$preset_email_texts as $text_data): ?>
										<option value="<?php echo $text_data['body_text_unique_field_id']; ?>" <?php if(isset($wcam_options['notification-preset-email-text-id']) && $wcam_options['notification-preset-email-text-id'] == $text_data['body_text_unique_field_id']) echo 'selected="selected"'; ?> ><?php echo $text_data['body_text_title']; ?></option>
									<?php endforeach; ?>
							</select>
					</p>
				
					<h4 class="wcam_instruction_title"><?php _e('Message', 'woocommerce-attach-me'); ?></h4>
					<!-- <p class="wcam_instruction_paragraph"><?php _e('Use the <span class="wcam_highlight_text">[wcam_order_page_link]</span> to embed a link to the order page. To customize link text use the following format: <span class="wcam_highlight_text">[wcam_order_page_link]Custom link text[/wcam_order_page_link]</span>.', 'woocommerce-attach-me'); ?></p> -->
					<p class="wcam_instruction_paragraph">
					<?php _e('Use the <span class="wcam_highlight_text">[wcam_order_page_link]</span> to embed a link to the order page. To customize link text use the following format: <span class="wcam_highlight_text">[wcam_order_page_link]Custom link text[/wcam_order_page_link]</span>.<br/>Use the attachments download/view shortcodes to link the attachment. Ex.: Download your <span class="wcam_highlight_text">[wcam_download_link id="0"]invoice[/wcam_download_link]</span> and <span class="wcam_highlight_text">[wcam_download_link id="1"]digital manual[/wcam_download_link]</span>.<br/>If you want to use headings, note that the <span class="wcam_highlight_text">h3</span> is the default tile tag used in WooCoommerce emails. ', 'woocommerce-attach-me'); ?>
				</p>
					<textarea rows="8" id="wcam-message" placeholder="<?php _e('Ex.: Order invoce is now available on order details page. To view, click on the follow link [wcam_download_link id=\"99999\"]', 'woocommerce-attach-me'); ?>" name="wcam-notification-message"></textarea>
				</div>
			</div>
			<div class="wcam_spacer"></div>
			 <button class="button button-primary wcam_primary_button" id="wcam-save-button"><?php _e('Save', 'woocommerce-attach-me'); ?></button>
			<!--<input type="submit" id="wcam_save_order_button"  class="button" value="<?php _e('Save order', 'woocommerce-attach-me'); ?>" ></input> -->
		</div>
		<script>
		var wcam_plugin_path = "<?php echo wcam_PLUGIN_PATH; ?>";
		var wcaf_index = <?php echo ++$max_id; ?>;
		var loader_path = '<img src="<?php echo wcam_PLUGIN_PATH; ?>/images/ajax-loader.gif"></img>';
		var confirm_message = '<?php _e('Are you sure?', 'woocommerce-attach-me'); ?>';
		var send_message_error = '<?php _e('Subject or body message cannot be empty', 'woocommerce-attach-me'); ?>';
		function wcam_add_field(e) 
		{ 
			e.preventDefault();
		   var template ='';
		   
		   template += '<li class="wcam-attachment-box" data-id="'+wcaf_index+'" data-already-uploaded="no">';
		   template += '<input type="hidden" name="wcam_id['+wcaf_index+']" value="'+wcaf_index+'" ></input>';
		   template += '<h2 ><div class="button wcam_collapse_expand_button" data-id="'+wcaf_index+'">-</div> <?php _e('Attachment #', 'woocommerce-attach-me'); ?>'+wcaf_index+'</h2>';
		   template += '<div class="wcam-attachment-box-content" id="wcam-attachment-box-content-'+wcaf_index+'" data-already-collapsed="no">';
		   template += '<h4 ><?php _e('Shortcode', 'woocommerce-attach-me'); ?></h4>';
		   template += ' <p><a href="#" class="wcam_tooltip">[wcam_download_link id="'+wcaf_index+'"]';
				 template += '				<span><?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?> <b>[wcam_download_link id="'+wcaf_index+'"]</b>  <?php _e('in the notification email body (see down below) to embed a direct link to download/view the file.', 'woocommerce-attach-me'); ?></span>';
				 template += '		</a>';
				 template += '</p>';

			 // template += '<h5><?php _e('Attachment', 'woocommerce-attach-me'); ?></h5>';
			   //template += '<p><?php _e('Paste the following shortcode ', 'woocommerce-attach-me'); ?><strong>[wcam_attachment id="'+wcaf_index+'"]</strong> <?php _e('in the notification email body (see down below) to attach the file to the email.', 'woocommerce-attach-me'); ?>';
			 //  template += '<br/>';
			  // template += '<strong><?php _e('Note:', 'woocommerce-attach-me'); ?></strong> <?php _e('External file cannot be attached to the notification email.', 'woocommerce-attach-me'); ?>';	
					
			   template += '<h4 ><?php echo str_replace("'", "\'", __('Attachment title', 'woocommerce-attach-me')); ?></h4>';
			   template += '<input style="display:block;" type="text"id="wcam_attachment_title_'+wcaf_index+'" class="wcam-attachments-titles" name="wcam_attachment_title['+wcaf_index+']" placeholder="<?php echo str_replace("'", "\'", __('Title/description', 'woocommerce-attach-me')); ?>" value="<?php if(isset($general_options['default_attachment_title'][$curr_lang ])) echo $general_options['default_attachment_title'][$curr_lang];?>"></input>';
			   template += '<div id="wcam-file-data-box">';
			   template += '	<input type="radio" id="wcam-radio-file-'+wcaf_index+'" name="wcam-attachment-type'+wcaf_index+'" value="file" checked="checked"></input>';
			   template += '  	<label><?php echo str_replace("'", "\'", __('Upload a file', 'woocommerce-attach-me')); ?></label>';
			   template += '  	<input style="display:block; margin-bottom:15px;" type="file" class="file_input" name="wcam_attachment_file-'+wcaf_index+'" ></input>';
			
			   template += '  	<input type="radio" id="wcam-radio-ext-'+wcaf_index+'" name="wcam-attachment-type'+wcaf_index+'" value="ext"></input>';
			   template += ' 	<label><?php echo str_replace("'", "\'", __('Or attach an external URL:', 'woocommerce-attach-me')); ?></label>';
			   template += '  	<input style="display:block; margin-bottom:15px;" id="wcam-external-link-'+wcaf_index+'" class="wcam-external-url" type="text" name="wcam_attachment_external_url['+wcaf_index+']" placeholder="<?php echo str_replace("'", "\'", __('Ex: http://www.gdocs.com/file.jpg', 'woocommerce-attach-me')); ?>"></input>';
		
			   template += '  	<input type="radio" id="wcam-radio-gallery-'+wcaf_index+'" name="wcam-attachment-type'+wcaf_index+'"  value="file"></input>';
			   template += '  	<label><?php _e('Or attach from gallery:', 'woocommerce-attach-me'); ?></label>';
			   template += '  	<input style="display:block;"   class="wcam-gallery-input" id="wcam-gallery-link-'+wcaf_index+'" class="wcam-gallery-attachment" type="text" name="wcam_attachment_gallery_url['+wcaf_index+']" placeholder="<?php _e('click on browse button', 'woocommerce-attach-me'); ?>" readonly></input>';
			   template += '	<input type="hidden" id="gallery_media_id_'+wcaf_index+'" name="wcam_attachment_gallery_media_id['+wcaf_index+']"></input>';
			   template += '    <input type="button" data-id="'+wcaf_index+'" class="wcam-browse-link button" value="<?php _e('Browse from Media gallery', 'woocommerce-attach-me') ?>" />';
		
			   template += '</div>';
			   
			   template += '<button class="button wccm_advanced_option_button" data-id="'+wcaf_index+'" ><?php _e('Show/Hide advanced options', 'woocommerce-attach-me'); ?></button>';
			   template += '<div class="wccm_advanced_option_box" id="wcam-advanced-options-box-'+wcaf_index+'">';
				   template += '<input type="checkbox" name="wcam-secure-download['+wcaf_index+']"  id="wcam-secure-download-'+wcaf_index+'" class="wcam-customer-approve-checkbox" value="yes" <?php if(isset($general_options['secure_download']) && $general_options['secure_download'] == 'yes') echo 'checked="checked"';?>></input>';
				   template += '<label ><?php echo str_replace("'", "\'", __('Secure download (Only the owner of the order can view/download the file). This option will not work with external  links.', 'woocommerce-attach-me')); ?></label>';
				   template += '<br/>';
				   
				   template += '<input type="checkbox" name="wcam-customer-approve-checkbox['+wcaf_index+']" class="wcam-customer-approve-checkbox" value="yes" <?php if(isset($general_options['approve_checkbox_default_value']) && $general_options['approve_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
				   template += '<label ><?php echo str_replace("'", "\'", __('Let customer approve? ', 'woocommerce-attach-me')); ?></label>';
				   template += '<br/>';
				   
				   template += '<input type="checkbox"  name="wcam-customer-reapprove-checkbox['+wcaf_index+']" class="wcam-customer-approve-checkbox" value="yes"<?php if(isset($general_options['reapprove_checkbox_default_value']) && $general_options['reapprove_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
				   template += '<label><?php _e('Can customer change approval value after submit?', 'woocommerce-attach-me'); ?></label>';
				   template += '<br/>';
					 
				   template += '<input type="checkbox" name="wcam-customer-feedback-checkbox['+wcaf_index+']"  class="wcam-customer-approve-checkbox" value="yes" <?php if(isset($general_options['feedback_checkbox_default_value']) && $general_options['feedback_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
				   template += '<label for="wcam-customer-feedback-checkbox"><?php echo str_replace("'", "\'", __('Let customer leave a text feedback?', 'woocommerce-attach-me')); ?></label>';
				   template += '<br/>';
				   
				 //  template += ' <input type="checkbox" value="yes" name="wcam-customer-feedback-required['+wcaf_index+']"  value="yes" <?php if(isset($general_options['feedback_checkbox_default_value']) && $general_options['feedback_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>><?php _e('Is feedback required?', 'woocommerce-attach-me'); ?></input>';
					//template += 					'<br/>';
					template += 					'<?php _e('... and feedback is required when (it works only if the previous feedback option has been enabled)', 'woocommerce-attach-me'); ?>';
					template += 					'<br/><select type="checkbox" name="wcam-customer-feedback-strategy['+wcaf_index+']"  value="yes">';
					template += 						'<option value="always" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'always') echo 'selected="selected"';?>><?php _e('Always', 'woocommerce-attach-me'); ?></option>';
					template += 						'<option value="only_yes" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'only_yes') echo 'selected="selected"';?>><?php _e('If customer approves', 'woocommerce-attach-me'); ?></option>';
					template += 						'<option value="only_no" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'only_no') echo 'selected="selected"';?>><?php _e('If customer does not approve', 'woocommerce-attach-me'); ?></option>';
					template += 						'<option value="never" <?php if(isset($general_options['feedback_strategy']) && $general_options['feedback_strategy'] == 'never') echo 'selected="selected"';?>><?php _e('Never', 'woocommerce-attach-me'); ?></option>';
					template += 					'</select>';
					template += 				'<br/>';
				   
				   template += '<input type="checkbox" name="wcam-customer-admin-notification-checkbox['+wcaf_index+']" class="wcam-customer-admin-notification-checkbox" value="yes" <?php if(isset($general_options['customer_admin_notification_checkbox_default_value']) && $general_options['customer_admin_notification_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
				   template += '<label for="wcam-customer-admin-notification['+wcaf_index+']"><?php _e('Receive an email when the customer approves or leaves a feedback? ', 'woocommerce-attach-me'); ?></label>';
				  template += 	'<br/>';
				  template += '<input type="checkbox" <?php if(isset($general_options['display_copy_to_clipboard_button']) && $general_options['display_copy_to_clipboard_button'] == 'yes') echo 'checked="checked"';?> name="wcam-display-copy-to-clipboard-button['+wcaf_index+']"  value="yes"></input>';
				  template += 					'<label><?php _e('Display also the "Copy attached file url to clipboard" button next the "Download/View" button on Order details page?', 'woocommerce-attach-me'); ?></label>';
				  template += 				'<br/>';
				  
				   template += '<h4><?php _e('Select for which order status the attachment has to be hidden', 'woocommerce-attach-me'); ?></h4>';
									<?php 
									 $order_statuses = wc_get_order_statuses();
									 foreach((array)$order_statuses as $code => $status): 
										$status = str_replace("'", "\'", $status);
										$hide_on_current_status= isset($general_options['order_attachment_hide_by_status']) && in_array($code, $general_options['order_attachment_hide_by_status']) ? true : false;
									 ?>
					 template += '					<input type="checkbox" <?php if($hide_on_current_status) echo 'checked="checked"';?>';
					 template += '					   name="wcam-order-attachment-hide-by-status['+wcaf_index+'][]"  value="<?php echo $code; ?>"><?php _e('Hide on',''); ?> <?php echo $status ?></input>';
					 template += '					<br/>';
									<?php endforeach; ?>
				  
				  
				   template += '<h4 class="wcam_email_attachments_title"><?php _e('Email attachments (attachments will not work with exteran file/url)', 'woocommerce-attach-me'); ?></h4>';
				 
					template += '<input type="checkbox" class="wcam-attach-file-to-complete-order-email" name="wcam-attach-file-to-processing-order-email['+wcaf_index+']"  value="yes" <?php if(isset($general_options['attach_file_to_processing_order_email_default_value']) && $general_options['attach_file_to_processing_order_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
					template += '<label for="wcam-attach-file-to-processing-order-email['+wcaf_index+']"><?php _e('Attach file to the Processing order email', 'woocommerce-attach-me'); ?></label><br/>';
									
					template += '<input class="wcam-attach-file-to-complete-order-email" type="checkbox" name="wcam-attach-file-to-complete-order-email['+wcaf_index+']"  value="yes" <?php if(isset($general_options['attach_file_to_complete_order_email_default_value']) && $general_options['attach_file_to_complete_order_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
					template += '<label for="cam-attach-file-to-complete-order-email['+wcaf_index+']"><?php _e('Attach file to the Complete order email', 'woocommerce-attach-me'); ?></label><br/>';

					template += '<input type="checkbox" class="wcam-attach-file-to-complete-order-email" name="wcam-attach-file-to-customer-invoice-email['+wcaf_index+']"  value="yes" <?php if(isset($general_options['attach_file_to_customer_invoice_email_default_value']) && $general_options['attach_file_to_customer_invoice_email_default_value'] == 'yes') echo 'checked="checked"';?>></input>';
					template += '<label for="wcam-attach-file-to-customer-invoice-email['+wcaf_index+']"><?php _e('Attach file to the Customer Invoice email', 'woocommerce-attach-me'); ?></label><br/>';
					
					template += '<h4 class="wcam_expiration_strategy_title"><?php _e('Expires?', 'woocommerce-attach-me'); ?></h4>';
					template += '				<select class="wcam_expiration_strategy" name="wcam_expiration_strategy['+wcaf_index+']" data-id="'+wcaf_index+'">';
					template += '					<option value="never"><?php _e('Never', 'woocommerce-attach-me'); ?></option>';
					template += '					<option value="specific_date"><?php _e('At specific date', 'woocommerce-attach-me'); ?></option>';
					template += '					<option value="relative_date"><?php _e('After the selected time amount after order date', 'woocommerce-attach-me'); ?></option>';
					template += '				</select>';
									
					template += '				<div id="wcam_expiration_relative_time_amout_selection_box_'+wcaf_index+'" class="wcam_expiration_relative_time_amout_selection_box">';
					template += '					<label><?php _e('Time amount: ', 'woocommerce-attach-me'); ?></label>';
					template += '					<input type="number" min="1" step="1" name="wcam_expiration_relative_time_amount['+wcaf_index+']" value="1"></input>';
										
					template += '					<label class="wcam_expiration_relative_time_time_type_label"><?php _e('Time type: ', 'woocommerce-attach-me'); ?></label>';
					template += '					<select class="wcam_expiration_strategy" name="wcam_expiration_relative_time_type['+wcaf_index+']" data-id="'+wcaf_index+'">';
					template += '						<option value="second"><?php _e('Seconds', 'woocommerce-attach-me'); ?></option>';
					template += '						<option value="minute"><?php _e('Minutes', 'woocommerce-attach-me'); ?></option>';
					template += '						<option value="hour"><?php _e('Hours', 'woocommerce-attach-me'); ?></option>';
					template += '						<option value="day"><?php _e('Days', 'woocommerce-attach-me'); ?></option>';
					template += '						<option value="month"><?php _e('Months', 'woocommerce-attach-me'); ?></option>';
					template += '					</select>';
					template += '				</div>';
					template += '				<div id="wcam_expiration_specific_date_selection_box_'+wcaf_index+'" class="wcam_expiration_specific_date_selection_box">';
					template += '					<label><?php _e('Date and time: ', 'woocommerce-attach-me'); ?></label>';
					template += '					<input type="text" class="wcam_expiration_specific_date" name="wcam_expiration_specific_date['+wcaf_index+']" ></input>';
					template += '				</div>';
					
					
					template += '</div>';		
			   
			   template += '<button style="display:block; margin-top:15px;" class="button delete_button" data-fileid="'+wcaf_index+'" onclick="wcam_remove_field(event);" ><?php echo str_replace("'", "\'", __('Delete', 'woocommerce-attach-me')); ?></button>';
			 template += '</div>';
		  		

			template += '</li>';
		   jQuery("#wcam-attachments-list").append(template);
		   wcam_init_datetime_picker();
		   wcaf_index++;
		   return false;
		}
		</script>
		<div class="clear"></div>
		</div>
		
		<?php 
	}
	
}
?>