<?php 
class WCAM_EmailsPresetTextsConfigurator
{
	public function __construct()
	{
		//add_filter('acf/init', array(&$this,'init_options_menu'));
		add_action('wp_ajax_wcam_get_preset_email_text', array(&$this, 'ajax_get_preset_email_text'));
		$this->init_options_menu();
	}
	
	public function ajax_get_preset_email_text()
	{
		$general_options_model = new WCAM_Option();
		if(isset($_POST['wcam-preset-id']))
		{
			
			echo $general_options_model->get_emails_preset_text_by_id($_POST['wcam-preset-id']);
		}
		echo "";
		wp_die();
	}
	function init_options_menu()
	{
		if( function_exists('acf_add_options_page') ) 
		{	
			 acf_add_options_sub_page(array(
				'page_title' 	=> 'Email preset texts',
				'menu_title'	=> 'Email preset texts',
				'parent_slug'	=> 'woocommerce-attach-me',
			));
			
			
			
			add_action( 'current_screen', array(&$this, 'cl_set_global_options_pages') );
		}
	}
	/**
	 * Force ACF to use only the default language on some options pages
	 */
	function cl_set_global_options_pages($current_screen) 
	{
	  if(!is_admin())
		  return;
	  
	  //wcam_var_dump($current_screen->id);
	  $wcam_wpml_helper = new WCAM_Wpml();
	  $page_ids = array(
		"attach-me_page_acf-options-email-preset-texts"
	  );
	
	  if (in_array($current_screen->id, $page_ids)) 
	  {
		$wcam_wpml_helper->switch_to_default_language();
		add_filter('acf/settings/current_language', array(&$this, 'cl_acf_set_language'), 100);
	  }
	}
	

	function cl_acf_set_language() 
	{
	  return acf_get_setting('default_language');
	}

	
}
?>