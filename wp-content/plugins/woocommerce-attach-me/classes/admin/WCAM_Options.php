<?php 
class WCAM_Options
{
	public function __construct()
	{
	}
	/* static function get_option($option_name = null)
	{
		$options = get_option( 'wcam_general_options');
		$options = isset($options) ? $options: null;
		
		$result = null;
		if($option_name)
		{
			if($option_name == 'allowed_roles')
			{
				$result = !isset($options[$option_name]) ? array('customer','subscriber') : $options[$option_name];
			}
			elseif(isset($options[$option_name]))
				$result = $options[$option_name];
			else 
				$result = 'false';
			
				
		}
		else
			$result = $options;

		return $result;
	} */
	public function render_page() 
	{
		$this->render_options_page();
	}
	private function render_options_page()
	{
		$option = new WCAM_Option();
		if(isset($_POST['wcam_general_options']))
			$option->save_options($_POST['wcam_general_options']);
			
		$options = $option->get_option();//get_option( 'wcam_general_options');
		$time_offset = isset($options['time_offset']) ? $options['time_offset'] : 0;
		
		wp_enqueue_style( 'wcam-common', wcam_PLUGIN_PATH.'/css/wcam-common.css'  );
		wp_enqueue_style( 'wcam-options-page', wcam_PLUGIN_PATH.'/css/wcam-backend-options.css'  );
		
		//wp_enqueue_script('wcam-dropzone', wcam_PLUGIN_PATH.'/js/dropzone.js');
	    wp_enqueue_script('wcam-dropzone', wcam_PLUGIN_PATH.'/js/wcam-backend-options.js');
		
		$default_width = get_option('thumbnail' . '_size_w' );
		$default_height = get_option('thumbnail' . '_size_h' );
		$wpml = new WCAM_Wpml();
		$curr_lang = $wpml->get_current_language();
		$order_statuses = wc_get_order_statuses();
		?>
		<div class="wrap">
			<h2><?php _e('Options page', 'woocommerce-attach-me');?></h2>
			<!--<form method="post" action="options.php">-->
			<form method="post"  action=""  id="wcam-options-form">
			<?php //settings_fields('wcam_general_options_group'); ?> 
			<div id="options-container">
				
				<h3><?php _e('Order attachments', 'woocommerce-attach-me');?></h3>
				<p>
				<label><?php _e('Folder name (use only alphanumeric characters, NO special characters allowed). Previous attachments will not be moved to the new folder', 'woocommerce-attach-me');?></label>
				<input type="text" name="wcam_general_options[folder_name]" value="<?php if(isset($options['folder_name'])) echo $options['folder_name']; else echo "wcam" ?>" required></input><br/>
				<small><?php _e('Upload folder is located in wp-content/uploads/ folder.', 'woocommerce-attach-me');?></small>
				</p>
				
				<label><?php _e('File name prefix', 'woocommerce-attach-me');?></label>
				<input type="checkbox" <?php if(isset($options['disable_random_number_prefix']) && $options['disable_random_number_prefix'] == 'yes') echo 'checked="checked"';?> 
					   name="wcam_general_options[disable_random_number_prefix]" class="wcam-customer-approve-checkbox" value="yes"><?php _e('Disable the random number prefix added to the file name.', 'woocommerce-attach-me'); ?></input>
				<br/><br/>
				<label><?php _e('Choose which options are "on" by default (for each attachment you can lately change the default options directly in the order page)' , 'woocommerce-attach-me');?></label>
				
				<input type="checkbox" <?php if(isset($options['secure_download']) && $options['secure_download'] == 'yes') echo 'checked="checked"';?> 
					   name="wcam_general_options[secure_download]" class="wcam-customer-approve-checkbox" value="yes"><?php _e('Secure download (Only the owner of the order can view/download the file). This option will not work with external  links. ', 'woocommerce-attach-me'); ?></input>
				<br/>				
				<input type="checkbox" <?php if(isset($options['approve_checkbox_default_value']) && $options['approve_checkbox_default_value'] == 'yes') echo 'checked="checked"';?> 
					   name="wcam_general_options[approve_checkbox_default_value]" class="wcam-customer-approve-checkbox" value="yes"><?php _e('Let customer approves? ', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['reapprove_checkbox_default_value']) && $options['reapprove_checkbox_default_value'] == 'yes') echo 'checked="checked"';?> 
					   name="wcam_general_options[reapprove_checkbox_default_value]" class="wcam-customer-reapprove-checkbox" value="yes"><?php _e('Can customer change approval value after submit?', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['feedback_checkbox_default_value']) && $options['feedback_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>
						name="wcam_general_options[feedback_checkbox_default_value]"  class="wcam-customer-feedback-checkbox" value="yes"><?php _e('Let customer leave a text feedback?', 'woocommerce-attach-me'); ?></input>
				<br/>
				<!--<input type="checkbox" <?php if(isset($options['feedback_required']) && $options['feedback_required'] == 'yes') echo 'checked="checked"';?>
						name="wcam_general_options[feedback_required]"  class="wcam-customer-feedback-checkbox" value="yes"><?php _e('Is feedback required?', 'woocommerce-attach-me'); ?></input>-->
				
				&nbsp;&nbsp;&nbsp;<?php _e('... and feedback is required when (it works only if the previous feedback option has been enabled)', 'woocommerce-attach-me'); ?>
				<br/><select name="wcam_general_options[feedback_strategy]" >
					<option value="always" <?php if(isset($options['feedback_strategy']) && $options['feedback_strategy'] == 'always') echo 'selected="selected"';?>><?php _e('Always', 'woocommerce-attach-me'); ?></option>
					<option value="only_yes" <?php if(isset($options['feedback_strategy']) && $options['feedback_strategy'] == 'only_yes') echo 'selected="selected"';?>><?php _e('If customer approves', 'woocommerce-attach-me'); ?></option>
					<option value="only_no" <?php if(isset($options['feedback_strategy']) && $options['feedback_strategy'] == 'only_no') echo 'selected="selected"';?>><?php _e('If customer does not approve', 'woocommerce-attach-me'); ?></option>
					<option value="never" <?php if(isset($options['feedback_strategy']) && $options['feedback_strategy'] == 'never') echo 'selected="selected"';?>><?php _e('Never', 'woocommerce-attach-me'); ?></option>
				</select>
				
				<br/>
				<input  type="checkbox" <?php if(isset($options['customer_admin_notification_checkbox_default_value']) && $options['customer_admin_notification_checkbox_default_value'] == 'yes') echo 'checked="checked"';?>
					   name="wcam_general_options[customer_admin_notification_checkbox_default_value]" class="wcam-customer-admin-notification-checkbox" value="yes"><?php _e('Receive an email when the customer approve or leaves a feedback? ', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['display_copy_to_clipboard_button']) && $options['display_copy_to_clipboard_button'] == 'yes') echo 'checked="checked"';?>
						name="wcam_general_options[display_copy_to_clipboard_button]"  class="wcam-customer-feedback-checkbox" value="yes"><?php _e('Display also the "Copy attached file url to clipboard" button next the "Download/View" button on Order details page?', 'woocommerce-attach-me'); ?></input>
				<br/>
				
				<p><strong><i><?php _e('Select for which order status has to be hidden:', 'woocommerce-attach-me'); ?></i></strong><br/>
				<?php 
				 
				 foreach((array)$order_statuses as $code => $status): 
					
					$hide_on_current_status= isset($options['order_attachment_hide_by_status']) && in_array($code, $options['order_attachment_hide_by_status']) ? true : false;
				 ?>
				
					 <input type="checkbox" <?php if($hide_on_current_status) echo 'checked="checked"';?>
					   name="wcam_general_options[order_attachment_hide_by_status][]"  value="<?php echo $code; ?>"><?php _e('Hide on',''); ?> <?php echo $status ?></input>
					<br/>
				 
				<?php endforeach; ?>
				</p>
				
				<p><strong><i><?php _e('Select to which status email the attachment as to be attached:', 'woocommerce-attach-me'); ?></i></strong><br/>
				<input type="checkbox" <?php if(isset($options['attach_file_to_processing_order_email_default_value']) && $options['attach_file_to_processing_order_email_default_value'] == 'yes') echo 'checked="checked"';?>
					   class="wcam-attach-file-to-complete-order-email" name="wcam_general_options[attach_file_to_processing_order_email_default_value]"  value="yes"><?php _e('Attach file to the Processing order email', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['attach_file_to_complete_order_email_default_value']) && $options['attach_file_to_complete_order_email_default_value'] == 'yes') echo 'checked="checked"';?>
					   class="wcam-attach-file-to-complete-order-email" name="wcam_general_options[attach_file_to_complete_order_email_default_value]"  value="yes"><?php _e('Attach file to the Complete order email', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['attach_file_to_customer_invoice_email_default_value']) && $options['attach_file_to_customer_invoice_email_default_value'] == 'yes') echo 'checked="checked"';?>
					   class="wcam-attach-file-to-complete-order-email" name="wcam_general_options[attach_file_to_customer_invoice_email_default_value]"  value="yes"><?php _e('Attach file to the Customer Invoice email', 'woocommerce-attach-me'); ?></input>
				</p>
				
				
				<strong><i><?php _e('Embed download/view links to WooCommerce emails', 'woocommerce-attach-me');?></i></strong><br/>
				<input type="checkbox" <?php if(isset($options['embed_links_to_complete_mail']) && $options['embed_links_to_complete_mail'] == 'yes') echo 'checked="checked"';?>
				                      id="embed_links_to_complete_mail" name="wcam_general_options[embed_links_to_complete_mail]" value="yes"><?php _e('Embed download/view links to WooCommerce emails?', 'woocommerce-attach-me'); ?></input>
				
				<div id="embed_links_to_complete_mail_box" <?php if(!isset($options['embed_links_to_complete_mail'])) echo 'style="display:none;"'; ?>>
					<p><strong><i><?php _e('If embedding option is enable by default links are embedded to ALL outgoing WooCommerce status change emails. However the embedding can be disable according to the current order status:', 'woocommerce-attach-me'); ?></i></strong></p> 
					<?php foreach((array)$order_statuses as $code => $status): ?>
						<input type="checkbox" <?php if(isset($options['embed_links_hide_by_status']) && in_array($code, $options['embed_links_hide_by_status'])) echo 'checked="checked"'; ?>
						   name="wcam_general_options[embed_links_hide_by_status][]"  value="<?php echo $code; ?>" ><?php _e('Hide for',''); ?> <?php echo $status ?></input>
						<br/>
					<?php endforeach; ?>
					
					<p><strong><i><?php _e('You can use as message body a preset text. Use the following dropdown menu to choose which preset text to load:', 'woocommerce-attach-me'); ?></i></strong></p> 
					<select name="wcam_general_options[embed_links_preset_email_text_id]" id="wcam_load_preset_embedded_email" data-id="wcam-embed-links-to-complete-mail-text">
						<option value="input_text" <?php if(isset($options['embed_links_preset_email_text_id']) && $options['embed_links_preset_email_text_id'] == 'input_text') echo 'selected="selected"'; ?>><?php _e('Use input text', 'woocommerce-attach-me'); ?></option>
						<?php 
							$preset_email_texts = $option->get_emails_preset_texts();
							foreach((array)$preset_email_texts as $text_data): ?>
								<option value="<?php echo $text_data['body_text_unique_field_id']; ?>" <?php if(isset($options['embed_links_preset_email_text_id']) && $options['embed_links_preset_email_text_id'] == $text_data['body_text_unique_field_id']) echo 'selected="selected"'; ?> ><?php echo $text_data['body_text_title']; ?></option>
							<?php endforeach; ?>
					</select>
					<p><strong><i><?php _e('If none of the preset text has been selected, the following text will be used:', 'woocommerce-attach-me'); ?></i></strong></p> 
					<p class="wcam_instruction_paragraph"><strong><?php _e('Istruction:', 'woocommerce-attach-me'); ?></i></strong><br/>
						<?php _e('Use the <span class="wcam_highlight_text">[wcam_order_page_link]</span> to embed a link to the order page. To customize link text use the following format: <span class="wcam_highlight_text">[wcam_order_page_link]Custom link text[/wcam_order_page_link]</span>.<br/>Use the attachments download/view shortcodes to link the attachment. Ex.: Download your <span class="wcam_highlight_text">[wcam_download_link id="0"]invoice[/wcam_download_link]</span> and <span class="wcam_highlight_text">[wcam_download_link id="1"]digital manual[/wcam_download_link]</span>.<br/>If you want to use headings, note that the <span class="wcam_highlight_text">h3</span> is the default tile tag used in WooCoommerce emails. ', 'woocommerce-attach-me'); ?>
					</p>
					<?php 
					$content = isset($options['embed_links_preset_email_text']) ? $options['embed_links_preset_email_text'] : "";
					wp_editor( $content, 'embed_links_preset_email_text', array('textarea_name' => "wcam_general_options[embed_links_preset_email_text]", 
																			'media_buttons' => false,
																			'textarea_rows' => 6) ); ?>
					
				</div>
				
				<div style="display:block; margin-top:30px;">
					<label><?php _e('Status change according to the approval answer (takes effect only if you have enabled the "Let customer approves" option):', 'woocommerce-attach-me'); ?></label>
					<span class="wcam_status_change_due_to_approval_info_text"><?php _e('Select which status assign when an attachment is marked as <strong>approved</strong> by the user', 'woocommerce-attach-me'); ?></span>
					<select name="wcam_general_options[status_to_assign_for_approval]" >
						<option value="unchanged" <?php if($options['status_to_assign_for_approval'] == 'unchanged') echo 'selected="selected"';?>><?php _e('Leave unchanged', 'woocommerce-attach-me'); ?></option>
						<?php 
								foreach((array)$order_statuses as $code => $status): 
									$selected = $options['status_to_assign_for_approval'] == $code ? 'selected = "selected"' : "";
									echo '<option value="'.$code.'" '.$selected.'>'.$status.'</option>';
								endforeach;
							 ?>
					</select>
					
					<span class="wcam_status_change_due_to_approval_info_text"><?php _e('Select which status assign when an attachment is marked as <strong>not approved</strong> by the user', 'woocommerce-attach-me'); ?></span>
					<select name="wcam_general_options[status_to_assign_for_not_approval]" >
						<option value="unchanged" <?php if($options['status_to_assign_for_not_approval'] == 'unchanged') echo 'selected="selected"';?>><?php _e('Leave unchanged', 'woocommerce-attach-me'); ?></option>
						<?php 
								foreach((array)$order_statuses as $code => $status): 
									$selected = $options['status_to_assign_for_not_approval'] == $code ? 'selected = "selected"' : "";
									echo '<option value="'.$code.'" '.$selected.'>'.$status.'</option>';
								endforeach;
							 ?>
					</select>
					
					<span class="wcam_status_change_due_to_approval_info_text"><?php _e('Select which status assign when an attachment is marked as <strong>waiting for approval</strong> by the user', 'woocommerce-attach-me'); ?></span>
					<select  name="wcam_general_options[status_to_assign_for_waiting_for_approval]" >
						<option value="unchanged" <?php if($options['status_to_assign_for_waiting_for_approval'] == 'unchanged') echo 'selected="selected"';?>><?php _e('Leave unchanged', 'woocommerce-attach-me'); ?></option>
						<?php 
								foreach((array)$order_statuses as $code => $status): 
									$selected = $options['status_to_assign_for_waiting_for_approval'] == $code ? 'selected = "selected"' : "";
									echo '<option value="'.$code.'" '.$selected.'>'.$status.'</option>';
								endforeach;
							 ?>
					</select>
				</div>
				
				<h3><?php _e('Locale - Set time offset', 'woocommerce-attach-me');?></h3>
				<p><?php _e('Product and Order attachments can have an expiring time. Time is synchronized using server time. Current server time is: ', 'woocommerce-attach-me'); ?>
				<strong><?php echo date("l").", ".date("H:i"); ?></strong><br/>
				<?php _e('Adjusted server time with offeset is: ', 'woocommerce-attach-me'); ?>
				<strong><?php echo date("l", strtotime($time_offset.' minutes')).", ".date("H:i", strtotime($time_offset.' minutes')); ?></strong></p>
				<label><?php _e('Set the time offset to syncronize server time with your local time. Time is expressed in minutes (so to increase by 1 hour select 60): ', 'woocommerce-attach-me'); ?></label>
				<input type="number" name="wcam_general_options[time_offset]" min="-1440" max="1440" value="<?php echo $time_offset; ?>"></inptu>
				
				<p>
					<label><?php _e('Select time format used to display expiring date on order page', 'woocommerce-attach-me');?></label>
					<select type="text" name="wcam_general_options[exiring_date_time_format]" >
						<option value="d/m/Y" <?php if(isset($options['exiring_date_time_format']) && $options['exiring_date_time_format']=='d/m/Y') echo 'selected="selected"'; ?>>dd/mm/yyyy</option>
						<option value="m/d/Y" <?php if(isset($options['exiring_date_time_format']) && $options['exiring_date_time_format']=='m/d/Y') echo 'selected="selected"'; ?>>mm/dd/yyyy</option>
					</select>
				</p>
				
				<h3><?php _e('Texts', 'woocommerce-attach-me');?></h3>
				<?php if($wpml->is_wpml_active()):?>
					<small><strong><?php _e('NOTE:', 'woocommerce-attach-me');?></strong> <?php _e('WPML Detected! to translate following texts simply select the language you desire from the upper WPML language selector, edit texts and save!', 'woocommerce-attach-me');?> </small>
				<?php endif; ?>
				
				<h4><?php _e('Admin Order details page', 'woocommerce-attach-me');?></h4>
				<p>
					<label><?php _e('Attachment default title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[default_attachment_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['default_attachment_title'][$curr_lang ])) echo $options['default_attachment_title'][$curr_lang]; else _e('Attachment', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<h4><?php _e('Frontend Product page page', 'woocommerce-attach-me');?></h4>
				<p>
					<label><?php _e('Attachment tab title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[product_tab_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['product_tab_title'][$curr_lang ])) echo $options['product_tab_title'][$curr_lang]; else _e('Attachment', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<h4><?php _e('Frontend My Account -> Orders list page', 'woocommerce-attach-me');?></h4>				
				<p>
					<label><?php _e('View attachment button', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[view_attachments_button][<?php echo $curr_lang ?>]" value="<?php if(isset($options['view_attachments_button'][$curr_lang ])) echo $options['view_attachments_button'][$curr_lang]; else _e('View Attachments', 'woocommerce-attach-me');; ?>"></input>
				</p>
				
				<h4><?php _e('Frontend Order details page', 'woocommerce-attach-me');?></h4>				
				<p>
					<label><?php _e('No attachments message (This message will be displayed there are no attachments. Live empty to not display anything)', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[no_attachments_message][<?php echo $curr_lang ?>]" value="<?php if(isset($options['no_attachments_message'][$curr_lang ])) echo $options['no_attachments_message'][$curr_lang]; else ""; ?>"></input>
				</p>
				<p>
					<label><?php _e('Attachments section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[title_box][<?php echo $curr_lang ?>]" value="<?php if(isset($options['title_box'][$curr_lang ])) echo $options['title_box'][$curr_lang]; else _e('Attachments', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('Download / View button', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[download_view_button_text][<?php echo $curr_lang ?>]" value="<?php if(isset($options['download_view_button_text'][$curr_lang ])) echo $options['download_view_button_text'][$curr_lang]; else _e('Download / View', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('Copy file URL to clipboard', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[copy_to_clipboard_button_text][<?php echo $curr_lang ?>]" value="<?php if(isset($options['copy_to_clipboard_button_text'][$curr_lang ])) echo $options['copy_to_clipboard_button_text'][$curr_lang]; else _e('Copy file URL to clipboard', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('Approval box title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[approve_box_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['approve_box_title'][$curr_lang ])) echo $options['approve_box_title'][$curr_lang]; else _e('Do you approve?', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('Approval already sent message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[approve_already_sent_message][<?php echo $curr_lang ?>]" ><?php if(isset($options['approve_already_sent_message'][$curr_lang ])) echo $options['approve_already_sent_message'][$curr_lang]; else _e('Approvation already sent. Selected value was:', 'woocommerce-attach-me'); ?></textarea>
				</p>
				<p>
					<label><?php _e('Approval options', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[approve_not_yet_text][<?php echo $curr_lang ?>]" placeholder="<?php _e("Text for 'waiting for approval' status", 'woocommerce-attach-me');?>" value="<?php if(isset($options['approve_not_yet_text'][$curr_lang ])) echo $options['approve_not_yet_text'][$curr_lang]; else _e('Not yet', 'woocommerce-attach-me'); ?>" required></input><br/>
					<input type="text" name="wcam_general_options[approve_yes_text][<?php echo $curr_lang ?>]" placeholder="<?php _e("Text for 'approved' status", 'woocommerce-attach-me');?>" value="<?php if(isset($options['approve_yes_text'][$curr_lang ])) echo $options['approve_yes_text'][$curr_lang]; else _e('Yes', 'woocommerce-attach-me'); ?>" required></input><br/>
					<input type="text" name="wcam_general_options[approve_no_text][<?php echo $curr_lang ?>]" placeholder="<?php _e("Text for 'not approved' status", 'woocommerce-attach-me');?>" value="<?php if(isset($options['approve_no_text'][$curr_lang ])) echo $options['approve_no_text'][$curr_lang]; else _e('No', 'woocommerce-attach-me'); ?>" required></input><br/>
				</p>
				<p>
					<label><?php _e('Feedback section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[feedback_box_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['feedback_box_title'][$curr_lang ])) echo $options['feedback_box_title'][$curr_lang]; else _e('Please enter a feedback', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<h4><?php _e('Product attachments section', 'woocommerce-attach-me');?></h4>
				<p>
					<label><?php _e('Attacments section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[product_attachments_box][<?php echo $curr_lang ?>]" value="<?php if(isset($options['product_attachments_box'][$curr_lang ])) echo $options['product_attachments_box'][$curr_lang]; else _e('Downloads', 'woocommerce-attach-me'); ?>"></input>
				</p>
				
				<h4><?php _e('Emails', 'woocommerce-attach-me');?></h4>
				<p>
					<label><?php _e('Customer approval section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[email_approve_box_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['email_approve_box_title'][$curr_lang ])) echo $options['email_approve_box_title'][$curr_lang]; else _e('Customer has approved?', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('Customer feedback section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[email_feedback_box_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['email_feedback_box_title'][$curr_lang ])) echo $options['email_feedback_box_title'][$curr_lang]; else _e('Customer feedback', 'woocommerce-attach-me'); ?>"></input>
				</p>
				<p>
					<label><?php _e('No customer feedback message', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[email_no_customer_feedback_message][<?php echo $curr_lang ?>]" value="<?php if(isset($options['email_no_customer_feedback_message'][$curr_lang])) echo $options['email_no_customer_feedback_message'][$curr_lang]; else _e('No customer feedback', 'woocommerce-attach-me'); ?>"></input>
				</p>
				
				<!--<h4><?php _e('Automatic email attachments', 'woocommerce-attach-me');?></h4>
				<p><?php _e('If you choose to attach automatic attachmets to emails (use the "Automatic emails attachments configurator" menu voice), an extra text title and message can be added in the email bodies. Leave empty to not include any extra text.', 'woocommerce-attach-me');?></p>
				<p>
					<label><?php _e('New account email extra section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[automatic_customer_new_account_email_attachments_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['automatic_customer_new_account_email_attachments_title'][$curr_lang ])) echo $options['automatic_customer_new_account_email_attachments_title'][$curr_lang]; ?>"></input>
				</p>
				<p>
					<label><?php _e('New account email extra text message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[automatic_customer_new_account_email_attachments_body][<?php echo $curr_lang ?>]"><?php if(isset($options['automatic_customer_new_account_email_attachments_body'][$curr_lang ])) echo $options['automatic_customer_new_account_email_attachments_body'][$curr_lang];  ?></textarea>
					<br/><br/>
				</p>
				<p>
					<label><?php _e('New order email extra section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[automatic_new_order_email_attachments_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['automatic_new_order_email_attachments_title'][$curr_lang ])) echo $options['automatic_new_order_email_attachments_title'][$curr_lang]; ?>"></input>
				</p>
				<p>
					<label><?php _e('New order email extra text message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[automatic_new_order_email_attachments_body][<?php echo $curr_lang ?>]"><?php if(isset($options['automatic_new_order_email_attachments_body'][$curr_lang ])) echo $options['automatic_new_order_email_attachments_body'][$curr_lang];  ?></textarea>
					<br/><br/>
				</p>
				<p>
					<label><?php _e('Processing order email extra section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[automatic_customer_processing_order_email_attachments_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['automatic_customer_processing_order_email_attachments_title'][$curr_lang ])) echo $options['automatic_customer_processing_order_email_attachments_title'][$curr_lang]; ?>"></input>
					<br/><br/>
				</p>
				<p>
					<label><?php _e('Processing order email extra text message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[automatic_customer_processing_order_email_attachments_body][<?php echo $curr_lang ?>]"><?php if(isset($options['automatic_customer_processing_order_email_attachments_body'][$curr_lang ])) echo $options['automatic_customer_processing_order_email_attachments_body'][$curr_lang];  ?></textarea>
					<br/><br/>
				</p>
				<p>
					<label><?php _e('Complete order email extra section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[automatic_customer_completed_order_email_attachments_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['automatic_customer_completed_order_email_attachments_title'][$curr_lang ])) echo $options['automatic_customer_completed_order_email_attachments_title'][$curr_lang]; ?>"></input>
				</p>
				<p>
					<label><?php _e('Complete order email extra text message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[automatic_customer_completed_order_email_attachments_body][<?php echo $curr_lang ?>]"><?php if(isset($options['automatic_customer_completed_order_email_attachments_body'][$curr_lang ])) echo $options['automatic_customer_completed_order_email_attachments_body'][$curr_lang];  ?></textarea>
					<br/><br/>
				</p>
				<p>
					<label><?php _e('Invoice order email extra section title', 'woocommerce-attach-me');?></label>
					<input type="text" name="wcam_general_options[automatic_customer_invoice_email_attachments_title][<?php echo $curr_lang ?>]" value="<?php if(isset($options['automatic_customer_invoice_email_attachments_title'][$curr_lang ])) echo $options['automatic_customer_invoice_email_attachments_title'][$curr_lang]; ?>"></input>
					<br/>
				</p>
				<p>
					<label><?php _e('Invoice order email extra text message', 'woocommerce-attach-me');?></label>
					<textarea type="text" name="wcam_general_options[automatic_customer_invoice_email_attachments_body][<?php echo $curr_lang ?>]"><?php if(isset($options['automatic_customer_invoice_email_attachments_body'][$curr_lang ])) echo $options['automatic_customer_invoice_email_attachments_body'][$curr_lang];  ?></textarea>
				</p> -->

				
				<!--<h3><?php _e('Automatic email attachments', 'woocommerce-attach-me');?></h3>
				<h4><?php _e('Emails types', 'woocommerce-attach-me');?></h4>
				<input type="checkbox" <?php if(isset($options['automatic_email_attachments_include_in_processing_order_email']) && $options['automatic_email_attachments_include_in_processing_order_email'] == 'yes') echo 'checked="checked"';?>
						name="wcam_general_options[automatic_email_attachments_include_in_processing_order_email]"  class="" value="yes"><?php _e('Include in the Processing order email?', 'woocommerce-attach-me'); ?></input>
				<br/>
				<input type="checkbox" <?php if(isset($options['automatic_email_attachments_include_in_complete_order_email']) && $options['automatic_email_attachments_include_in_complete_order_email'] == 'yes') echo 'checked="checked"';?>
					   name="wcam_general_options[automatic_email_attachments_include_in_complete_order_email]" class="wcam-customer-admin-notification-checkbox" value="yes"><?php _e('Include in the Complete order email? ', 'woocommerce-attach-me'); ?></input>
				<br/>
				
				<h4><?php _e('File(s) to attach', 'woocommerce-attach-me');?></h4>
				<div class="fallback">
					<input type="file" name="wcam_general_automatic_files_to_attach" multiple /> 
				</div> -->
				
				<h3><?php _e('My Account - Downloads', 'woocommerce-attach-me');?></h3>
				<p>
				<label><?php _e('Show attachments in Download tab', 'woocommerce-attach-me');?></label>
				<?php _e('By default attachments are showed only in the Order details page. Enablig this option they will be listed in the Downloads page.', 'woocommerce-attach-me'); ?><br/>
				<input type="checkbox" min="0" name="wcam_general_options[show_attachments_in_downloads_page]"value="true" <?php if(isset($options['show_attachments_in_downloads_page'])) echo 'checked="checked"';?>><?php _e('Enable', 'woocommerce-attach-me');?></input>
				</p>
				
				
				<h3><?php _e('Link open method', 'woocommerce-attach-me');?></h3>
				<p>
				<label><?php _e('Open link in same window', 'woocommerce-attach-me');?></label>
				<?php _e('By default attachments link are opened in new tab. However this behaviour could be prevented by some Ad blocker. If you are experiencing this issue, enable the following checkbox:', 'woocommerce-attach-me'); ?><br/>
				<input type="checkbox" min="0" name="wcam_general_options[open_link_in_same_window]"value="true" <?php if(isset($options['open_link_in_same_window'])) echo 'checked="checked"';?>><?php _e('Enable', 'woocommerce-attach-me');?></input>
				</p>
				
				<h3><?php _e('Style', 'woocommerce-attach-me');?></h3>
				<p>
				<label><?php _e('Approval option list style (You can select to display approval options using a select box or a radio buttons)', 'woocommerce-attach-me');?></label>
				<select  name="wcam_general_options[approval_box_style]">
					<option value="select" <?php if(!isset($options['approval_box_style']) || $options['approval_box_style'] == 'select') echo 'selected';?>>Select</option>
					<option value="radio" <?php if(isset($options['approval_box_style']) && $options['approval_box_style'] == 'radio') echo 'selected';?>>Radio</option>
				</select>
				</p>
				<p>
				<label><?php _e('Display image preview (both order and products attachments)', 'woocommerce-attach-me');?></label>
				<select  name="wcam_general_options[dispaly_image_preview]">
					<option value="no" <?php if(!isset($options['dispaly_image_preview']) || $options['dispaly_image_preview'] == 'no') echo 'selected';?>><?php _e('No', 'woocommerce-attach-me');?></option>
					<option value="yes" <?php if(isset($options['dispaly_image_preview']) && $options['dispaly_image_preview'] == 'yes') echo 'selected';?>><?php _e('Yes', 'woocommerce-attach-me');?></option>
				</select>
				</p>
				<p>
				<label><?php _e('Display images using a lightbox? (Works only Image Preview option has been enabled. For some themes may not work)', 'woocommerce-attach-me');?></label>
				<input type="checkbox" min="0" name="wcam_general_options[use_lightbox_preview]"value="true" <?php if(isset($options['use_lightbox_preview'])) echo 'checked="checked"';?>></input>
				</p>
				<p>
				<label><?php _e('Thumb  width (by default is used the one setted in "Settings" -> "Media" option)', 'woocommerce-attach-me');?></label>
				<input type="number" min="0" name="wcam_general_options[thumb_width]"value="<?php if(isset($options['thumb_width'])) echo $options['thumb_width']; else echo $default_width;?>" ></input>
				</p>
				<label><?php _e('Thumb  heigth (by default is used the one setted in "Settings" -> "Media" option)', 'woocommerce-attach-me');?></label>
				<input type="number" min="0" name="wcam_general_options[thumb_height]"value="<?php if(isset($options['thumb_height'])) echo $options['thumb_height']; else echo $default_height; ?>" ></input>
				</p>
						
				<input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Save Changes', 'woocommerce-attach-me'); ?>" />
				</p>
			</div>
			</form>
		</div>
		<?php
	}	
		
}
?>