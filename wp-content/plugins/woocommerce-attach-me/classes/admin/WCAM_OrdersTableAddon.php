<?php
class WCAM_OrdersTableAddon
{
	public function __construct()
	{
		add_action( 'manage_shop_order_posts_custom_column', array($this, 'manage_attachment_counter_column'), 10, 2 );
		add_filter( 'manage_edit-shop_order_columns', array($this, 'add_attachment_counter_column'),15 ); 
		 add_action('restrict_manage_posts', array( &$this,'add_attachments_select_box_filter'));
		add_filter('parse_query',array( &$this,'filter_query_by_attachments')); 
		//add_filter( 'manage_edit-shop_order_sortable_columns', array( &$this,'sort_columns') );
		add_action('admin_footer-edit.php', array( &$this,'add_bulk_delete_attachments_action'));
		add_action('load-edit.php', array( &$this,'delete_attachments_bulk_action'));
		add_action('admin_notices', array( &$this,'delete_attachments_admin_notices'));
	}
	
 
	function add_bulk_delete_attachments_action() 
	{
	  global $post_type;
	 
	  if($post_type == 'shop_order') {
		?>
		<script type="text/javascript">
		  jQuery(document).ready(function() {
			jQuery('<option>').val('wcam_delete_attachments').text('<?php _e('Delete attachments', 'woocommerce-attach-me')?>').appendTo("select[name='action']");
			jQuery('<option>').val('wcam_delete_attachments').text('<?php _e('Delete attachments', 'woocommerce-attach-me')?>').appendTo("select[name='action2']");
		  });
		</script>
		<?php
	  }
	}
	function delete_attachments_bulk_action() 
	{
	  global $wcam_file_model;
	  $wp_list_table = _get_list_table('WP_Posts_List_Table');
	  $action = $wp_list_table->current_action();
	
	  switch($action) 
	  {
		// 3. Perform the action
		case 'wcam_delete_attachments':
		  $deleted = 0;
		  $post_ids = $_GET['post'];
		  foreach( $post_ids as $order_id ) {
			$wcam_file_model->delete_all_order_attachments($order_id);
			$deleted++;
		  }
	 
		  $sendback = add_query_arg( array('wcam_deleted' => $deleted, 'post_type'=>'shop_order', 'ids' => join(',', $post_ids) ), $sendback );
		  //$sendback = add_query_arg( array('deleted' => $deleted, 'ids' => join(',', $post_ids) ) );
	 
		break;
		default: return;
	  }
	 
	  wp_redirect($sendback);
	 
	  exit();
	}
	
 
	function delete_attachments_admin_notices($args) 
	{
	  global $post_type, $pagenow;
	
	  if($pagenow == 'edit.php' && $post_type == 'shop_order' &&
		 isset($_REQUEST['wcam_deleted']) && (int) $_REQUEST['wcam_deleted']) 
		 {
		   $message = sprintf( _n( 'Order attachments deleted.', '%s orders attachments deleted.', $_REQUEST['wcam_deleted'] ), number_format_i18n( $_REQUEST['wcam_deleted'] ) );
		   echo '<div class="updated"><p>'.$message.'</p></div>';
	     }
	}
	public function manage_attachment_counter_column( $column, $orderid ) 
	{
		if ( $column == 'attachment-counter' ) 
		{
			$attachmented_files = get_post_meta($orderid, '_wcam_attachments_meta');
			if(!$attachmented_files || empty($attachmented_files[0]))
				echo "0";
			else echo count($attachmented_files[0]);
		}
		
		
	}
	
	function sort_columns( $columns)
	{
		 $columns['attachment-counter'] = 'attachment-counter';
		return $columns;
	}
	public function add_attachment_counter_column($columns)
	 {
		
	   //remove column
	   //unset( $columns['tags'] );

	   //add column
	   $columns['attachment-counter'] =__('Attachment counter', 'woocommerce-attach-me'); 

	   return $columns;
	}
	public function add_attachments_select_box_filter()
	{
		global $typenow;
		global $wp_query;
		if ($typenow=='shop_order') 
		{
			$selected = isset($_GET['wcam_filter_by_attachments']) && $_GET['wcam_filter_by_attachments'] ? $_GET['wcam_filter_by_attachments']:"none";
			//onchange="this.form.submit()" 
			?>
			<select name="wcam_filter_by_attachments" >
				<option value="all" <?php if($selected == "all") echo 'selected="selected"';?>><?php _e('Orders with and without attachments', 'woocommerce-attach-me') ?></option>
				<option value="attachments-only" <?php if($selected == "attachments-only") echo 'selected="selected"';?>><?php _e('Orders with attachments', 'woocommerce-attach-me') ?></option>
			</select>
			<?php
		}
	}
	function filter_query_by_attachments($query) {
		global $pagenow;
		$qv = &$query->query_vars;
		if ($pagenow=='edit.php' && 
		    isset($qv['post_type']) && $qv['post_type']=='shop_order' && isset($_GET['wcam_filter_by_attachments']) && $_GET['wcam_filter_by_attachments'] == 'attachments-only') 
		{
			 $qv['meta_query'][] = 
				array(
				'key' => '_wcam_attachments_meta',
				'compare' => 'NOT NULL'/*,
				 'type' => 'CHAR'  */
			  );
		}
		
	}
}
?>