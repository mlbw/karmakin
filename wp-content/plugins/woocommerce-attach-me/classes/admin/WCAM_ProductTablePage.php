<?php 
class WCAM_ProductTablePage
{
	var $wpml;
	public function __construct()
	{
		add_action( 'manage_product_posts_custom_column', array(&$this, 'manage_product_schedule_column'), 10, 2 );
		add_filter( 'manage_edit-product_columns', array(&$this, 'add_post_schedule_info_column'),15 );
	}
	function manage_product_schedule_column( $column, $post_id ) 
	{
		global $wcam_product_model, $wcam_bulk_products_attachments_options;
		
		if(!isset($this->wpml))
			$this->wpml = new WCAM_Wpml();
		$original_id = 	$this->wpml->get_original_id($post_id);
	    $post_id = $original_id ? $original_id : $post_id ;
		
		$rules = $wcam_bulk_products_attachments_options->get_rules_by_product_id($post_id);
		$specific_product_rules = $wcam_product_model->get_attachments($post_id);
		if ( $column == 'wcam-bulk-attachment-rule' ) 
		{
			if($rules)
			{
				echo '<strong>'.__('Bulk rule id(s): ', 'woocommerce-attach-me').'</strong><br/>';
				echo '<ul>';
					foreach((array)$rules as $rule)
					{
					  echo '<li><a class="" target="_blank" href="'.admin_url().'admin.php?page='.WCAM_BulkProductsAttachmentsConfigurator::$page_url_par.'">'.
							//'<span class="dashicons dashicons-calendar-alt"></span>'.
							$rule['rule_id'].
							'</a></li>';
					}
				echo '</ul>';
				//Variations
				/* if(($variations = $wcam_product_model->get_variations($post_id) ) != null)
				{
					foreach((array)$variations as $variation)
					{
						$rules2 = $wcam_product_model->get_price_rules_by_prduct_id($variation->ID, 'product');
						foreach((array)$rules2 as $rule)
						{
							  _e('(ID: ','woocommerce-attach-me');
							  echo $variation->ID.') <a class="" target="_blank" href="'.admin_url().'admin.php?page='.WCAM_BulkProductsAttachmentsConfigurator::$page_url_par.'">'.
									//'<span class="dashicons dashicons-calendar-alt"></span>'.
									$rule.
									'</a><br/>';
						}
					}
				} */
			}
			
			//specific rules
			if($specific_product_rules)
			{
				/*if(!empty($rules) || !empty($rules2))
					echo '<br/>'; */
				
				/* $unique_files = array();
				$total_attachment = 0;
				foreach($specific_product_rules as $rule)
				{
					if(!isset($unique_files[$rule['rule_id']]))
						$total_attachment ++;
				} */
				echo '<strong>'.__('# attachments: ', 'woocommerce-attach-me').'</strong><br/>';
				
				echo count($specific_product_rules).'<br/>';
			}
		}
	}
	function add_post_schedule_info_column($columns)
	 {

	   //remove column
	   //unset( $columns['tags'] );

	   //add column
	   $columns['wcam-bulk-attachment-rule'] =__('Attachments', 'woocommerce-attach-me'); 

	   return $columns;
	}
}
?>