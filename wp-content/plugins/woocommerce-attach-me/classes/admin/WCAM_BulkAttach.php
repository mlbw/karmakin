<?php 
class WCAM_BulkAttach
{
	public function __construct()
	{
		
	}
	public function render_page()
	{
		wp_enqueue_script (  'jquery-ui-core'  ) ;
		wp_enqueue_script( 'jquery-ui-progressbar' );
		wp_enqueue_script('wcam-csv-jquery-lib',wcam_PLUGIN_PATH.'/js/jquery.csv-0.71.min.js'); 
		wp_enqueue_script('wcam-backend-bulk-import', wcam_PLUGIN_PATH.'/js/wcam-backend-bulk-import.js', array( 'jquery' ) );
		
		wp_enqueue_style( 'wcam-common', wcam_PLUGIN_PATH.'/css/wcam-common.css'  );
		wp_enqueue_style('wcam-bluk-import', wcam_PLUGIN_PATH.'/css/wcam-bluk-import.css');	
		wp_enqueue_style('jquery-style',  wcam_PLUGIN_PATH.'/css/jquery-ui.css');
		
		?>
		<div class="wrap">
			<h2><?php _e('Bulk import', 'woocommerce-attach-me');?></h2>
			<div id="white-box">
			
				<h4><?php _e('File format info', 'woocommerce-attach-me');?></h4>
				<div id="wcam-file-upload-box">
					<p>
						<h3><?php _e('Will be imported only data for columns with following titles:', 'woocommerce-attach-me'); ?></h3>
					</p>
					<ul>
						<li><strong>order_id</strong></li>
						<li><strong>title</strong></li>
						<li><strong>url</strong></li>
						<li><strong>customer-has-to-be-approved</strong> <i><?php _e('(possible values: "yes" or "no". if empty, will be considered as "no")', 'woocommerce-attach-me'); ?></i></li>
						<li><strong>customer-can-reapprove</strong> <i><?php _e('(possible values: "yes" or "no". if empty, will be considered as "no")', 'woocommerce-attach-me'); ?></i></li>
						<li><strong>customer-feedback-enabled</strong> <i><?php _e('(possible values: "yes" or "no". if empty, will be considered as "no")', 'woocommerce-attach-me'); ?></i></li>
						<li><strong>customer-admin-notification</strong> <i><?php _e('(possible values: "yes" or "no". if empty, will be considered as "no")', 'woocommerce-attach-me'); ?></i></li>
					<ul>
					<h4><?php _e('Note', 'woocommerce-attach-me');?></h4>
					<p><?php _e('Bulk import work ONLY with external link, files will not be copied on local server.', 'woocommerce-attach-me'); ?><p>
					<input type="file" name="wcam-file" id="wcam-file"></input>
				</div>
				<div id="wcam-result-box" style="display:none;">
					<h3 id="ajax-progress-title"><?php  _e('Importing Progress', 'woocommerce-attach-me');?></h3>
					<div id="ajax-progress"></div>
					<div id="progressbar"></div>
					<h3 id="ajax-response-title"><?php  _e('Importing Result', 'woocommerce-attach-me');?></h3>				
					<div id="ajax-response"></div>
					<button class="button-primary" id="wcam-reimport-csv-button"><?php _e('Import another file', 'woocommerce-attach-me'); ?></button>
				</div>
				<button class="button-primary" id="wcam-import-csv-button"><?php _e('Import', 'woocommerce-attach-me'); ?></button>
			</div>
		</div>
		<?php
		
	}
}
?>