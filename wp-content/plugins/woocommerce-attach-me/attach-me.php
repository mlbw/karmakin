<?php
/*
Plugin Name: WooCommerce Attach Me!
Description: WCAM lets the shop admin to attach any file to any order and any product!
Author: Lagudi Domenico
Version: 10.4
*/

/* 
Copyright: WooCommerce Attach Me! uses the ACF PRO plugin. ACF PRO files are not to be used or distributed outside of the WooCommerce Attach Me! plugin.
*/

//define('wcam_PLUGIN_PATH', WP_PLUGIN_URL."/".dirname( plugin_basename( __FILE__ ) ) );
define('wcam_PLUGIN_PATH', rtrim(plugin_dir_url(__FILE__), "/") ) ;
define('WCAM_PLUGIN_ABS_PATH', dirname( __FILE__ ) );

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) 
{
	include_once( "classes/com/WCAM_Acf.php");
	include_once( "classes/com/WCAM_Globals.php");
	/* if(!class_exists('WCAM_OrderAddon'))
		require_once('WCAM_OrderAddon.php'); */
	
	//Admin
	if(!class_exists('WCAM_OrderAddon'))
	{
		require_once('classes/admin/WCAM_OrderAddon.php');
		$woocommerce_order_addon = new WCAM_OrderAddon();
	}
	if(!class_exists('WCAM_EmailsAttachmentsConfigurator'))
	{
		require_once('classes/admin/WCAM_EmailsAttachmentsConfigurator.php');
		$wcam_email_attachments_configurator = new WCAM_EmailsAttachmentsConfigurator();
	}
	if(!class_exists('WCAM_EmailsPresetTextsConfigurator'))
	{
		require_once('classes/admin/WCAM_EmailsPresetTextsConfigurator.php');
		$wcam_email_preset_texts_configurator = new WCAM_EmailsPresetTextsConfigurator();
	}
	if(!class_exists('WCAM_BulkProductsAttachmentsConfigurator'))
	{
		require_once('classes/admin/WCAM_BulkProductsAttachmentsConfigurator.php');
		$wcam_bulk_products_attachments_configurator = new WCAM_BulkProductsAttachmentsConfigurator();
	}
	if(!class_exists('WCAM_OrdersTableAddon'))
	{
			require_once('classes/admin/WCAM_OrdersTableAddon.php');
			$wcam_order_table_addon = new WCAM_OrdersTableAddon();
	}
	if(!class_exists('WCAM_ProductTablePage'))
	{
			require_once('classes/admin/WCAM_ProductTablePage.php');
			$wcam_order_table_addon = new WCAM_ProductTablePage();
	}
	
	//Com
	if(!class_exists('WCAM_Wpml'))
	{
			require_once('classes/com/WCAM_Wpml.php');
	}
	if(!class_exists('WCAM_File'))
	{
			require_once('classes/com/WCAM_File.php');
			$wcam_file_model = new WCAM_File();
	}
	if(!class_exists('WCAM_Order'))
	{
			require_once('classes/com/WCAM_Order.php');
			$wcam_order_model = new WCAM_Order();
	}
	if(!class_exists('WCAM_Product'))
	{
			require_once('classes/com/WCAM_Product.php');
			$wcam_product_model = new WCAM_Product();
	}
	if(!class_exists('WCAM_Shortcode'))
	{
			require_once('classes/com/WCAM_Shortcode.php');
			$wcam_shortcode_model= new WCAM_Shortcode();
	}
	if(!class_exists('WCAM_Customer'))
	{
			require_once('classes/com/WCAM_Customer.php');
			$wcam_customer_model = new WCAM_Customer();
	}
	if(!class_exists('WCAM_Email'))
		require_once('classes/com/WCAM_Email.php');
	if(!class_exists('WCAM_Notifier'))
	{
		require_once('classes/com/WCAM_Notifier.php');
	}
	if(!class_exists('WCAM_Option'))
		require_once('classes/com/WCAM_Option.php');
	
	if(!class_exists('WCAM_BulkProductsAttachments'))
	{
		require_once('classes/com/WCAM_BulkProductsAttachments.php');
		$wcam_bulk_products_attachments_options = new WCAM_BulkProductsAttachments();
	}
	
	//Frontend
	if(!class_exists('WCAM_OrderDetailsAddon'))
	{
			require_once('classes/frontend/WCAM_OrderDetailsAddon.php');
			$wcam_woocommerce_order_details_addon = new WCAM_OrderDetailsAddon();
	}
	if(!class_exists('WCAM_MyAccountPage'))
	{
			require_once('classes/frontend/WCAM_MyAccountPage.php');
			$wcam_my_account_page = new WCAM_MyAccountPage();
	}
	if(!class_exists('WCAM_DownloadsPage'))
	{
			require_once('classes/frontend/WCAM_DownloadsPage.php');
			$wcam_downloads_page = new WCAM_DownloadsPage();
	}
	if(!class_exists('WCAM_DownloadsPage'))
	{
			require_once('classes/frontend/WCAM_DownloadsPage.php');
			$wcam_downloads_page = new WCAM_DownloadsPage();
	}
	if(!class_exists('WCAM_ProductPage'))
	{
			require_once('classes/frontend/WCAM_ProductPage.php');
			$wcam_product_page = new WCAM_ProductPage();
	}
	
	add_action('admin_menu', 'wcam_init_admin_panel');
	add_action('init', 'wcam_wp_init');
	add_action('admin_init', 'wcam_register_settings');
	//add_action( 'admin_init', 'wcam_register_settings');
	//add_action( 'admin_enqueue_scripts', 'wcam_register_settings' );
} 

function wcam_wp_init()
{
	load_plugin_textdomain('woocommerce-attach-me', false, basename( dirname( __FILE__ ) ) . '/languages' );
	
	if ( ! function_exists( 'wp_handle_upload' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
	}
	$wcam_notifier = new WCAM_Notifier();
}

function wcam_register_settings()
{ 
	register_setting('wcam_general_options_group','wcam_general_options');
	//wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
	/*register_setting('wcam_files_fields_meta_groups', 'wcam_files_fields_meta');
	wp_enqueue_script('tiny_mce');
	wp_enqueue_script('wcam-js', wcam_PLUGIN_PATH.'/js/wcam.js', array('jquery')); */
} 

function wcam_init_admin_panel()
{
	if(!current_user_can('manage_woocommerce'))
		return;
	
	$place = $place = wcam_get_free_menu_position(55, 0.1);
	//load_plugin_textdomain('woocommerce-attach-me', false, basename( dirname( __FILE__ ) ) . '/languages' );
	add_menu_page( __( 'Attach Me!', 'woocommerce' ), __( 'Attach Me!', 'woocommerce' ), 'manage_woocommerce', 'woocommerce-attach-me', null, wcam_PLUGIN_PATH."/images/icon-clip.png", (string)$place );
	//add_submenu_page('woocommerce-attach-me', __('Bulk import','woocommerce-attach-me'), __('Bulk import','woocommerce-attach-me'), 'edit_shop_orders', 'woocommerce-attach-me-bulk-import', 'wcam_render_bulk_import_page');
	add_submenu_page('woocommerce-attach-me', __('General options & texts','woocommerce-attach-me'), __('General options & texts','woocommerce-attach-me'), 'edit_shop_orders', 'woocommerce-attach-me-options', 'wcam_render_options_page');

	remove_submenu_page( 'woocommerce-attach-me', 'woocommerce-attach-me');
	
	/* $wcam_email_attachments_configurator = new WCAM_EmailsAttachmentsConfigurator();
	$wcam_email_preset_texts_configurator = new WCAM_EmailsPresetTextsConfigurator(); */
}
function wcam_render_options_page()
{
	if(!class_exists('WCAM_Options'))
		require_once('classes/admin/WCAM_Options.php');
	$page = new WCAM_Options();
	$page->render_page();
}
function wcam_render_bulk_import_page()
{
	if(!class_exists('WCAM_BulkAttach'))
		require_once('classes/admin/WCAM_BulkAttach.php');
	$page = new WCAM_BulkAttach();
	$page->render_page();
}
function wcam_var_dump($var)
{
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
}
function wcam_get_free_menu_position($start, $increment = 0.1)
{
	foreach ($GLOBALS['menu'] as $key => $menu) {
		$menus_positions[] = $key;
	}
	
	if (!in_array($start, $menus_positions)) return $start;

	/* the position is already reserved find the closet one */
	while (in_array($start, $menus_positions)) 
	{
		$start += $increment;
	}
	return $start;
}
function wcam_is_image($image_path_string)
{
	$supported_image = array(
		 '.jpg',
		'.jpeg',
		'.gif',
		'.png',
	);
	 if (in_array(strtolower(strrchr($image_path_string, '.')), $supported_image))
		return true;
	 else 
		return false;
	
}
function wcam_get_woo_version_number()
{
	    // If get_plugins() isn't available, require it
	if ( ! function_exists( 'get_plugins' ) )
		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	
        // Create the plugins folder and file variables
	$plugin_folder = get_plugins( '/' . 'woocommerce' );
	$plugin_file = 'woocommerce.php';
	
	// If the plugin version number is set, return it 
	if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
		return $plugin_folder[$plugin_file]['Version'];

	} else {
	// Otherwise return null
		return NULL;
	}
}
?>